// Generated code from Butter Knife. Do not modify!
package com.airasia.mobile;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.Injector;

public class Add_OnsActivity$$ViewInjector<T extends com.airasia.mobile.Add_OnsActivity> implements Injector<T> {
  @Override public void inject(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131427411, "field 'tv_amount'");
    target.tv_amount = finder.castView(view, 2131427411, "field 'tv_amount'");
    view = finder.findRequiredView(source, 2131427412, "field 'tv_currencySymbol'");
    target.tv_currencySymbol = finder.castView(view, 2131427412, "field 'tv_currencySymbol'");
    view = finder.findRequiredView(source, 2131427413, "field 'tv_decimalvalue'");
    target.tv_decimalvalue = finder.castView(view, 2131427413, "field 'tv_decimalvalue'");
    view = finder.findRequiredView(source, 2131427414, "field 'btn_down'");
    target.btn_down = finder.castView(view, 2131427414, "field 'btn_down'");
    view = finder.findRequiredView(source, 2131427417, "field 'btn_nextsummary'");
    target.btn_nextsummary = finder.castView(view, 2131427417, "field 'btn_nextsummary'");
    view = finder.findRequiredView(source, 2131427416, "field 'list_View'");
    target.list_View = finder.castView(view, 2131427416, "field 'list_View'");
  }

  @Override public void reset(T target) {
    target.tv_amount = null;
    target.tv_currencySymbol = null;
    target.tv_decimalvalue = null;
    target.btn_down = null;
    target.btn_nextsummary = null;
    target.list_View = null;
  }
}
