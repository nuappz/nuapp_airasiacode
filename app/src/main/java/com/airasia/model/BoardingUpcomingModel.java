package com.airasia.model;

public class BoardingUpcomingModel {
    private String mId;
    private String mFlightNo;
    private String mSource;
    private String mDestination;
    private String mNameList;
    private String mIsGroup;
    private int mThumbnail;

    public String getmId() {
        return mId;
    }

    public void setmId(String mId) {
        this.mId = mId;
    }

    public String getmFlightNo() {
        return mFlightNo;
    }

    public void setmFlightNo(String mFlightNo) {
        this.mFlightNo = mFlightNo;
    }

    public String getmSource() {
        return mSource;
    }

    public void setmSource(String mSource) {
        this.mSource = mSource;
    }

    public int getmThumbnail() {
        return mThumbnail;
    }

    public void setmThumbnail(int mThumbnail) {
        this.mThumbnail = mThumbnail;
    }

    public String getmIsGroup() {
        return mIsGroup;
    }

    public void setmIsGroup(String mIsGroup) {
        this.mIsGroup = mIsGroup;
    }

    public String getmNameList() {
        return mNameList;
    }

    public void setmNameList(String mNameList) {
        this.mNameList = mNameList;
    }

    public String getmDestination() {
        return mDestination;
    }

    public void setmDestination(String mDestination) {
        this.mDestination = mDestination;
    }
}
