package com.airasia.model;

import java.util.ArrayList;
import java.util.List;

public class CountryModel {

	private String name;
	private String code;
	private String dialingCode;
	private String stateName;
	private String stateKey;
	
	List<CurrencyModel> currencyList = new ArrayList<CurrencyModel>();
	
	public CountryModel(){
		
	}
	
	public CountryModel(String _name, String _code, String _dialingCode){
		this.name = _name;
		this.code = _code;
		this.dialingCode = _dialingCode;
	}
	
	public CountryModel(String _name, String _code, String _stateName, String _stateKey){
		this.name = _name;
		this.code = _code;
		this.stateKey = _stateKey;
		this.stateName = _stateName;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getDialingCode() {
		return dialingCode;
	}
	public void setDialingDode(String dialingDode) {
		this.dialingCode = dialingDode;
	}

	public String getStateName() {
		return stateName;
	}

	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

	public String getStateKey() {
		return stateKey;
	}

	public void setStateKey(String stateKey) {
		this.stateKey = stateKey;
	}

	public void setDialingCode(String dialingCode) {
		this.dialingCode = dialingCode;
	}
	
	public void addCurrency(String key, float value){
		if (currencyList == null){
			currencyList = new ArrayList<CurrencyModel>();
		}
		CurrencyModel cModel = new CurrencyModel();
		cModel.setCurrencyCountry(key);
		cModel.setPrice(value);
		currencyList.add(cModel);
	}
	
	public byte[] getCurrencyBytes(){
		byte[] bytesArr = null;
		if (currencyList != null){
			String stringToConvert = "";
			for (int i = 0; i < currencyList.size(); i ++){
				CurrencyModel cModel = currencyList.get(i);
				stringToConvert += cModel.getFromCurrency() + "&"+cModel.getPrice();
				if (i!=(currencyList.size()-1)){
					stringToConvert+="|";
				}
			}
			bytesArr  = stringToConvert.getBytes();
		}
		return bytesArr;
	}
	
	
}
