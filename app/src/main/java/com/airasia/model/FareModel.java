package com.airasia.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class FareModel implements Serializable {

	List<FareTaxesModel> taxHash = new ArrayList<FareTaxesModel>();
	String currencyCode;
	double originalFare, finalFare;
	

	public String getCurrencyCode() {
		return currencyCode;
	}
	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}
	public double getOriginalFare() {
		return originalFare;
	}
	public void setOriginalFare(double originalFare) {
		this.originalFare = originalFare;
	}
	public double getFinalFare() {
		return finalFare;
	}
	public void setFinalFare(double finalFare) {
		this.finalFare = finalFare;
	}
	
	public int getDiscountPercentage(){
		int result = 0;
		
		double deducted = getOriginalFare() - getFinalFare();
		
		if (deducted!=0){
			result = (int) (deducted/getOriginalFare()) * 100;
		}
		
		return result;
	}
	
	public List<FareTaxesModel> getTaxHash() {
		return taxHash;
	}
	
	public void setTaxHash(List<FareTaxesModel> taxHash) {
		this.taxHash = taxHash;
	}
		
}
