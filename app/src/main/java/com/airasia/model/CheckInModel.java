package com.airasia.model;

import java.io.Serializable;

public class CheckInModel implements Serializable {

	String liftStatus;
	int boardingSequence;
	
	
	public String getLiftStatus() {
		return liftStatus;
	}
	
	public boolean isCheckIn(){
		return liftStatus.equalsIgnoreCase("Default");
	}
	
	public void setLiftStatus(String _liftStatus) {
		liftStatus = _liftStatus;
	}
	public int getBoardingSequence() {
		return boardingSequence;
	}
	public void setBoardingSequence(int _boardingSequence) {
		boardingSequence = _boardingSequence;
	}
	
	
	
	
}
