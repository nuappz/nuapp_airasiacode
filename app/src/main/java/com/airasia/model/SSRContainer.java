package com.airasia.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class SSRContainer implements Serializable{

	String carrierCode, flightNum, opSuffix, departdate;
	String departCode, arrivalCode, defaultBaggage;

	int mealSegPos = 0;
	
	boolean isDepart = false;
	
	List<SSRItemModel> wheelList;
	List<SSRItemModel> baggageList;
	List<SSRItemModel> mealList;
	List<SSRItemModel> mealBundle;
	
	boolean freeBaggage = false, meal = false;

	public String getCarrierCode() {
		return carrierCode;
	}

	public void setCarrierCode(String carrierCode) {
		this.carrierCode = carrierCode;
	}

	public String getFlightNum() {
		return flightNum;
	}

	public void setFlightNum(String flightNum) {
		this.flightNum = flightNum;
	}

	public String getOpSuffix() {
		return opSuffix;
	}

	public void setOpSuffix(String opSuffix) {
		this.opSuffix = opSuffix;
	}

	public String getDepartdate() {
		return departdate;
	}

	public void setDepartdate(String departdate) {
		this.departdate = departdate;
	}

	public String getDepartCode() {
		return departCode;
	}

	public void setDepartCode(String departCode) {
		this.departCode = departCode;
	}

	public String getArrivalCode() {
		return arrivalCode;
	}

	public void setArrivalCode(String arrivalCode) {
		this.arrivalCode = arrivalCode;
	}

	public String getDefaultBaggage() {
		return defaultBaggage;
	}

	public void setDefaultBaggage(String defaultBaggage) {
		this.defaultBaggage = defaultBaggage;
	}

	public boolean isFreeBaggage() {
		return freeBaggage;
	}

	public void setFreeBaggage(boolean freeBaggage) {
		this.freeBaggage = freeBaggage;
	}

	public boolean isMeal() {
		return meal;
	}

	public void setMeal(boolean meal) {
		this.meal = meal;
	}

	public List<SSRItemModel> getWheelList() {
		return wheelList;
	}

	public void setWheelList(List<SSRItemModel> _wheelList) {
		this.wheelList = _wheelList;
//		if (wheelList == null){
//			wheelList = new ArrayList<WheelChairModel>();
//		}else{
//			wheelList.clear();
//			wheelList = null;
//			wheelList = new ArrayList<WheelChairModel>();
//		}
//		this.wheelList.addAll(wheelList);
	}

	public List<SSRItemModel> getBaggageList() {
		return baggageList;
	}

	public void setBaggageList(List<SSRItemModel> _baggageList) {
		this.baggageList = _baggageList;
	}
	public List<SSRItemModel> getMealList() {
		return mealList;
	}

	public void setMealList(List<SSRItemModel> mealList) {
		this.mealList = mealList;
	}
	
	public SSRItemModel getItemBaseKey(String key){
		for (int i = 0 ; i < baggageList.size(); i++){
			SSRItemModel mModel = baggageList.get(i);
			if (mModel.getSsrCode().equalsIgnoreCase(key)){
				return mModel;
			}
		}
		return null;
	}

	public boolean isDepart() {
		return isDepart;
	}

	public void setDepart(boolean isDepart) {
		this.isDepart = isDepart;
	}

	public int getMealSegPos() {
		return mealSegPos;
	}

	public void setMealSegPos(int mealSegPos) {
		this.mealSegPos = mealSegPos;
	}

	public List<SSRItemModel> getMealBundle() {
		return mealBundle;
	}

	public void setMealBundle(List<SSRItemModel> mealBundle) {
		this.mealBundle = mealBundle;
	}
	
}
