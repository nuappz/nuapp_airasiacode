package com.airasia.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class StationModel implements Serializable {

	private String key, stationName, countryName, airportName, countryCode;
	List<String> destinationKey;
	
	public StationModel(){
		
	}
	
	public StationModel(String _key, String _stationName, String _countryName, String _airportName){
		this.key  = _key;
		this.stationName = _stationName;
		this.countryName = _countryName;
		this.airportName = _airportName;
		
	}

	public String getKey() {
		return key;
	}

	public String getStationName() {
		return stationName;
	}

	public String getCountryName() {
		return countryName;
	}

	public String getAirportName() {
		return airportName;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public void setStationName(String stationName) {
		this.stationName = stationName;
	}

	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}

	public void setAirportName(String airportName) {
		this.airportName = airportName;
	}
	
	public String getNameWithKey(){
		return this.stationName + " (" + this.key + ")";
	}

	public List<String> getDestinationKey() {
		return destinationKey;
	}

	public void setDestinationKey(List<String> destinationKey) {
		if (this.destinationKey == null){
			this.destinationKey = new ArrayList<String>();
		}
		this.destinationKey.addAll(destinationKey);
	}
	
	
	
	
	
}
