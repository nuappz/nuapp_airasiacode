package com.airasia.model;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;

import com.airasia.mobile.R;
import com.airasia.fragment.FragmentCase;

public class DrawerModel {

	boolean isSelected;
	String name;
	int position;
	
	public DrawerModel(String _name, boolean _isSelected, int _position){
		this.name = _name;
		this.isSelected = _isSelected;
		this.position = _position;
	}
	
	
	public int getPosition(){
		return this.position;
	}
	
	public String getName(){
		return this.name;
	}
	
	public static List<DrawerModel> generateDrawerLsit(Activity act){
		List<DrawerModel> drawerList = new ArrayList<DrawerModel>();

		drawerList.add(new DrawerModel(act.getString(R.string.drawer_banner), false, FragmentCase.FRAG_DRAWER_BANNER));
		drawerList.add(new DrawerModel(act.getString(R.string.drawer_home), false, FragmentCase.FRAG_HOME));
		drawerList.add(new DrawerModel(act.getString(R.string.drawer_booking), false, FragmentCase.FRAG_BOOK_FLIGHTS));
		drawerList.add(new DrawerModel(act.getString(R.string.drawer_check_in), false, FragmentCase.FRAG_CHECK_IN));
		drawerList.add(new DrawerModel(act.getString(R.string.drawer_flight_status), false, FragmentCase.FRAG_FLIGHT_STATUS));
		drawerList.add(new DrawerModel(act.getString(R.string.drawer_manage), false, FragmentCase.FRAG_MANAGE_BOOKING));
		drawerList.add(new DrawerModel(act.getString(R.string.drawer_hotel), false, FragmentCase.FRAG_HOTEL_DEALS));
		drawerList.add(new DrawerModel(act.getString(R.string.drawer_settings), false, FragmentCase.FRAG_SETTINGS));
		
		return drawerList;
	}
	
	public static List<DrawerModel> generateProfileLsit(Activity act){
		List<DrawerModel> drawerList = new ArrayList<DrawerModel>();
		
		drawerList.add(new DrawerModel(act.getString(R.string.profile_my_profile), false, FragmentCase.FRAG_INFO_PAGE));
		drawerList.add(new DrawerModel(act.getString(R.string.profile_my_travel), false, FragmentCase.FRAG_MY_DOCUMENT));
		drawerList.add(new DrawerModel(act.getString(R.string.profile_my_bookings), false, FragmentCase.FRAG_MY_BOOKING));
		drawerList.add(new DrawerModel(act.getString(R.string.profile_my_frenfamily), false, FragmentCase.FRAG_MY_FREN));
		
		return drawerList;
	}
	
}
