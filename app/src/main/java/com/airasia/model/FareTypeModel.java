package com.airasia.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class FareTypeModel implements Serializable {
	
	String id;
	int fareType;
	int MinAvailableSeats;
	HashMap<String, FareModel> hashMap = new HashMap<String, FareModel>();
	List<SegmentModel> segList = new ArrayList<SegmentModel>();
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public int getMinAvailableSeats() {
		return MinAvailableSeats;
	}
	public void setMinAvailableSeats(int minAvailableSeats) {
		MinAvailableSeats = minAvailableSeats;
	}
	
	public HashMap<String, FareModel> getHashMap() {
		return hashMap;
	}
	
	public void setHashMap(HashMap<String, FareModel> hashMap) {
		this.hashMap = hashMap;
	}
	
	public void addFareModel(String key, FareModel model){
		hashMap.put(key, model);
	}
	
	public List<SegmentModel> getSegList() {
		return segList;
	}
	
	public void setSegList(List<SegmentModel> segList) {
		this.segList = segList;
	}
	public int getFareType() {
		return fareType;
	}
	public void setFareType(int fareType) {
		this.fareType = fareType;
	}

}
