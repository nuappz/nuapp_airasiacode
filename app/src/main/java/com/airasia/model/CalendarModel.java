package com.airasia.model;

import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import com.airasia.util.LogHelper;

public class CalendarModel {

	public Date calDate;
	
	String header;
	
	public int numberOfDays;
	public int firstDayOfMonthDate;
	
	public CalendarModel(Date date)
	{
		
		Calendar cal = Calendar.getInstance();
		
		cal.setTime(date);
		cal.set(Calendar.DAY_OF_MONTH, Calendar.getInstance().getActualMinimum(Calendar.DAY_OF_MONTH));
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		
		calDate = cal.getTime();
		
		LogHelper.debug("adding date = "+calDate);

		// Get the number of days in that month
		numberOfDays = cal.getActualMaximum(Calendar.DAY_OF_MONTH); // 28
		LogHelper.debug("number of days in month = "+numberOfDays);
		
		firstDayOfMonthDate = cal.get(Calendar.DAY_OF_WEEK)-cal.getFirstDayOfWeek()+7;
		
		firstDayOfMonthDate = firstDayOfMonthDate%7;
		
		LogHelper.debug("first day of the month = "+firstDayOfMonthDate);
		
		getMonthYear();
	}
	
	public String getMonthYear()
	{
		if(header==null || header.length()==0)
		{
			Calendar c = Calendar.getInstance();
			c.setTime(calDate);
			header = String.format(Locale.getDefault(),
					"%1$tB %1$tY", c);
		}
		
		
		return header;
	}
	
	
	

}
