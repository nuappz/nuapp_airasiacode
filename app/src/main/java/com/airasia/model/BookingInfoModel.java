package com.airasia.model;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import com.airasia.util.LogHelper;

public class BookingInfoModel implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	 
	String id, recordLocator, currencyCode, paidStatus, bookingStatus, bookingType;
	String expiredDate, lastSegmentSTD;
	
	int paxCount, segmentCount, totalJourney, connectingFlights, authorizedBalanceDue;
	float balanceDue, totalCost;
	
	List<FareModel> oneWay;
	List<FareModel> returnWay;
	
	List<JourneyModel> returnJourney = new ArrayList<JourneyModel>();
	List<JourneyModel> departJourney = new ArrayList<JourneyModel>();
	
	List<List<String>> journey;
	
	List<MemberInfoModel> passengerInfo;

	FareTypeModel departFare;
	FareTypeModel returnFare;
	
	HashMap<String, List<FareTaxesModel>> departTaxes;
	List<FareTaxesModel> departTotalTax = new ArrayList<FareTaxesModel>();;
	List<FareTaxesModel> returnTotalTax = new ArrayList<FareTaxesModel>();;
	HashMap<String, List<FareTaxesModel>> returnTaxes;
	
	List<SegmentModel> departSegments;
	List<SegmentModel> returnSegments;
	
	int adultCount = 0;
	int childCount = 0;
	int infantCount = 0;
	
	boolean isInternational = false;
	
//	ExpiredDate	:	2015-06-17 22:45:00
//	LastSegmentSTD	:	2015-06-11 13:40:00
			

	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	
	public String getRecordLocator() {
		return recordLocator;
	}
	
	public void setRecordLocator(String recordLocator) {
		this.recordLocator = recordLocator;
	}
	
	public String getCurrencyCode() {
		return currencyCode;
	}
	
	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}
	
	public String getPaidStatus() {
		return paidStatus;
	}
	
	public void setPaidStatus(String paidStatus) {
		this.paidStatus = paidStatus;
	}
	
	public String getBookingStatus() {
		return bookingStatus;
	}
	
	public void setBookingStatus(String bookingStatus) {
		this.bookingStatus = bookingStatus;
	}
	
	public String getBookingType() {
		return bookingType;
	}
	
	public void setBookingType(String bookingType) {
		this.bookingType = bookingType;
	}
	
	public int getPaxCount() {
		return paxCount;
	}
	
	public void setPaxCount(int paxCount) {
		this.paxCount = paxCount;
	}
	
	public int getSegmentCount() {
		return segmentCount;
	}
	
	public void setSegmentCount(int segmentCount) {
		this.segmentCount = segmentCount;
	}
	
	public int getTotalJourney() {
		return totalJourney;
	}
	
	public void setTotalJourney(int totalJourney) {
		this.totalJourney = totalJourney;
	}
	
	public int getConnectingFlights() {
		return connectingFlights;
	}
	
	public void setConnectingFlights(int connectingFlights) {
		this.connectingFlights = connectingFlights;
	}
	
	public float getBalanceDue() {
		return balanceDue;
	}
	
	public void setBalanceDue(float balanceDue) {
		this.balanceDue = balanceDue;
	}
	
	public float getTotalCost() {
		return totalCost;
	}
	
	public void setTotalCost(float totalCost) {
		this.totalCost = totalCost;
	}
	

	public String getExpiredDate() {
		return expiredDate;
	}

	public void setExpiredDate(String _expiredDate) {
		expiredDate = _expiredDate;
	}

	public String getLastSegmentSTD() {
		return lastSegmentSTD;
	}

	public void setLastSegmentSTD(String _lastSegmentSTD) {
		lastSegmentSTD = _lastSegmentSTD;
	}

	public int getAuthorizedBalanceDue() {
		return authorizedBalanceDue;
	}

	public void setAuthorizedBalanceDue(int _authorizedBalanceDue) {
		authorizedBalanceDue = _authorizedBalanceDue;
	}
	
	public void setDepartureList(List<FareModel> fareList){
		this.oneWay = fareList;
	}

	public void setReturnList(List<FareModel> fareList){
		this.returnWay = fareList;
	}

	public List<JourneyModel> getReturnJourney() {
		return returnJourney;
	}

	public void setReturnJourney(List<JourneyModel> returnJourney) {
		this.returnJourney = returnJourney;
	}

	public List<JourneyModel> getDepartJourney() {
		return departJourney;
	}

	public void setDepartJourney(List<JourneyModel> departJourney) {
		this.departJourney = departJourney;
	}
	
	public void setMemberList(List<MemberInfoModel> _passengerInfo){
		this.passengerInfo = _passengerInfo;
	}
	
	public List<MemberInfoModel> getMemberList(){
		return this.passengerInfo;
	}
	
	public void updateMemberAtPos(int pos, String expDate, String issue,
			String dob, String national, String passport){
		if (pos<passengerInfo.size()){
			passengerInfo.get(pos).setExpiredDate(expDate);
			passengerInfo.get(pos).setDob(dob);
			passengerInfo.get(pos).setIssuingCountry(issue);
			passengerInfo.get(pos).setNationality(national);
			passengerInfo.get(pos).setPassport(passport);
		}
	}

	public List<FareModel> getOneWay() {
		return oneWay;
	}

	public void setOneWay(List<FareModel> oneWay) {
		this.oneWay = oneWay;
	}

	public List<FareModel> getReturnWay() {
		return returnWay;
	}

	public void setReturnWay(List<FareModel> returnWay) {
		this.returnWay = returnWay;
	}

	public List<List<String>> getJourney() {
		return journey;
	}

	public void setJourney(List<List<String>> journey) {
		this.journey = journey;
	}

	public List<MemberInfoModel> getPassengerInfo() {
		return passengerInfo;
	}

	public void setPassengerInfo(List<MemberInfoModel> passengerInfo) {
		this.passengerInfo = passengerInfo;
	}

	public List<SegmentModel> getDepartSegments() {
		return departSegments;
	}

	public void setDepartSegments(List<SegmentModel> departSegments) {
		this.departSegments = departSegments;
	}

	public List<SegmentModel> getReturnSegments() {
		return returnSegments;
	}

	public void setReturnSegments(List<SegmentModel> returnSegments) {
		this.returnSegments = returnSegments;
	}

	public boolean isInternational() {
		return isInternational;
	}

	public void setInternational(boolean isInternational) {
		this.isInternational = isInternational;
	}
	
	public boolean isDepartCheckin(){
		Date today = new Date();
		for (int i = 0; i <departSegments.size(); i++){
			String dateS = departSegments.get(i).getDateSTDCHECK()+"+0800";
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ssZ");
			String dateSTD = returnSegments.get(i).getDataSTD()+"0800";
			try {
				Date date = format.parse(dateS);
				Date dateD = format.parse(dateSTD);
				if (today.after(date) && today.before(dateD)){
					LogHelper.debug("Check in depart available");
					return true;
				}
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		return false;
	}
	
	public boolean isReturnCheckin(){
		Date today = new Date();
		for (int i = 0; i <returnSegments.size(); i++){
			String dateS = returnSegments.get(i).getDateSTDCHECK()+"+0800";
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ssZ");
			String dateSTD = returnSegments.get(i).getDataSTD()+"0080";
			try {
				Date date = format.parse(dateS);
				Date dateD = format.parse(dateSTD);
				if (today.after(date) && today.before(dateD)){
					LogHelper.debug("Check in return available");
					return true;
				}
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return false;
	}

	public FareTypeModel getDepartFare() {
		return departFare;
	}

	public void setDepartFare(FareTypeModel departFare) {
		this.departFare = departFare;
	}

	public FareTypeModel getReturnFare() {
		return returnFare;
	}

	public void setReturnFare(FareTypeModel returnFare) {
		this.returnFare = returnFare;
	}

	public HashMap<String, List<FareTaxesModel>> getDepartTaxes() {
		return departTaxes;
	}
	
	public void setTaxesPrice(boolean isReturn){

		HashMap<String, List<FareTaxesModel>> bookTax;
		if (!isReturn){
			bookTax = departTaxes;
		}else{
			bookTax = returnTaxes;
		}
		
		if (bookTax == null){
			return;
		}
		
		if (bookTax.containsKey("ADT")){
			LogHelper.debug("Has Adult Taxt");
			List<FareTaxesModel> adultTaxes = bookTax.get("ADT");
			for (int i = 0; i < adultTaxes.size(); i ++){
				double price =	adultTaxes.get(i).getAmount() * adultCount;
				FareTaxesModel item = new FareTaxesModel();
				item.setTitle(adultTaxes.get(i).getTitle());
				item.setKey(adultTaxes.get(i).getKey());
				item.setAmount(price);
				updateTotalTax(isReturn, item);
			}
		}
		
		if (bookTax.containsKey("CHD")){
			List<FareTaxesModel> childTax = bookTax.get("CHD");
			for (int i = 0; i < childTax.size(); i ++){
				double price =	childTax.get(i).getAmount() * childCount;
				FareTaxesModel item = new FareTaxesModel();
				item.setTitle(childTax.get(i).getTitle());
				item.setKey(childTax.get(i).getKey());
				item.setAmount(price);
				updateTotalTax(isReturn, item);
			}
		}
		
		if (bookTax.containsKey("INF")){
			List<FareTaxesModel> infTax = bookTax.get("INF");
			for (int i = 0; i < infTax.size(); i ++){
				double price =	infTax.get(i).getAmount() * childCount;
				FareTaxesModel item = new FareTaxesModel();
				item.setTitle(infTax.get(i).getTitle());
				item.setKey(infTax.get(i).getKey());
				item.setAmount(price);
				updateTotalTax(isReturn, item);
			}
		}
		
	}

	public void updateTotalTax(boolean isReturn, FareTaxesModel item){
		List<FareTaxesModel> list;
		if (!isReturn){
			list = departTotalTax;
		}else{
			list = returnTotalTax;
		}
		boolean update = false;
		for (int i = 0; i < list.size(); i ++){
			if (list.get(i).getKey().equalsIgnoreCase(item.getKey())){
				update = true;
				double price = list.get(i).getAmount() + item.getAmount();
				list.get(i).setAmount(price);
				break;
			}
		}
		if (update == false){
			list.add(item);
		}
	}
	
	public void setDepartTaxes(HashMap<String, List<FareTaxesModel>> departTaxes) {
		this.departTaxes = departTaxes;
	}

	public HashMap<String, List<FareTaxesModel>> getReturnTaxes() {
		return returnTaxes;
	}

	public void setReturnTaxes(HashMap<String, List<FareTaxesModel>> returnTaxes) {
		this.returnTaxes = returnTaxes;
	}

	public int getAdultCount() {
		return adultCount;
	}

	public void setAdultCount(int adultCount) {
		this.adultCount = adultCount;
	}

	public int getChildCount() {
		return childCount;
	}

	public void setChildCount(int childCount) {
		this.childCount = childCount;
	}

	public int getInfantCount() {
		return infantCount;
	}

	public void setInfantCount(int infantCount) {
		this.infantCount = infantCount;
	}

	public List<FareTaxesModel> getDepartTotalTax() {
		return departTotalTax;
	}

	public void setDepartTotalTax(List<FareTaxesModel> departTotalTax) {
		this.departTotalTax = departTotalTax;
	}

	public List<FareTaxesModel> getReturnTotalTax() {
		return returnTotalTax;
	}

	public void setReturnTotalTax(List<FareTaxesModel> returnTotalTax) {
		this.returnTotalTax = returnTotalTax;
	}
	
	public String getTotalBaggagePrice(){
		String result = "0.00";
		double total = 0.00;
		if (passengerInfo != null && passengerInfo.size()>0){
			for (int i = 0; i< passengerInfo.size(); i++){
				total+= passengerInfo.get(i).getTotalBaggageCharge();
			}
		}
		
		result = String.valueOf(total);
		
		return result;
	}
	
	public void generateEmptyMeal(){
		for (int i = 0; i<departSegments.size(); i++){
			for (int psgIndex = 0 ; psgIndex < passengerInfo.size(); psgIndex++){
				passengerInfo.get(psgIndex).addMeal(new ArrayList<SSRItemModel>(), true);
				LogHelper.debug("Meal Depart empty add " + i);
			}
		}
		
		if (returnSegments!=null && returnSegments.size()>0){
			for (int i = 0; i<returnSegments.size(); i++){
				for (int psgIndex = 0 ; psgIndex < passengerInfo.size(); psgIndex++){
					passengerInfo.get(psgIndex).addMeal(new ArrayList<SSRItemModel>(), false);
					LogHelper.debug("Meal Return empty add " + i);
				}
			}
		}
		
	}
	
}
