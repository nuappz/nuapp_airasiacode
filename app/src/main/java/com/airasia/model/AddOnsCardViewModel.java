package com.airasia.model;

/**
 * Created by NUappz-5 on 7/29/2015.
 */
public class AddOnsCardViewModel {
    private String addOnListName;
    private String addOnDescription;

    public AddOnsCardViewModel(String addOnListName, String addOnDescription, String rate) {
        this.addOnListName = addOnListName;
        this.addOnDescription = addOnDescription;
        this.rate = rate;
    }

    AddOnsCardViewModel() {
        addOnListName = new String();
        addOnDescription = new String();
        rate = new String();

    }

    public String getAddOnDescription() {
        return addOnDescription;
    }

    public void setAddOnDescription(String addOnDescription) {
        this.addOnDescription = addOnDescription;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    private String rate;


    public String getAddOnListName() {
        return addOnListName;
    }

    public void setAddOnListName(String addOnListName) {
        this.addOnListName = addOnListName;
    }
}