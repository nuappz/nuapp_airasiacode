package com.airasia.model;


public class CurrencyModel {

	String countryCode = "";
	
	float price;

	public String getFromCurrency() {
		return countryCode;
	}

	public void setCurrencyCountry(String _countryCode) {
		this.countryCode = _countryCode;
	}

	public float getPrice() {
		return price;
	}

	public void setPrice(float price) {
		this.price = price;
	}
	
	
	
	
}
