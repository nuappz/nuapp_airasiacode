package com.airasia.dialog;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;


import com.airasia.adapter.SeatAssignAdapter;
import com.airasia.mobile.R;
import com.airasia.model.SeatAssignListModel;

import java.util.ArrayList;

/**
 * Created by NUappz-5 on 7/27/2015.
 */
public class SeatMapAssignDialog extends Dialog {

    Context context;
    ListView list_view_list;

    EditText oldPassword, newPassword, confirmPassword;
    ArrayList<SeatAssignListModel> data = new ArrayList<SeatAssignListModel>();
    SeatAssignAdapter adapter;
    Button button1;



    RelativeLayout rlProgress;

    public SeatMapAssignDialog(Context ctx) {
        super(ctx);
        this.context = ctx;
        // TODO Auto-generated constructor stub
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.asign_seat);
        list_view_list =(ListView)findViewById(R.id.customerNameList);
        button1 = (Button) findViewById(R.id.assignButton);
        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
              dismiss();
            }
        });
        setList();
        list_view_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapter, View v, int position,
                                    long arg3) {
                View view = list_view_list.getChildAt(position);
                int desiredBackgroundColor = Color.rgb(255, 48, 48);
                ColorDrawable viewColor = (ColorDrawable) view.getBackground();
                if (viewColor == null) {
//                    Intent i = new Intent(SeatAssignActivity.this, SeatAssignActivity.class);
//                    startActivity(i);
                    Toast.makeText(context, "" + position, Toast.LENGTH_SHORT).show();
                    view.setBackgroundColor(desiredBackgroundColor);
                    return;
                }
                int currentColorId = viewColor.getColor();
                if (currentColorId == desiredBackgroundColor) {
                    view.setBackgroundColor(Color.TRANSPARENT);
                } else {
//                    Intent i = new Intent(SeatAssignActivity.this, SeatAssignActivity.class);
//                    startActivity(i);
                    Toast.makeText(context, "" + position, Toast.LENGTH_SHORT).show();
                    view.setBackgroundColor(desiredBackgroundColor);
                }

                list_view_list.getChildAt(position).setBackgroundColor(Color.RED);
                Toast.makeText(context, "" + position, Toast.LENGTH_SHORT).show();

            }
        });

    }

       /* oldPassword = (EditText) findViewById(R.id.editTextOldPassword);
        newPassword = (EditText) findViewById(R.id.editTextNewPassword);
        confirmPassword = (EditText) findViewById(R.id.editTextConfirmPassword);
        rlProgress = (RelativeLayout) findViewById(R.id.rlProgress);
        save = (FButton) findViewById(R.id.buttonSave);
        preference = new MySharedPreference(context);
        cd = new ConnectionDetector(context);
        oldPassword.setText("");
        newPassword.setText("");
        confirmPassword.setText("");*/


    public void setList() {
        data.clear();
        data.add(new SeatAssignListModel("Lim Soo Ling"));
        data.add(new SeatAssignListModel("Harina Elaine kaur A/P Harji Singh"));
        data.add(new SeatAssignListModel("Koo Liew Xing"));
        data.add(new SeatAssignListModel("Harina Elaine"));
        data.add(new SeatAssignListModel("Lim Soo Ling"));
        data.add(new SeatAssignListModel("Harji Singh"));
        adapter = new SeatAssignAdapter(context, R.layout.assign_seat_content, data);
        list_view_list.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }
}