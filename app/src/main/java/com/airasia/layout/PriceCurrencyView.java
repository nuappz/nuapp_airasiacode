package com.airasia.layout;

import com.airasia.mobile.R;

import android.content.Context;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.widget.TextView;

public class PriceCurrencyView extends TextView {

	public PriceCurrencyView(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
	}

	public PriceCurrencyView(Context context, AttributeSet attrs) {
		super(context, attrs);
		if (!isInEditMode()) {
			UiUtil.setCustomFont(this, context, attrs,
					R.styleable.com_airasia_mobile_FontableTextView,
					R.styleable.com_airasia_mobile_FontableTextView_font);
		}
	}

	public PriceCurrencyView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		if (!isInEditMode()) {
			UiUtil.setCustomFont(this, context, attrs,
					R.styleable.com_airasia_mobile_FontableTextView,
					R.styleable.com_airasia_mobile_FontableTextView_font);
		}
	}
	
	@Override
	protected void onFocusChanged(boolean focused, int direction,
			Rect previouslyFocusedRect) {
		if (focused)
			super.onFocusChanged(focused, direction, previouslyFocusedRect);
	}

	@Override
	public void onWindowFocusChanged(boolean focused) {
		if (focused)
			super.onWindowFocusChanged(focused);
	}

	@Override
	public boolean isFocused() {
		return true;
	}
	
}
