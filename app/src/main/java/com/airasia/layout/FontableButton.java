package com.airasia.layout;

import com.airasia.mobile.R;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.Button;


public class FontableButton extends Button {
	public FontableButton(Context context) {
		super(context);
	}

	public FontableButton(Context context, AttributeSet attrs) {
		super(context, attrs);
		if (!isInEditMode()) {
			UiUtil.setCustomFont(this, context, attrs,
					R.styleable.com_airasia_mobile_FontableButton,
					R.styleable.com_airasia_mobile_FontableButton_font);
		}
	}

	public FontableButton(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		if (!isInEditMode()) {
			UiUtil.setCustomFont(this, context, attrs,
					R.styleable.com_airasia_mobile_FontableButton,
					R.styleable.com_airasia_mobile_FontableButton_font);
		}
	}
}
