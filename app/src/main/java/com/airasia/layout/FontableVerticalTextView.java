package com.airasia.layout;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.text.TextPaint;
import android.util.AttributeSet;
import android.view.Gravity;
import android.widget.TextView;

import com.airasia.mobile.R;


public class FontableVerticalTextView extends TextView {
	final boolean topDown;

	public FontableVerticalTextView(Context context) {
		super(context);
		final int gravity = getGravity();
		if (Gravity.isVertical(gravity)
				&& (gravity & Gravity.VERTICAL_GRAVITY_MASK) == Gravity.BOTTOM) {
			setGravity((gravity & Gravity.HORIZONTAL_GRAVITY_MASK)
					| Gravity.TOP);
			topDown = false;
		} else {
			topDown = true;
		}
	}

	public FontableVerticalTextView(Context context, AttributeSet attrs) {
		super(context, attrs);
		if (!isInEditMode()) {
			UiUtil.setCustomFont(this, context, attrs,
					R.styleable.com_airasia_mobile_FontableTextView,
					R.styleable.com_airasia_mobile_FontableTextView_font);
		}
		final int gravity = getGravity();
		if (Gravity.isVertical(gravity)
				&& (gravity & Gravity.VERTICAL_GRAVITY_MASK) == Gravity.BOTTOM) {
			setGravity((gravity & Gravity.HORIZONTAL_GRAVITY_MASK)
					| Gravity.TOP);
			topDown = false;
		} else {
			topDown = true;
		}
	}

	public FontableVerticalTextView(Context context, AttributeSet attrs,
			int defStyle) {
		super(context, attrs, defStyle);
		if (!isInEditMode()) {
			UiUtil.setCustomFont(this, context, attrs,
					R.styleable.com_airasia_mobile_FontableTextView,
					R.styleable.com_airasia_mobile_FontableTextView_font);
		}
		final int gravity = getGravity();
		if (Gravity.isVertical(gravity)
				&& (gravity & Gravity.VERTICAL_GRAVITY_MASK) == Gravity.BOTTOM) {
			setGravity((gravity & Gravity.HORIZONTAL_GRAVITY_MASK)
					| Gravity.TOP);
			topDown = false;
		} else {
			topDown = true;
		}
	}

	@Override
	protected void onFocusChanged(boolean focused, int direction,
			Rect previouslyFocusedRect) {
		if (focused)
			super.onFocusChanged(focused, direction, previouslyFocusedRect);
	}

	@Override
	public void onWindowFocusChanged(boolean focused) {
		if (focused)
			super.onWindowFocusChanged(focused);
	}

	@Override
	public boolean isFocused() {
		return true;
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		super.onMeasure(heightMeasureSpec, widthMeasureSpec);
		setMeasuredDimension(getMeasuredHeight(), getMeasuredWidth());
	}

	@Override
	protected void onDraw(Canvas canvas) {
		TextPaint textPaint = getPaint();
		textPaint.setColor(getCurrentTextColor());
		textPaint.drawableState = getDrawableState();

		canvas.save();

		if (topDown) {
			canvas.translate(getWidth(), 0);
			canvas.rotate(90);
		} else {
			canvas.translate(0, getHeight());
			canvas.rotate(-90);
		}

		canvas.translate(getCompoundPaddingLeft(), getExtendedPaddingTop());

		getLayout().draw(canvas);
		canvas.restore();
	}
}
