package com.airasia.fragment;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;

import com.airasia.mobile.R;

public class FragmentBase extends Fragment{

	int mPos = -1;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
	}

	@SuppressLint("InflateParams") @Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		
		mPos = getArguments().getInt("type");
		
		if (mPos == -1 && savedInstanceState != null)
			mPos = savedInstanceState.getInt("mPos");
		switch(mPos){
		case FragmentCase.FRAG_LOGIN_LOGIN:
			return (RelativeLayout) inflater
					.inflate(R.layout.fragment_login, null);
		case FragmentCase.FRAG_LOGIN_POPUP:
			return (RelativeLayout) inflater
					.inflate(R.layout.fragment_login_popup, null);
		case FragmentCase.FRAG_LOGIN_FORGET:
			return (LinearLayout) inflater
					.inflate(R.layout.fragment_forget_password, null);
		case FragmentCase.FRAG_SIGNUP_PERSONAL:
			return (LinearLayout) inflater
					.inflate(R.layout.signup_personal, null);
		case FragmentCase.FRAG_SIGNUP_CONTACT:
			return (LinearLayout) inflater
					.inflate(R.layout.signup_contact, null);
		case FragmentCase.FRAG_SIGNUP_LOGIN:
			return (ScrollView) inflater
					.inflate(R.layout.fragment_signup_container, null);
		case FragmentCase.FRAG_BOOK_FLIGHTS:
			return (LinearLayout) inflater
					.inflate(R.layout.fragment_booking_flight, null);
		case FragmentCase.FRAG_USER_PROFILE:
			return (LinearLayout) inflater
					.inflate(R.layout.fragment_user_profile, null);
		case FragmentCase.FRAG_HOME:
			return (RelativeLayout) inflater
					.inflate(R.layout.fragment_main, null);
		default:
			return (RelativeLayout) inflater
					.inflate(R.layout.fragment_main, null);
		}
	}

}
