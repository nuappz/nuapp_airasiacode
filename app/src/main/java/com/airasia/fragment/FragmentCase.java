package com.airasia.fragment;

public class FragmentCase {

	public static final int FRAG_SIGNUP_LOGIN = 0;
	public static final int FRAG_SIGNUP_PERSONAL = 1;
	public static final int FRAG_SIGNUP_CONTACT = 2;
	public static final int FRAG_SIGNUP_CONFIRMATION = 3;
	public static final int FRAG_LOGIN_LOGIN = 4;
	public static final int FRAG_LOGIN_FORGET = 5;
	public static final int FRAG_HOME = 6;
	public static final int FRAG_BOOK_FLIGHTS = 7;
	public static final int FRAG_CHECK_IN = 8;
	public static final int FRAG_FLIGHT_STATUS = 9;
	public static final int FRAG_MANAGE_BOOKING = 10;
	public static final int FRAG_HOTEL_DEALS = 11;
	public static final int FRAG_SETTINGS = 12;
	public static final int FRAG_USER_PROFILE = 13;
	public static final int FRAG_INFO_PAGE = 14;
	public static final int FRAG_MY_DOCUMENT = 15;
	public static final int FRAG_MY_BOOKING = 16;
	public static final int FRAG_MY_FREN = 17;
	public static final int FRAG_LOGIN_POPUP = 18;
	public static final int FRAG_DRAWER_BANNER = 19;
	public static final int FRAG_CHECKIN_BOOKING = 20;
	public static final int FRAG_CHECKIN_TNC = 21;
	public static final int FRAG_CHECKIN_DETAIL = 22;
	public static final int FRAG_LOGIN_CHECKIN = 23;
	public static final int FRAG_FARE_DISPLAY = 24;
	public static final int FRAG_FARE_LIST =25;
	public static final int FRAG_FARE_SUMMARY = 26;
	public static final int FRAG_GUEST_DETAILS = 27;
	public static final int FRAG_SSR_LISTING = 28;
	public static final int FRAG_SSR_BAGGAGE = 29;
	public static final int FRAG_SSR_MEAL = 30;
	public static final int FRAG_SSR_SEAT = 31;
	public static final int FRAG_SSR_INSURANCE = 32;
	public static final int FRAG_SSR_ASSISTANCE = 33;
	public static final int FRAG_PAYMENT_SUMMARY = 34;
	public static final int FRAG_BOARDING_PASS = 35;
	
}
