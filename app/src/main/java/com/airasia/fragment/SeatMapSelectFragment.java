package com.airasia.fragment;


import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.widget.GridLayout;
import android.widget.HorizontalScrollView;
import android.widget.ScrollView;
import android.widget.TableLayout;
import android.widget.TableRow;

import android.widget.TableRow.LayoutParams;
import android.widget.TextView;
import android.widget.Toast;

import com.airasia.dialog.SeatMapAssignDialog;
import com.airasia.dialog.SeatMapRejectDialog;
import com.airasia.layout.GridLayoutView;
import com.airasia.layout.GridLayoutView.OnToggledListener;
import com.airasia.mobile.R;
import com.nhaarman.supertooltips.ToolTip;
import com.nhaarman.supertooltips.ToolTipRelativeLayout;
import com.nhaarman.supertooltips.ToolTipView;


/**
 * Created by NUappz1 on 7/26/2015.
 */
public class SeatMapSelectFragment extends
        Fragment
{//implements OnToggledListener {

    /* GridLayoutView[] myViews;

     GridLayout myGridLayout;*/
    SeatMapAssignDialog dialog;
    SeatMapRejectDialog dialog1;


    int status[][];
    int i = 0;
    int j = 0;
    View v;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_seatmapselectpick, container, false);

        String[] column/*row*/ = {"A", "B", "C", "", "D", "E", "F", "", "G", "H", "I", ""};
        String[] row/*column*/ = {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13"};
        int rl = row.length + 1;
        int cl = column.length + 1;
        status = new int[rl][cl];
        //seatmap_tooltip
        ScrollView sv = (ScrollView) v.findViewById(R.id.scrollView_seatMap);
        TableLayout tableLayout = createTableLayout(row, column, rl, cl);
        sv.addView(tableLayout);
        dialog = new SeatMapAssignDialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog1 = new SeatMapRejectDialog(getActivity());
        dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);


        return v;
    }

    private TableLayout createTableLayout(String[] rv, String[] cv, int rowCount, int columnCount) {
        // 1) Create a tableLayout and its params
        TableLayout.LayoutParams tableLayoutParams = new TableLayout.LayoutParams();
        //tableLayoutParams.set
        TableLayout tableLayout = new TableLayout(getActivity());
        //tableLayout.setBackgroundColor(Color.WHITE);
        tableLayout.setBackgroundColor(getResources().getColor(R.color.gray_title));

        // 2) create tableRow params
        TableRow.LayoutParams tableRowParams = new TableRow.LayoutParams();
        tableRowParams.setMargins(2, 2, 2, 2);
        tableRowParams.weight = 1;

        for (/*int */i = 0; i < rowCount; i++) {
            // 3) create tableRow
            TableRow tableRow = new TableRow(getActivity());
            for (/*int */j = 0; j < columnCount; j++) {
                // 4) create textView
                final TextView textView = new TextView(getActivity());
                textView.setHeight(60);
                textView.setWidth(60);
                //  textView.setText(String.valueOf(j));
                // textView.setBackgroundColor(Color.WHITE);
                textView.setBackgroundColor(getResources().getColor(R.color.gray_data));
                textView.setBackgroundDrawable(getResources().getDrawable(R.drawable.user_gray));
                textView.setGravity(Gravity.CENTER);
                String s1 = Integer.toString(i);
                String s2 = Integer.toString(j);
                if (i == 0 && j == 0) {
                    textView.setBackgroundColor(getResources().getColor(R.color.gray_title));
                } else if (j == 4 || j == 8 || j == 12) {
                    textView.setBackgroundColor(getResources().getColor(R.color.gray_title));
                    textView.setWidth(15);
                } else if (i == 0) {
                    textView.setText(cv[j - 1]);
                    textView.setBackgroundColor(getResources().getColor(R.color.gray_title));
                } else if (j == 0) {
                    textView.setText(rv[i - 1]);
                    textView.setBackgroundColor(getResources().getColor(R.color.gray_title));
                } else {
                    String s3 = cv[j - 1] + rv[i - 1];
                    textView.setBackgroundColor(getResources().getColor(R.color.gray_data));
                    status[i - 1][j - 1] = 0;
                    textView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (status[i - 1][j - 1] == 0) {
                                textView.setBackgroundDrawable(getResources().getDrawable(R.drawable.u1));
                                status[i - 1][j - 1] = 1;
                                dialog.show();
                            } else if (status[i - 1][j - 1] == 1) {
                                textView.setBackgroundDrawable(getResources().getDrawable(R.drawable.u2));
                                status[i - 1][j - 1] = 0;
                                dialog1.show();
                            }
                        }
                    });
                }

                // 5) add textView to tableRow
                tableRow.addView(textView, tableRowParams);
            }

            // 6) add tableRow to tableLayout
            tableLayout.addView(tableRow, tableLayoutParams);
        }

     return tableLayout;
    }
}



