package com.airasia.fragment;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.airasia.mobile.R;
import com.airasia.model.BookingInfoModel;
import com.airasia.model.SSRMainModel;
import com.androidquery.AQuery;

public class PaymentFragment extends Fragment {
	
	BookingInfoModel booking;
	SSRMainModel ssrMain;
	AQuery aq;
	
	public static PaymentFragment newInstance(SSRMainModel _ssrMain, 
			BookingInfoModel _booking) {
		PaymentFragment f = new PaymentFragment();
		f.booking = _booking;
		f.ssrMain = _ssrMain;
		return f;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View v = inflater.inflate(R.layout.fragment_payment, null);
		
		aq = new AQuery(v);
		
		return v;
	}
	
	
	
}
