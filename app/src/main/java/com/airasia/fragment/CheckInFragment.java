package com.airasia.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.method.ScrollingMovementMethod;
import android.text.style.ClickableSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.airasia.mobile.LoginActivity;
import com.airasia.mobile.MainActivity;
import com.airasia.mobile.R;
import com.airasia.model.BookingInfoModel;
import com.airasia.util.LogHelper;
import com.androidquery.AQuery;

public class CheckInFragment extends Fragment{

	AQuery aq;
	int type;
	BookingInfoModel item;
	
	public static final String BOOKING_ID = "id"
			, BOOKING_NAME = "lname"
			, BOOKING_STATION = "station";
	
	public static CheckInFragment newInstance(int _type) {
		CheckInFragment f = new CheckInFragment();
		f.type = _type;
//		f.setArguments(b);
		return f;
	}
	
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
	}

	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		
		RelativeLayout layout;

		
		if(type == FragmentCase.FRAG_CHECK_IN){
			LogHelper.debug("Check in");
			layout = (RelativeLayout) inflater.inflate(R.layout.fragment_expandablelist, null);
		}else if (type == FragmentCase.FRAG_CHECKIN_TNC){
			layout = (RelativeLayout) inflater.inflate(R.layout.fragment_check_in_tnc, null);
		}else if (type == FragmentCase.FRAG_LOGIN_CHECKIN){
			layout = (RelativeLayout) inflater.inflate(R.layout.fragment_login_check_in, null);
		}else{
			layout = (RelativeLayout) inflater.inflate(R.layout.fragment_check_in_unlogin, null);
		}
		
		aq = new AQuery(layout);
		if(type == FragmentCase.FRAG_CHECK_IN){
			aq.id(R.id.expandable_container).getView().setBackgroundColor(getActivity().getResources().getColor(R.color.bg_color));
		}else if (type == FragmentCase.FRAG_CHECKIN_TNC){
			aq.id(R.id.check_in_tnc_text).getTextView().setMovementMethod(new ScrollingMovementMethod());
		}else if (type == FragmentCase.FRAG_LOGIN_CHECKIN){
			
		}else{
			aq.id(R.id.checkin_departure).getButton().setText("Kuala Lumpur (KUL)");
			aq.id(R.id.checkin_departure).getButton().setTag("KUL");
			setupSpannableLogin();
		}
		
		return layout;
	}
	
	ClickableSpan clickableSpan = new ClickableSpan() {
		
		@Override
		public void onClick(View widget) {
			// TODO Auto-generated method stub
			widget.setFocusable(false);
			widget.setSelected(false);
			widget.setActivated(false);
			TextView tv = (TextView) widget;
			
			tv.setHighlightColor(getActivity().getResources().getColor(android.R.color.transparent));
			Intent i = new Intent(getActivity(), LoginActivity.class);
			Bundle extra = new Bundle();
			extra.putInt("frag_type", 1);
			i.putExtras(extra);
			startActivity(i);
			
		}

		@Override
		public void updateDrawState(TextPaint ds) {
			// TODO Auto-generated method stub
			super.updateDrawState(ds);
		}
		
	};
	
	
	public void setupSpannableLogin(){
		aq.id(R.id.checkin_login);
		String text = getResources().getString(R.string.check_in_login_text);
		SpannableString spannable = new SpannableString(text);
		spannable.setSpan(clickableSpan, text.length()-11, text.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
		aq.id(R.id.checkin_login).text(spannable);
		aq.id(R.id.checkin_login).getTextView().setMovementMethod(LinkMovementMethod.getInstance());
	}

	@Override
	public void onDestroyView() {
		// TODO Auto-generated method stub
		super.onDestroyView();
	}
	
	
	
	

}
