package com.airasia.fragment;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.Header;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;

import com.airasia.adapter.FlightFareAdapter;
import com.airasia.holder.PrefsHolder;
import com.airasia.mobile.MainActivity;
import com.airasia.mobile.R;
import com.airasia.model.FareTypeModel;
import com.airasia.model.FlightSearchModel;
import com.airasia.model.SearchInfoModel;
import com.airasia.util.CustomHttpClient;
import com.airasia.util.JSonHelper;
import com.airasia.util.LogHelper;
import com.androidquery.AQuery;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

public class FareListingFragment extends Fragment implements View.OnClickListener{

	AQuery aq;
	Handler handler;
	SearchInfoModel item;
	int type = 0;
	int fragPos = 0;
	String selectedId = "";
	Context c;
	boolean isReturn, isEdit = false;
	ProgressDialog progress;
	
	public static FareListingFragment newInstance(SearchInfoModel _item
						, boolean _isReturn
						, Context _c
						, int position
						, String _selectedId
						, boolean _isEdit) {
		FareListingFragment f = new FareListingFragment();
		f.isReturn = _isReturn;
		f.item = _item;
		f.c = _c;
		f.selectedId = _selectedId;
		f.fragPos = position;
		f.isEdit = _isEdit;
		return f;
	}
	
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view;
		
		view = inflater.inflate(R.layout.fragment_flight_details, null);
		aq = new AQuery(view);
		
		if (handler == null){
			handler = new Handler();
		}
		
		progress = ProgressDialog.show(getActivity(), "", getActivity().getResources().getString(R.string.loading));
		
		handler.postDelayed(viewUpdate, 500);

		return view;
		
	}
	
	Runnable viewUpdate = new Runnable() {
		
		@Override
		public void run() {
			// TODO Auto-generated method stub
			setupList();
		}
	};
	
	void setupList(){
		
		List<FareTypeModel> itemList = new ArrayList<FareTypeModel>();
		FlightSearchModel model = item.getFlightList().get(fragPos);
		LogHelper.debug("Setup List At Page " +  model.getDateyyyyMMdd());
		if (!isReturn){
			if (model.hasDepart){
				itemList = model.getDepartFareList(0);
				closeDialog();
			}else{
				if (!model.isOutRange){
					connSearchFlight(fragPos);
				}else{
					closeDialog();
				}
			}
		}else{
			if (model.hasReturn){
				itemList = model.getReturnFareList(0);
				closeDialog();
			}else{
				if (!model.isOutRange){
					connSearchFlight(fragPos);
				}else{
					closeDialog();
				}
			}
		}
		
		initListAdapter(itemList);
		aq.id(R.id.flight_low_fare).getButton().setOnClickListener(this);
		aq.id(R.id.flight_business_fare).getButton().setOnClickListener(this);
		aq.id(R.id.flight_premium_fare).getButton().setOnClickListener(this);
		checkButtonAvailable();
	}
	
	public void closeDialog(){
		if (progress!=null){
			progress.dismiss();
			LogHelper.debug("Dialog Close" + fragPos);
		}
		LogHelper.debug("Dialog Close for Frag " + fragPos);
	}
	
	void initListAdapter(List<FareTypeModel> itemList){
		FlightFareAdapter fareAdap = new FlightFareAdapter(aq.id(R.id.fare_list).getView(),
				c, itemList, selectedId);
		aq.id(R.id.fare_list).getListView().setAdapter(fareAdap);
		aq.id(R.id.fare_list).getListView().setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int pos,
					long arg3) {
				// TODO Auto-generated method stub
				LogHelper.debug("Return View Click");

				
				
				FareTypeModel fare = new FareTypeModel();
				FareTypeModel upgradeFare = new FareTypeModel();
				if (!isReturn){
					fare = item.getFlightList()
							.get(fragPos)
							.getDepartFareList(type)
							.get(pos);
					upgradeFare = item.getFlightList()
							.get(fragPos)
							.getDepartUpgrade(type, pos);
				}else{
					fare = item.getFlightList()
							.get(fragPos)
							.getReturnFareList(type)
							.get(pos);
					upgradeFare = item.getFlightList()
							.get(fragPos)
							.getReturnUpgrade(type, pos);
				}
				
				if (isReturn){
					boolean timeCorrect = MainActivity.act.checkFligthDate(fare, fragPos, pos);
					LogHelper.debug("Time correct " + timeCorrect);
					if (!timeCorrect){
						return;
					}
				}
				
											
				
				if (item.getReturnList() != null &&
						item.getReturnList().size()>0 &&
						item.getSelectReturnDate().length()>0){
					if (!isReturn && !isEdit){
						MainActivity.act.setupSelectedFlight(fare.getId(),
								item,
								fragPos,
								fare,
								upgradeFare,
								true);
						MainActivity.act.setupReturnFlight();
					}else{
						if (isReturn){
							MainActivity.act.setupSelectedFlight(fare.getId(),
									item,
									fragPos,
									fare,
									upgradeFare,
									false);
							MainActivity.act.switchFragment(FragmentCase.FRAG_FARE_SUMMARY);
						}else{
							MainActivity.act.setupSelectedFlight(fare.getId(),
									item,
									fragPos,
									fare,
									upgradeFare,
									true);
							MainActivity.act.switchFragment(FragmentCase.FRAG_FARE_SUMMARY);
						}
						
					}
				}else{
					LogHelper.debug("Update Depart Fligth");
					MainActivity.act.setupSelectedFlight(fare.getId(),
							item,
							fragPos,
							fare,
							upgradeFare,
							true);
					MainActivity.act.switchFragment(FragmentCase.FRAG_FARE_SUMMARY);
				}
			}
			
			
		});
	}
	
	void changeList(){
		List<FareTypeModel> itemList = item.getFlightList().get(fragPos).getDepartFareList(type);
		FlightFareAdapter adapter = (FlightFareAdapter) aq.id(R.id.fare_list).getListView().getAdapter();
		if (type == 0){
			adapter.isPromo = true;
		}else{
			adapter.isPromo = false;
		}
		adapter.changeList(itemList);
		
	}
	
	
	@Override
	public void onClick(View v) {
	 //do what you want to do when button is clicked
	    switch (v.getId()) {
	        case R.id.flight_low_fare:
	        	type = 0;
	        	changeList();
	            break;
	        case R.id.flight_business_fare:
	        	type = 1;
	        	changeList();
	            break;
	        case R.id.flight_premium_fare:
	        	type = 2;
	        	changeList();
	        	break;
        	default:
        		break;
	    }
	}
	
	public void checkButtonAvailable(){
		FlightSearchModel mFlight = item.getFlightList().get(fragPos);
		boolean isHadFlight = false;
		if (!isReturn){
			if (mFlight.getDepartFlight() == null || mFlight.getDepartFlight().size()<=0){
				aq.id(R.id.flight_fare_container).gone();
				return;
			}
			
			if (mFlight.getDepartFareList(0).size()>0){
				aq.id(R.id.flight_low_fare).visible();
				isHadFlight = true;
			}else{
				aq.id(R.id.flight_low_fare).gone();
			}
			if(mFlight.getDepartFareList(1).size() >0){
				aq.id(R.id.flight_business_fare).visible();
				isHadFlight = true;
			}else{
				aq.id(R.id.flight_business_fare).gone();
			}
			if(mFlight.getDepartFareList(2).size()>0){
				aq.id(R.id.flight_premium_fare).visible();
				isHadFlight = true;
			}else{
				aq.id(R.id.flight_premium_fare).gone();
			}
		}else{
			if (mFlight.getReturnFlight() == null || mFlight.getReturnFlight().size()<=0){
				aq.id(R.id.flight_fare_container).gone();
				return;
			}
			if (mFlight.getReturnFareList(0).size()>0){
				aq.id(R.id.flight_low_fare).visible();
				isHadFlight = true;
			}else{
				aq.id(R.id.flight_low_fare).gone();
			}
			if(mFlight.getReturnFareList(1).size() >0){
				aq.id(R.id.flight_business_fare).visible();
				isHadFlight = true;
			}else{
				aq.id(R.id.flight_business_fare).gone();
			}
			if(mFlight.getReturnFareList(2).size()>0){
				aq.id(R.id.flight_premium_fare).visible();
				isHadFlight = true;
			}else{
				aq.id(R.id.flight_premium_fare).gone();
			}
		}
		
		if(isHadFlight==true){
			aq.id(R.id.flight_fare_container).visible();
		}else{
			aq.id(R.id.flight_fare_container).gone();
		}
	}
	
	private void connSearchFlight(final int pos){
	
		SharedPreferences prefs = c.getSharedPreferences(PrefsHolder.PREFS, Context.MODE_PRIVATE);
		RequestParams params = new RequestParams();
		params.put("departureStation", item.getDepartStation());
		params.put("arrivalStation", item.getArrivalStation());
		params.put("departureDate", item.getFlightList().get(pos).getDateyyyyMMdd());
		if (item.isReturn()){
			params.put("returnDate", item.getFlightList().get(pos).getDateyyyyMMdd());
		}
		params.put("adultPax", ""+item.getAdultCount());
		params.put("childPax", ""+item.getChildCount());
		params.put("infantPax", ""+item.getInfantCount());
		params.put("promoCode", item.getPromoCode());
		params.put("userCurrencyCode", item.getUserCurrency());
		params.put("userSession", prefs.getString(PrefsHolder.SESSION_TICKET, ""));
		
		CustomHttpClient.postWithHash(CustomHttpClient.GET_AVAILABILITY, params, new AsyncHttpResponseHandler() {
	
			@Override
			public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
				// TODO Auto-generated method stub
				String response = new String(responseBody);
				int responseCode = JSonHelper.GetResponseStatus(response);
				LogHelper.debug("Response Flight code " + responseCode);
				if (responseCode == JSonHelper.SUCCESS_REPONSE){
					item = JSonHelper.parseFlight(response
										, item.getFlightList().get(pos).getDateyyyyMMdd()
										, item.getFlightList().get(pos).getDateyyyyMMdd()
										, item);
					checkButtonAvailable();
					changeList();
				}else{
					String errorMsg = JSonHelper.GetResponseMessage(response);
					MainActivity.showErrorMessage(getActivity(), errorMsg);
				}
			
			}
			
			@Override
			public void onFailure(int statusCode, Header[] headers,
					byte[] responseBody, Throwable error) {
				// TODO Auto-generated method stub
//				MainActivity.showErrorMessage(getActivity(), c.getString(R.string.connection_error));
			}
			
			@Override
			public void onFinish() {
				// TODO Auto-generated method stub\
				
				FlightFareAdapter adapter = (FlightFareAdapter) aq.id(R.id.fare_list).getListView().getAdapter();
				adapter.notifyDataSetChanged();

				closeDialog();
				super.onFinish();
			}
			
			@Override
			public void onStart() {
				// TODO Auto-generated method stub
				super.onStart();
			}
		
		});
	
	}
	
}
