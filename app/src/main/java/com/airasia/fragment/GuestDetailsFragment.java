package com.airasia.fragment;

import java.util.List;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.airasia.adapter.GuestListingAdapter;
import com.airasia.mobile.R;
import com.airasia.model.MemberInfoModel;
import com.airasia.model.SelectedFlightModel;
import com.airasia.util.LogHelper;
import com.androidquery.AQuery;

public class GuestDetailsFragment extends Fragment{

	Handler handler = new Handler();
	AQuery aq;
	SelectedFlightModel item; 
	List<MemberInfoModel> mList;
	boolean isLogin;
	
	public static GuestDetailsFragment newInstance(SelectedFlightModel _item,
					List<MemberInfoModel> _list,
					boolean _isLogin) {
		GuestDetailsFragment f = new GuestDetailsFragment();
		f.item = _item;
		f.mList = _list;
		f.isLogin = _isLogin;
		return f;
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view;
		
		view = inflater.inflate(R.layout.fragment_guest_list, null);
		
		aq = new AQuery(view);
		aq.id(R.id.activity_list_container).background(R.color.light_grey);
		aq.id(R.id.list_view_container).background(android.R.color.transparent);
		if (handler == null){
			handler = new Handler();
		}
		
		handler.postDelayed(viewUpdate, 1000);

		return view;
		
	}
	
	Runnable viewUpdate = new Runnable() {
		
		@Override
		public void run() {
			// TODO Auto-generated method stub
			setupView();
		}
	};
	
	public void setupView(){
		LogHelper.debug("Init list Size " + mList.size());
		GuestListingAdapter gAdapter = new GuestListingAdapter(aq.id(R.id.list_view_container).getView(),
										getActivity(),
										mList,
										isLogin);
		aq.id(R.id.list_view_container).getListView().setAdapter(gAdapter);
	}
	
}
