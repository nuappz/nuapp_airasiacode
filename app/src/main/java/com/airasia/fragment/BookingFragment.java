package com.airasia.fragment;

import java.util.ArrayList;
import java.util.List;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnGroupClickListener;
import android.widget.HorizontalScrollView;

import com.airasia.adapter.FarePagerAdapter;
import com.airasia.adapter.UpgradeFlightAdapter;
import com.airasia.holder.ConstantHolder;
import com.airasia.mobile.R;
import com.airasia.model.FareClassUpgrade;
import com.airasia.model.FlightSearchModel;
import com.airasia.model.SearchInfoModel;
import com.airasia.model.SegmentModel;
import com.airasia.model.SelectedFlightModel;
import com.airasia.model.StationModel;
import com.airasia.util.ConstantHelper;
import com.airasia.util.LogHelper;
import com.airasia.util.SQLhelper;
import com.androidquery.AQuery;

public class BookingFragment extends Fragment{

	int type, currentPos = 1;
	SearchInfoModel searchItem;
	AQuery aq;
	Handler handler;
	boolean isReturn = false, isEditMode = false;
	ViewPager pager;
	String selectedId = "", selectedDate = "";
	SelectedFlightModel selectItem;
	
	
	public static BookingFragment newInstance(int _type) {
		BookingFragment f = new BookingFragment();
		f.type = _type;
		return f;
	}
	
	public static BookingFragment newInstance(int _type,
								SearchInfoModel _item,
								int _isReturn,
								int pos,
								String _selectedId,
								String _selectedDate) {
		BookingFragment f = new BookingFragment();
		f.searchItem = _item;
		f.type = _type;
		f.selectedId = _selectedId;
		f.selectedDate = _selectedDate;
		f.isReturn = (_isReturn == 1);
		LogHelper.debug("Create Bookign f " + f.isReturn);
		f.currentPos = pos;
		return f;
	}
	
	public static BookingFragment newInstance(int _type,
			SearchInfoModel _item,
			int _isReturn,
			int pos,
			String _selectedId,
			String _selectedDate,
			boolean editMode) {
		BookingFragment f = newInstance(_type, _item, _isReturn, pos, _selectedId, _selectedDate);
		f.isEditMode = editMode;
		return f;
	}
	
	public static BookingFragment newInstance(int _type, SelectedFlightModel _selectItem) {
		BookingFragment f = new BookingFragment();
		f.selectItem = _selectItem;
		f.type = _type;
		return f;
	}
	
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view;
		
		switch(type){
		case FragmentCase.FRAG_FARE_DISPLAY:
			view = inflater.inflate(R.layout.fragment_fare_display, null);
			break;
		case FragmentCase.FRAG_FARE_SUMMARY:
			view = inflater.inflate(R.layout.fragment_fare_summary, null);
			break;
		default:
			view = inflater.inflate(R.layout.fragment_booking_flight, null);
			break;
		}
		
		aq = new AQuery(view);
		
		if (handler == null){
			handler = new Handler();
		}
		
		handler.postDelayed(viewUpdate, 500);

		return view;
		
	}
	
	Runnable viewUpdate = new Runnable() {
		
		@Override
		public void run() {
			// TODO Auto-generated method stub
			setupFareDisplay();
		}
	};
	
	private void setupFareDisplay(){

		switch(type){
		case FragmentCase.FRAG_FARE_DISPLAY:

			SQLhelper db = new SQLhelper(getActivity());
			LogHelper.debug("setup Listing");
			setPager();
			List<String> journeyList = new ArrayList<String>();
			if (!isReturn){
				journeyList = searchItem.getDepartList();
				aq.id(R.id.fare_depart_word).text(R.string.depart_word);
			}else{
				journeyList = searchItem.getReturnList();
				aq.id(R.id.fare_depart_word).text(R.string.return_word);
			}
			
			
			StationModel mStation;
			if (journeyList != null && journeyList.size()<3){
				aq.id(R.id.fare_journey_no_join).visible();
				aq.id(R.id.fare_journey_join).gone();
				
				mStation = db.getStationBaseKey(journeyList.get(0), 0);
				aq.id(R.id.fare_depart).text(mStation.getKey());
				aq.id(R.id.fare_depart_station).text(mStation.getStationName());
				
				mStation = db.getStationBaseKey(journeyList.get(1), 0);
				aq.id(R.id.fare_return).text(mStation.getKey());
				aq.id(R.id.fare_return_station).text(mStation.getStationName());
				
			}else{
				aq.id(R.id.fare_journey_no_join).gone();
				aq.id(R.id.fare_journey_join).visible();
				if(journeyList != null ){
					aq.id(R.id.fare_destination_1).text(journeyList.get(0));
					aq.id(R.id.fare_destination_2).text(journeyList.get(1));
					aq.id(R.id.fare_destination_3).text(journeyList.get(2));
				}
			}
			
			View v = aq.id(R.id.fare_date_container).getView();
			HorizontalScrollView scrollView = (HorizontalScrollView) aq.id(R.id.fare_date_scroll).getView();
			scrollView.scrollTo(v.getWidth()/5, 0);
			scrollView.setOnTouchListener( new OnTouchListener() {

	            @Override
	            public boolean onTouch(View v, MotionEvent event) 
	            {
	                  return true;
	            }
			});

			setHorizontalDateView(currentPos);
			mStation = null;

			db.close();
			break;
		case FragmentCase.FRAG_FARE_SUMMARY:
			setupSummaryDepart();
			setupSummaryReturn();
			break;
		default:
			break;
		}

		
	}
	
	private void setupSummaryDepart(){

		SQLhelper db = new SQLhelper(getActivity());
		StationModel summaryStation;
		if (selectItem.departName != null && selectItem.departName.size()<=3){
			aq.id(R.id.summary_depart_summary).visible();
			aq.id(R.id.fare_journey_no_join).visible();
			aq.id(R.id.fare_journey_join).gone();

			double fare = selectItem.getDepartTotal(0);
			LogHelper.debug("Journey Fare " + fare); 
			String fareTotal = String.valueOf(fare);
			String[] fareSplit = fareTotal.split("\\.");
			aq.id(R.id.summary_d_currency).text(selectItem.currency);
			aq.id(R.id.summary_d_dollar).text(fareSplit[0]);
			aq.id(R.id.summary_d_sen).text(fareSplit[1]);
			
			summaryStation = db.getStationBaseKey(selectItem.departName.get(0), 0);
			LogHelper.debug("Summary Sation Key " + summaryStation.getKey());
			aq.id(R.id.summary_d_name_header).text(summaryStation.getKey());
			aq.id(R.id.summary_d_name).text(summaryStation.getKey());
			aq.id(R.id.summary_d_station_header).text(summaryStation.getStationName());
			aq.id(R.id.summary_d_time).text(ConstantHelper.gethhmmFromDateString(selectItem.departModel.getSegList().get(0).getDataSTD()));
			SegmentModel mSeg = selectItem.departModel.getSegList().get(0);
			aq.id(R.id.summary_d_estimate)
							.text(ConstantHelper.calculateEstimateTime(mSeg.getDataSTD(), mSeg.getDateSTA(), ConstantHolder.PATTERN_YMDTHMS));
			aq.id(R.id.summary_d_num).text(mSeg.getFlgithNum());
			
			int upgradeType = selectItem.upgradeDepart.getFareType();
			LogHelper.debug("Depart Upgrade Fare Type " + upgradeType);
			FareClassUpgrade upgradeD = db.getFareUpgrade(upgradeType);
			if (upgradeType != 0 
					&& upgradeD.getBenefits() != null 
					&& upgradeD.getBenefits().size()>0){

				double upgardeCost = selectItem.getDepartTotal(1);
				String textCost = "0.00";
				if (upgardeCost>fare){
					upgardeCost = upgardeCost - fare;
					textCost = "+"+String.valueOf(upgardeCost);
				}else if (upgardeCost == fare){
					upgardeCost = 0.00;
				}else {
					upgardeCost = fare - upgardeCost;
					textCost = "-"+String.valueOf(upgardeCost);
				}
				
				
				
				aq.id(R.id.depart_upgrade).visible();
				UpgradeFlightAdapter aUpgrade = new UpgradeFlightAdapter(getActivity(),
						aq.id(R.id.depart_upgrade).getView(), 
						upgradeD,
						textCost,
						selectItem.currency);
				aq.id(R.id.depart_upgrade).getExpandableListView().setAdapter(aUpgrade);
				aq.id(R.id.depart_upgrade).getExpandableListView().setOnGroupClickListener(new OnGroupClickListener() {
				
					@Override
					public boolean onGroupClick(ExpandableListView parent, View v,
					int groupPosition, long id) {
						// TODO Auto-generated method stub
						LogHelper.debug("Group Header Click");
						UpgradeFlightAdapter aUpgrade 
							= (UpgradeFlightAdapter) aq.id(R.id.depart_upgrade).getExpandableListView().getExpandableListAdapter();
						if (aUpgrade.isSelected){
							aUpgrade.isSelected = false;
							selectItem.setUpgradeDepart(false);
						}else{
							aUpgrade.isSelected = true;
							selectItem.setUpgradeDepart(true);
						}
						aUpgrade.notifyDataSetChanged();
						
					
						return false;
					}
				});
			}else{
				aq.id(R.id.depart_upgrade).gone();
			}
			
			
			
			if (selectItem.departName.size()==3){

				aq.id(R.id.summary_c_container).visible();
				aq.id(R.id.fare_c_aero).visible();
				
				summaryStation = db.getStationBaseKey(selectItem.departName.get(1), 0);
				aq.id(R.id.summary_c_name_header).text(summaryStation.getKey());
				aq.id(R.id.summary_c_station_header).text(summaryStation.getStationName());
				aq.id(R.id.summary_a_name).text(summaryStation.getKey());
				aq.id(R.id.summary_a_time).text(ConstantHelper.gethhmmFromDateString(selectItem.departModel.getSegList().get(0).getDateSTA()));
				aq.id(R.id.summary_d_name_2).text(summaryStation.getKey());
				aq.id(R.id.summary_d_time_2).text(ConstantHelper.gethhmmFromDateString(selectItem.departModel.getSegList().get(1).getDataSTD()));
				
				
				mSeg = selectItem.departModel.getSegList().get(1);
				aq.id(R.id.summary_d_estimate_2)
								.text(ConstantHelper.calculateEstimateTime(mSeg.getDataSTD(), mSeg.getDateSTA(), ConstantHolder.PATTERN_YMDTHMS));
				aq.id(R.id.summary_d_num_2).text(mSeg.getFlgithNum());
				
				summaryStation = db.getStationBaseKey(selectItem.departName.get(2), 0);
				aq.id(R.id.summary_r_name_header).text(summaryStation.getKey());
				aq.id(R.id.summary_r_station_header).text(summaryStation.getStationName());
				aq.id(R.id.summary_a_time_2).text(ConstantHelper.gethhmmFromDateString(selectItem.departModel.getSegList().get(1).getDateSTA()));
				aq.id(R.id.summary_a_name_2).text(summaryStation.getKey());
			}else{
				aq.id(R.id.summary_c_container).gone();
				aq.id(R.id.fare_c_aero).gone();
				aq.id(R.id.summary_continous_divider).gone();
				aq.id(R.id.summary_continous_container).gone();
				
				summaryStation = db.getStationBaseKey(selectItem.departName.get(1), 0);
				aq.id(R.id.summary_r_name_header).text(summaryStation.getKey());
				aq.id(R.id.summary_r_station_header).text(summaryStation.getStationName());
				aq.id(R.id.summary_a_name).text(summaryStation.getKey());
				aq.id(R.id.summary_a_time).text(ConstantHelper.gethhmmFromDateString(selectItem.departModel.getSegList().get(0).getDateSTA()));
			}
			
		}
		db.close();
	}
	
	private void setupSummaryReturn(){

		SQLhelper db = new SQLhelper(getActivity());
		StationModel summaryStation;
		if (selectItem.returnName != null && selectItem.returnName.size()>0){
			aq.id(R.id.summary_return_container).visible();
			

			double fare = selectItem.getReturnTotal(0);
			LogHelper.debug("Journey Fare " + fare); 
			String fareTotal = String.valueOf(fare);
			String[] fareSplit = fareTotal.split("\\.");
			aq.id(R.id.summary_r_currency).text(selectItem.currency);
			aq.id(R.id.summary_r_dollar).text(fareSplit[0]);
			aq.id(R.id.summary_r_sen).text(fareSplit[1]);
			
			
			summaryStation = db.getStationBaseKey(selectItem.returnName.get(0), 0);
			aq.id(R.id.summary_d_name_header_r).text(summaryStation.getKey());
			aq.id(R.id.summary_d_name_r).text(summaryStation.getKey());
			aq.id(R.id.summary_d_station_header_r).text(summaryStation.getStationName());
			aq.id(R.id.summary_d_time_r).text(ConstantHelper.gethhmmFromDateString(selectItem.returnModel.getSegList().get(0).getDataSTD()));
			SegmentModel mSeg = selectItem.returnModel.getSegList().get(0);
			aq.id(R.id.summary_d_estimate_r)
							.text(ConstantHelper.calculateEstimateTime(mSeg.getDataSTD(), mSeg.getDateSTA(), ConstantHolder.PATTERN_YMDTHMS));
			aq.id(R.id.summary_d_num_r).text(mSeg.getFlgithNum());
			
			int upgradeType = selectItem.upgradeReturn.getFareType();
			FareClassUpgrade upgradeD = db.getFareUpgrade(upgradeType);
			if (upgradeType != 0 
					&& upgradeD.getBenefits()!=null
					&& upgradeD.getBenefits().size()>0){

				double upgardeCost = selectItem.getDepartTotal(1);
				upgardeCost = upgardeCost - fare;
				
			
				UpgradeFlightAdapter aUpgrade = new UpgradeFlightAdapter(getActivity(),
													aq.id(R.id.return_upgrade).getView(), 
													upgradeD,
													String.valueOf(upgardeCost),
													selectItem.currency);
				aq.id(R.id.return_upgrade).getExpandableListView().setAdapter(aUpgrade);
				aq.id(R.id.return_upgrade).getExpandableListView().setOnGroupClickListener(new OnGroupClickListener() {
					
					@Override
					public boolean onGroupClick(ExpandableListView parent, View v,
							int groupPosition, long id) {
						// TODO Auto-generated method stub
						UpgradeFlightAdapter aUpgrade 
							= (UpgradeFlightAdapter) aq.id(R.id.return_upgrade).getExpandableListView().getExpandableListAdapter();
						if (aUpgrade.isSelected){
							aUpgrade.isSelected = false;
							selectItem.setUpgradeReturn(false);
						}else{
							aUpgrade.isSelected = true;
							selectItem.setUpgradeReturn(true);
						}
						return false;
					}
				});
			}else{
				aq.id(R.id.return_upgrade).gone();
			}
			
			
			if (selectItem.departName.size()==3){

				aq.id(R.id.summary_c_container_r).visible();
				aq.id(R.id.fare_c_aero_r).visible();
				
				summaryStation = db.getStationBaseKey(selectItem.returnName.get(1), 0);
				aq.id(R.id.summary_c_name_header_r).text(summaryStation.getKey());
				aq.id(R.id.summary_a_name_r).text(summaryStation.getKey());
				aq.id(R.id.summary_c_station_header_r).text(summaryStation.getStationName());
				aq.id(R.id.summary_a_time_r).text(ConstantHelper.gethhmmFromDateString(selectItem.returnModel.getSegList().get(0).getDateSTA()));
				aq.id(R.id.summary_d_name_2_r).text(summaryStation.getKey());
				aq.id(R.id.summary_d_time_2_r).text(ConstantHelper.gethhmmFromDateString(selectItem.returnModel.getSegList().get(1).getDataSTD()));
				
				mSeg = selectItem.departModel.getSegList().get(1);
				aq.id(R.id.summary_d_estimate_2_r)
								.text(ConstantHelper.calculateEstimateTime(mSeg.getDataSTD(), mSeg.getDateSTA(), ConstantHolder.PATTERN_YMDTHMS));
				aq.id(R.id.summary_d_num_2_r).text(mSeg.getFlgithNum());
				
				summaryStation = db.getStationBaseKey(selectItem.returnName.get(2), 0);
				aq.id(R.id.summary_r_name_header_r).text(summaryStation.getKey());
				aq.id(R.id.summary_r_station_header_r).text(summaryStation.getStationName());
				aq.id(R.id.summary_a_time_2_r).text(ConstantHelper.gethhmmFromDateString(selectItem.returnModel.getSegList().get(1).getDateSTA()));
				aq.id(R.id.summary_a_name_2_r).text(summaryStation.getKey());
			}else{
				aq.id(R.id.summary_c_container_r).gone();
				aq.id(R.id.fare_c_aero_r).gone();
				aq.id(R.id.summary_continous_divider_r).gone();
				aq.id(R.id.summary_continous_container_r).gone();
				
				summaryStation = db.getStationBaseKey(selectItem.returnName.get(1), 0);
				aq.id(R.id.summary_r_name_header_r).text(summaryStation.getKey());
				aq.id(R.id.summary_r_station_header_r).text(summaryStation.getStationName());
				aq.id(R.id.summary_a_name_r).text(summaryStation.getKey());
				aq.id(R.id.summary_a_time_r).text(ConstantHelper.gethhmmFromDateString(selectItem.returnModel.getSegList().get(0).getDateSTA()));
			}
			
		}else{
			aq.id(R.id.summary_return_container).gone();
		}
		db.close();
	}
	
	private void setHorizontalDateView(int pos){
		

		String lowFare = "0.00";
		
		if ((pos-2) >= 0){
			aq.id(R.id.fare_date_1).text(searchItem.getFlightList().get(pos-2).getDateEEEddMM());
			lowFare = getLowFare(pos-2);
			setupScrollItem(R.id.sfare_currency_1,
					R.id.sfare_dollar_1,
					R.id.sfare_sen_1,
					searchItem.getUserCurrency(), 
					lowFare);
		}else{
			aq.id(R.id.fare_date_1).text("");
			aq.id(R.id.sfare_currency_1).text("");
			aq.id(R.id.sfare_sen_1).text("");
			aq.id(R.id.sfare_dollar_1).text("");
		}
		
		aq.id(R.id.fare_date_2).text(searchItem.getFlightList().get(pos-1).getDateEEEddMM());
		lowFare = getLowFare(pos-1);
		setupScrollItem(R.id.sfare_currency_2,
				R.id.sfare_dollar_2,
				R.id.sfare_sen_2,
				searchItem.getUserCurrency(), 
				lowFare);
		
		
		if (isReturn){
			lowFare = searchItem.getFlightList().get(pos).getLowestFareReturn();
		}else{
			lowFare = searchItem.getFlightList().get(pos).getLowestFareDepart();
		}
		
		aq.id(R.id.sfare_date_3).text(searchItem.getFlightList().get(pos).getDateEEEddMM());
		setupScrollItem(R.id.sfare_currency_3, R.id.sfare_dollar_3, R.id.sfare_sen_3,
							searchItem.getUserCurrency(),
							lowFare);
		
		
		aq.id(R.id.fare_date_4).text(searchItem.getFlightList().get(pos+1).getDateEEEddMM());
		lowFare = getLowFare(pos+1);
		setupScrollItem(R.id.sfare_currency_4,
				R.id.sfare_dollar_4,
				R.id.sfare_sen_4,
				searchItem.getUserCurrency(), 
				lowFare);
		
		if ((pos+2) <=searchItem.getFlightList().size()){
			aq.id(R.id.fare_date_5).text(searchItem.getFlightList().get(pos+2).getDateEEEddMM());
			lowFare = getLowFare(pos+1);
			setupScrollItem(R.id.sfare_currency_5,
					R.id.sfare_dollar_5,
					R.id.sfare_sen_5,
					searchItem.getUserCurrency(), 
					lowFare);
		}else{
			aq.id(R.id.fare_date_5).text("");
			aq.id(R.id.sfare_currency_5).text("");
			aq.id(R.id.sfare_sen_5).text("");
			aq.id(R.id.sfare_dollar_5).text("");
		}
	}
	
	public String getLowFare(int pos){
		String result = "0.00";
		if (isReturn){
			result = searchItem.getFlightList().get(pos).getLowestFareReturn();
		}else{
			result = searchItem.getFlightList().get(pos).getLowestFareDepart();
		}
		if (result == null || result.length()<=0){
			result = "0.00";
		}
		return result;
	}
	
	private void setupScrollItem(int currencyId, int dollarId, int senId, String currency, String _price){

		LogHelper.debug("LowFare " + _price);
		if (_price != null && _price.contains(".")){
			String[] price = _price.split("\\.");
			aq.id(currencyId).text(currency);
			aq.id(dollarId).text(price[0]);
			aq.id(senId).text(price[1]);
		}else{
			aq.id(currencyId).text(currency);
			aq.id(dollarId).text(_price);
			aq.id(senId).text("00");
		}
	}

	private void setPager(){
		pager = (ViewPager) aq.id(R.id.fare_view_pager).getView();
		FarePagerAdapter pagerAdap = new FarePagerAdapter(getActivity().getSupportFragmentManager(),
				getActivity(), searchItem, isReturn, selectedId, selectedDate, isEditMode);
		pager.setOffscreenPageLimit(1);
		pager.setAdapter(pagerAdap);
		
		pager.setCurrentItem(currentPos);
		
		
		pager.setOnPageChangeListener(new OnPageChangeListener() {
			
			int pagePos = 1;
			
			@Override
			public void onPageSelected(int pos) {
				// TODO Auto-generated method stub
				FlightSearchModel mFlight = searchItem.getFlightList().get(pos);
				if (mFlight.isOutRange){
					pager.setCurrentItem(pagePos);
				}else{
					pagePos = pos;
				}

				setHorizontalDateView(pagePos);
			}
			
			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void onPageScrollStateChanged(int arg0) {
				// TODO Auto-generated method stub
				
			}
		});
	}
	
	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		if (handler!=null){
			handler.removeCallbacks(viewUpdate);
		}
		super.onDestroy();
	}
	
	
	
	
	

}
