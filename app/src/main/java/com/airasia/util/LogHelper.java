package com.airasia.util;

import android.content.Context;
import android.util.Log;

public class LogHelper {

	public static final String TAG = "AIRASIAAPP";

	public static final int LEVEL_DEBUG = 0;
	public static final int LEVEL_INFO = 1;
	public static final int LEVEL_ERROR = 2;

	private static int logLevel = LEVEL_DEBUG;

	public static void setLogLevel(int level) {
		if (level > LEVEL_DEBUG || level < LEVEL_DEBUG) {
			logLevel = level;
		} else {
			logLevel = 0;
		}
	}

	public static void debug( String message) {
		if (logLevel <= LEVEL_DEBUG) {
			Log.d(TAG, "[DEBUG]" + message);
		}
	}
	
	public static void log(Context _context, String message) {
		if (logLevel <= LEVEL_DEBUG) {
			Log.d(TAG, "["+_context.getClass().getSimpleName()+"] " + message);
		}
	}

	public static void info(String message) {
		if (logLevel <= LEVEL_INFO) {
			Log.i(TAG, "[INFO]  " + message);
		}
	}

	public static void error(String message) {
		if (logLevel <= LEVEL_ERROR) {
			Log.e(TAG, "[ERROR] " + message);
		}
	}

}
