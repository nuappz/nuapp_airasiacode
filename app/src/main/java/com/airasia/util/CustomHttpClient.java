package com.airasia.util;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import android.content.Context;
import android.content.SharedPreferences;

import com.airasia.holder.ConstantHolder;
import com.airasia.holder.PrefsHolder;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

public class CustomHttpClient {

	public final static int GET_SETTING = 101;
	public final static int UPDATE_DEVICE = 102;
	public final static int GET_SESSION = 103;
	public final static int GEO_IP = 104;
	public final static int GET_BANNER = 105;
	public final static int GET_COUNTRY = 106;
	public final static int MEMBER_LOGIN = 201;
	public final static int MEMBER_LOGOUT = 202;
	public final static int FORGET_PASSWORD = 203;
	public final static int GET_FREN_FAMILY = 204;
	public final static int GET_TRAVEL_DOC = 205;
	public final static int MEMBER_SIGNUP = 206;
	public final static int GET_COUNTRY_STATES = 207;
	public final static int GET_STATION = 301;
	public final static int GET_AVAILABILITY = 302;
	public final static int SELL_JOURNEY = 303;
	public final static int GET_SSR = 304;
	public final static int GET_BOOKING = 308;
	public final static int GET_USER_BOOKING = 501;
	public final static int VERIFY_DOC = 502;
	public final static int UPDATE_DOC = 503;
	public final static int AUTO_ASSIGN_SEAT = 504;
	public final static int CHECK_IN = 505;
	
	public static Element convertElement(String _response){
		
		if(_response==null)
			_response = "";
		
		DocumentBuilder builder;
		Document doc = null;
		try {
			builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
			doc = builder.parse(new InputSource(new StringReader(_response)));
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Element element = null;
		if (doc != null)
			element = doc.getDocumentElement();
		return element;

	}
	
	private static AsyncHttpClient client = new AsyncHttpClient(true, 0, 0);

	
	public static void changeClient() {
		client = new AsyncHttpClient(false, 0, 0);
	}

	public static void get(int actionCode, RequestParams params,
			AsyncHttpResponseHandler responseHandler) {
		try {

			if (params == null)
				params = new RequestParams();
			params.put("action", String.valueOf(actionCode));

			client.setTimeout(ConstantHolder.DEFAULT_HTTP_TIMEOUT);
			client.setMaxRetriesAndTimeout(ConstantHolder.DEFAULT_HTTP_RETRY,
					ConstantHolder.DEFAULT_HTTP_TIMEOUT);

			client.get(ConstantHolder.URL, params, responseHandler);
		} catch (Exception ignored) {

		}
	}

	public static void post(Context c, int actionCode, RequestParams params,
			AsyncHttpResponseHandler responseHandler) {
		try {

			if (params == null)
				params = new RequestParams();
			params.put("action", String.valueOf(actionCode));
			params.put("operatingSystem", "android");
			
			LogHelper.debug("check action = "+actionCode);
			
			client.setTimeout(ConstantHolder.DEFAULT_HTTP_TIMEOUT);
			client.setMaxRetriesAndTimeout(ConstantHolder.DEFAULT_HTTP_RETRY,
					ConstantHolder.DEFAULT_HTTP_TIMEOUT);
			client.post(c, ConstantHolder.URL, params, responseHandler);
			
		} catch (Exception ignored) {

		}
	}

	public static boolean postWithHeader(Context c, String url,
			RequestParams params, Map<String, String> header,
			AsyncHttpResponseHandler responseHandler) {
		try {

			if (params == null)
				params = new RequestParams();
			client.setTimeout(ConstantHolder.DEFAULT_HTTP_TIMEOUT);
			client.setMaxRetriesAndTimeout(ConstantHolder.DEFAULT_HTTP_RETRY,
					ConstantHolder.DEFAULT_HTTP_TIMEOUT);
			for (Map.Entry<String, String> e : header.entrySet()) {
				client.addHeader(e.getKey(), e.getValue());
			}
			
			
			
			// client.addHeader("user_id", (String) header.get("user_id"));
			// client.addHeader("USER_DEVICE_WIDTH",
			// (String) header.get("USER_DEVICE_WIDTH"));
			// client.addHeader("user_device_type",
			// (String) header.get("user_device_type"));
			// client.addHeader("USER_TZ", (String) header.get("USER_TZ"));
			client.post(c, url, params, responseHandler);
			
			return true;
		} catch (Exception ignored) {
			return false;
		}

	}

	public static void autoRefreshCancel(Context c) {
		try {
			if (client != null) {
				client.cancelRequests(c, true);
			}
		} catch (Exception ignored) {

		}
	}

	public static void cancel(final Context c) {
		try {
			if (client != null) {
				client.cancelRequests(c, true);
			}
		} catch (Exception ignored) {

		}
	}
	
	//Widget
	public static void post(int actionCode, RequestParams params,
			AsyncHttpResponseHandler responseHandler) {
		try {

			if (params == null)
				params = new RequestParams();
			params.put("actionCode", String.valueOf(actionCode));
			params.put("operatingSystem", "android");
			params.put("cultureCode", ConstantHolder.lang);
			
			client.setMaxRetriesAndTimeout(ConstantHolder.DEFAULT_HTTP_RETRY,
					ConstantHolder.DEFAULT_HTTP_TIMEOUT);
			client.post(ConstantHolder.URL, params, responseHandler);
		} catch (Exception ignored) {

		}
	}
	
	public static void postWithHash(int actionCode, RequestParams params,
			AsyncHttpResponseHandler responseHandler) {
		try {

			if (params == null)
				params = new RequestParams();
			params.put("actionCode", String.valueOf(actionCode));
			params.put("operatingSystem", "android");
			params.put("cultureCode", ConstantHolder.lang);	
			
			params.put("username", "AAM_MOBILE_STG");
			params.put("password", "NgxtcYc5Hn");
			
			String hash = params.toString().replace("&", "");
			hash = ConstantHelper.sha256(hash+ConstantHolder.testKey);
			params.put("hashKey", hash);
			

			LogHelper.debug("hash check 2 = "+ params.toString());
			
			client.setMaxRetriesAndTimeout(ConstantHolder.DEFAULT_HTTP_RETRY,
					ConstantHolder.DEFAULT_HTTP_TIMEOUT);
			client.post(ConstantHolder.URL, params, responseHandler);
		} catch (Exception ignored) {

		}
	}
	
	public static void postWithHashWithoutServer(int actionCode, RequestParams params,
			AsyncHttpResponseHandler responseHandler) {
		try {

			if (params == null)
				params = new RequestParams();
			params.put("actionCode", String.valueOf(actionCode));
			params.put("operatingSystem", "android");
			params.put("cultureCode", ConstantHolder.lang);
			
			String hash = params.toString().replace("&", "");
			hash = ConstantHelper.sha256(hash+ConstantHolder.testKey);
			params.put("hashKey", hash);
			

			LogHelper.debug("hash check 1 = "+ params.toString());
			
			client.setMaxRetriesAndTimeout(ConstantHolder.DEFAULT_HTTP_RETRY,
					ConstantHolder.DEFAULT_HTTP_TIMEOUT);
			client.post(ConstantHolder.URL, params, responseHandler);
		} catch (Exception ignored) {

		}
	}
	
	public static void postWithNoPos(int actionCode, RequestParams params,
			AsyncHttpResponseHandler responseHandler) {
		try {

			if (params == null)
				params = new RequestParams();
			params.put("actionCode", String.valueOf(actionCode));
			params.put("operatingSystem", "android");
			params.put("cultureCode", ConstantHolder.lang);	
			
			params.put("username", "AAM_MOBILE_STG");
			params.put("password", "NgxtcYc5Hn");
			
			String hash = params.toString().replace("&", "");
			hash = ConstantHelper.sha256(hash+ConstantHolder.testKey);
			params.put("hashKey", hash);
			

			LogHelper.debug("hash check 2 = "+ params.toString());
			
//			client.setMaxRetriesAndTimeout(ConstantHolder.DEFAULT_HTTP_RETRY,
//					ConstantHolder.DEFAULT_HTTP_TIMEOUT);
//			client.post(ConstantHolder.URL, params, responseHandler);
		} catch (Exception ignored) {

		}
	}
	
	public static void post(String url,
			AsyncHttpResponseHandler responseHandler) {
		try {

			
			client.setMaxRetriesAndTimeout(ConstantHolder.DEFAULT_HTTP_RETRY,
					ConstantHolder.DEFAULT_HTTP_TIMEOUT);
			client.post(url, responseHandler);
		} catch (Exception ignored) {

		}
	}

	
}
