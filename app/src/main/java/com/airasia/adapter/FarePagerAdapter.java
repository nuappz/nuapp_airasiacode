package com.airasia.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.airasia.fragment.BookingFragment;
import com.airasia.fragment.FareListingFragment;
import com.airasia.model.FlightSearchModel;
import com.airasia.model.SearchInfoModel;

public class FarePagerAdapter extends FragmentStatePagerAdapter {
	 
    Context mContext;
    SearchInfoModel item;
    boolean isReturn = false;
    boolean isEdit = false;
    String selectedId = "", selectedDate = "";
    
    
    public FarePagerAdapter(FragmentManager fm, 
    				Context context, 
    				SearchInfoModel _item, 
    				boolean _isReturn,
    				String _selectedId,
    				String _selectedDate,
    				boolean _isEdit) {
        super(fm);
        mContext = context;
        item = _item;
        this.selectedDate = _selectedDate;
        selectedId = _selectedId;
        this.isReturn = _isReturn;
        this.isEdit = _isEdit;
    }

    @Override
    public Fragment getItem(int position) {

        // Create fragment object
    	Fragment fragment;
    	if (item.getFlightList().get(position).getDateyyyyMMdd().equals(selectedDate)){
    		fragment = FareListingFragment.newInstance(item, isReturn, mContext, position, selectedId, isEdit);
    	}else{
    		fragment = FareListingFragment.newInstance(item, isReturn, mContext, position, "", isEdit);
    	}
    	
        
        // Attach some data to the fragment
        // that we'll use to populate our fragment layouts
//        Bundle args = new Bundle();
//        args.putInt("page_position", position + 1);

        // Set the arguments on the fragment
        // that will be fetched in the
        // DemoFragment@onCreateView
//        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public int getCount() {
        return 420;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return "Page " + (position + 1);
    }
}

