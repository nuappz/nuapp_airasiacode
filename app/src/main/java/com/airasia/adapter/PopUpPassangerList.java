package com.airasia.adapter;

import java.util.List;

import android.R;
import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.airasia.holder.ViewHolder;
import com.airasia.model.MemberInfoModel;
import com.androidquery.AQuery;

public class PopUpPassangerList extends BaseAdapter{
	
	LayoutInflater inflator;
	List<MemberInfoModel> items;
	AQuery aq;
	ViewHolder.FrenFamilyHolder holder;
	
	
	public PopUpPassangerList(View v, Activity _act, List<MemberInfoModel> _items){
		this.items = _items;
		inflator = (LayoutInflater) _act.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		aq = new AQuery(v);
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return items.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return items.get(position);
		
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		View vi = convertView;
		if (vi == null){
			holder = new ViewHolder.FrenFamilyHolder();
			vi = inflator.inflate(android.R.layout.simple_list_item_1, null);
			holder.fName = (TextView) vi.findViewById(R.id.text1);
			vi.setTag(holder);
		}else{
			holder = (ViewHolder.FrenFamilyHolder) vi.getTag();
		}
		
		AQuery taq = this.aq.recycle(vi);
		
		taq.id(holder.fName).text(items.get(position).getFullName());
		
		return vi;
	}
	
	
	
	
}
