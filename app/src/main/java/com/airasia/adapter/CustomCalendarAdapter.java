package com.airasia.adapter;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import android.content.Context;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.airasia.mobile.R;
import com.airasia.model.CalendarModel;
import com.airasia.util.LogHelper;


public class CustomCalendarAdapter extends ArrayAdapter<CalendarModel> {

	List<Integer> cellList;
	
	Date startDate = new Date();
	Date endDate = new Date();
	
	Date pickedStartDate;
	Date pickedEndDate;
	
	Context contxt;
	
    public CustomCalendarAdapter(Context context, int textViewResourceId) {
    	
        super(context, textViewResourceId);
        
        contxt = context;
    }

    public CustomCalendarAdapter(Context context, int resource, List<CalendarModel> items) {
        super(context, resource, items);
        
        contxt = context;
    }
    
    public void updateAdapter(Date start, Date end, Date pickedStart, Date pickedEnd)
    {
    	startDate = start;
    	
    	if(startDate==null)
    	{
    		startDate = new Date();
    	}
    	
    	endDate = end;
    	
    	if(endDate==null)
    	{
    		endDate = new Date();
    	}
    	
    	pickedStartDate = pickedStart;
    	pickedEndDate = pickedEnd;
    	
    	this.notifyDataSetChanged();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = convertView;

        if (v == null) {
            LayoutInflater vi;
            vi = LayoutInflater.from(getContext());
            v = vi.inflate(R.layout.item_calendar_row, null);
            
            
        }
        
        if(cellList==null || cellList.size()==0)
        {
        	cellList = new ArrayList<Integer>();
        	cellList.add(R.id.cell0);
        	cellList.add(R.id.cell1);
        	cellList.add(R.id.cell2);
        	cellList.add(R.id.cell3);
        	cellList.add(R.id.cell4);
        	cellList.add(R.id.cell5);
        	cellList.add(R.id.cell6);
        	cellList.add(R.id.cell7);
        	cellList.add(R.id.cell8);
        	cellList.add(R.id.cell9);
        	cellList.add(R.id.cell10);
        	cellList.add(R.id.cell11);
        	cellList.add(R.id.cell12);
        	cellList.add(R.id.cell13);
        	cellList.add(R.id.cell14);
        	cellList.add(R.id.cell15);
        	cellList.add(R.id.cell16);
        	cellList.add(R.id.cell17);
        	cellList.add(R.id.cell18);
        	cellList.add(R.id.cell19);
        	cellList.add(R.id.cell20);
        	cellList.add(R.id.cell21);
        	cellList.add(R.id.cell22);
        	cellList.add(R.id.cell23);
        	cellList.add(R.id.cell24);
        	cellList.add(R.id.cell25);
        	cellList.add(R.id.cell26);
        	cellList.add(R.id.cell27);
        	cellList.add(R.id.cell28);
        	cellList.add(R.id.cell29);
        	cellList.add(R.id.cell30);
        	cellList.add(R.id.cell31);
        	cellList.add(R.id.cell32);
        	cellList.add(R.id.cell33);
        	cellList.add(R.id.cell34);
        	cellList.add(R.id.cell35);
        	cellList.add(R.id.cell36);
        	cellList.add(R.id.cell37);
        	cellList.add(R.id.cell38);
        	cellList.add(R.id.cell39);
        	cellList.add(R.id.cell40);
        	cellList.add(R.id.cell41);
        }
       

        CalendarModel item = getItem(position);

        if (item != null) {
        	
        	TextView header = (TextView) v.findViewById(R.id.monthYearTitle);
        	
        	header.setText(item.getMonthYear());
        	
        	
        	
        	Calendar calCounter = Calendar.getInstance();
        	calCounter.setTime(item.calDate);
        	
        	int counter = 0;
        	for(int i = 0; i<cellList.size();i++)
        	{
        		RelativeLayout cellLayout = (RelativeLayout)v.findViewById(cellList.get(i));
        		
        		Button button = (Button)cellLayout.findViewById(R.id.cellButton);
        		button.setBackgroundColor(contxt.getResources().getColor(android.R.color.transparent));
        		
        		if(i>=item.firstDayOfMonthDate && counter<item.numberOfDays)
        		{
        			
        			cellLayout.setVisibility(View.VISIBLE);
        			
                	
                	
                	button.setTag(calCounter.getTime());
                	
                	button.setText(String.valueOf(counter+1));
                	
                	
                	
                	
                	
                	
                	if((calCounter.getTime().equals(startDate) || calCounter.getTime().after(startDate)) && calCounter.getTime().before(endDate))
                	{
                		if(DateUtils.isToday(calCounter.getTimeInMillis()))
                    	{
                    		button.setTextColor(contxt.getResources().getColor(R.color.calendar_cell_font_today));
                    	}
                		else
                		{
                			button.setTextColor(contxt.getResources().getColor(R.color.calendar_cell_font_normal));
                		}
                		button.setEnabled(true);
                	}
                	else
                	{
                		button.setTextColor(contxt.getResources().getColor(R.color.calendar_cell_font_passed));
                		
                		button.setEnabled(false);
                	}
                	
                	if(pickedStartDate!=null || pickedEndDate != null)
                	{
                		
                		if (pickedStartDate!=null && pickedEndDate!=null && 
                				(calCounter.getTime().after(pickedStartDate)||calCounter.getTime().equals(pickedStartDate)) && 
                				(calCounter.getTime().before(pickedEndDate)||calCounter.getTime().equals(pickedEndDate))
                				) {
                			button.setTextColor(contxt.getResources().getColor(R.color.calendar_cell_font_normal_selected));
                			button.setBackgroundColor(contxt.getResources().getColor(R.color.calendar_selected_cell));
                        }
                        
                        if (pickedStartDate!=null && calCounter.getTime().equals(pickedStartDate)) {
                        	button.setTextColor(contxt.getResources().getColor(R.color.calendar_cell_font_normal_selected));
                        	button.setBackgroundColor(contxt.getResources().getColor(R.color.calendar_selected_cell));
                        }
                        
                        if (pickedEndDate!=null && calCounter.getTime().equals(pickedEndDate)) {
                        	button.setBackgroundColor(contxt.getResources().getColor(R.color.calendar_selected_cell));
                            button.setTextColor(contxt.getResources().getColor(R.color.calendar_cell_font_normal_selected));
                        }
                		
                	}
                	
                	calCounter.add(Calendar.DATE, 1);
                	counter++;
        		}
        		else
        		{
        			cellLayout.setVisibility(View.INVISIBLE);
        		}
        		
        	}
        	
        	int total = item.firstDayOfMonthDate+item.numberOfDays;
            
            LogHelper.debug("total = "+total);
            
            TableRow row = (TableRow)v.findViewById(R.id.cellRow5);
            
            if (total>35){
                
            	row.setVisibility(View.VISIBLE);
            }
            else
            {
            	row.setVisibility(View.GONE);
            }
        	
        	
//            TextView tt1 = (TextView) v.findViewById(R.id.id);
//            TextView tt2 = (TextView) v.findViewById(R.id.categoryId);
//            TextView tt3 = (TextView) v.findViewById(R.id.description);
//
//            if (tt1 != null) {
//                tt1.setText(p.getId());
//            }
//
//            if (tt2 != null) {
//                tt2.setText(p.getCategory().getId());
//            }
//
//            if (tt3 != null) {
//                tt3.setText(p.getDescription());
//            }
        }

        return v;
    }

}
