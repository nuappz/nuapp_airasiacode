package com.airasia.adapter;

import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.airasia.holder.ViewHolder;
import com.airasia.mobile.R;
import com.airasia.model.BookingInfoModel;
import com.airasia.model.CheckInModel;
import com.airasia.model.JourneyModel;
import com.airasia.model.MemberInfoModel;
import com.airasia.util.LogHelper;
import com.androidquery.AQuery;

public class BookingAdapter extends BaseExpandableListAdapter {

	BookingInfoModel item;
	LayoutInflater inflater;
	ViewHolder.BookingHolder holder;
	ViewHolder.BookingHeaderHolder headerHolder;
	AQuery aq;
	Activity act;
	
	public BookingAdapter(BookingInfoModel _item, Activity _act, View view){
		this.item = _item;
		inflater = (LayoutInflater) _act.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		aq = new AQuery(view);
		this.act = _act;
		
	}

	@Override
	public Object getChild(int groupPosition, int childPosition) {
		// TODO Auto-generated method stub
		return this.item.getMemberList().get(childPosition);
	}

	@Override
	public long getChildId(int groupPosition, int childPosition) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getChildView(int groupPosition, int childPosition,
			boolean isLastChild, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		View vi = convertView;
		if (vi == null){
			vi = inflater.inflate(R.layout.item_check_in_name, null);
			holder = new ViewHolder.BookingHolder();
			holder.userName = (TextView) vi.findViewById(R.id.checkin_name);
			holder.reminder = (TextView) vi.findViewById(R.id.checkin_item_reminder);
			holder.layout = (RelativeLayout) vi.findViewById(R.id.checkin_item_complete);
			holder.checkedIn = (ImageView) vi.findViewById(R.id.checkin_item_tick);
			holder.checkIn = false;
			vi.setTag(holder);
		}else{
			holder = (ViewHolder.BookingHolder) vi.getTag();
		}
		AQuery taq = this.aq.recycle(vi);
		MemberInfoModel model = item.getMemberList().get(childPosition);
		taq.id(holder.userName).getTextView().setText(model.getFullName());

		holder.checkIn = false;
		
		if (!model.isInfant()){
			List<CheckInModel> checkIn = model.getCheckinList().get(groupPosition);
			for (int i = 0; i < checkIn.size(); i ++){
				if(checkIn.get(i).getLiftStatus().equals("CheckedIn")){
					holder.checkIn = true;
				}
			}
			
			if(holder.checkIn){
				taq.id(holder.reminder).visible();
			}else if( (model.isReturn() && groupPosition ==1)
					|| (model.isDepart() && groupPosition==0)){
				taq.id(holder.checkedIn).image(R.drawable.info_complete_tick);
				taq.id(holder.reminder).gone();
				taq.id(holder.reminder).gone();
			}else{
				taq.id(holder.reminder).gone();
				taq.id(holder.checkedIn).image(0);
			}
			
		}
		return vi;
	}

	@Override
	public int getChildrenCount(int groupPosition) {
		// TODO Auto-generated method stub
		if (item.getMemberList().size() == 0){
			return 0;
		}
		
		if (groupPosition == 0 && !item.isDepartCheckin() || (!item.isReturnCheckin() && groupPosition==1)){
			return 0;
		}
		
		return item.getMemberList().size();
	}

	@Override
	public Object getGroup(int groupPosition) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int getGroupCount() {
		// TODO Auto-generated method stub
		return 2;
	}

	@Override
	public long getGroupId(int groupPosition) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getGroupView(int groupPosition, boolean isExpanded,
			View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		View vi = convertView;
		if (vi == null){
			vi = inflater.inflate(R.layout.item_check_in_header, null);
			headerHolder = new ViewHolder.BookingHeaderHolder();
			headerHolder.name = (TextView) vi.findViewById(R.id.expandable_name_header);
			headerHolder.textV1 = (TextView) vi.findViewById(R.id.check_in_location_1);
			headerHolder.textV2 = (TextView) vi.findViewById(R.id.check_in_location_2);
			headerHolder.textV3 = (TextView) vi.findViewById(R.id.check_in_location_3);
			headerHolder.image1 = (ImageView) vi.findViewById(R.id.check_in_location_divider);
			headerHolder.image2 = (ImageView) vi.findViewById(R.id.check_in_location_divider_2);
			vi.setTag(headerHolder);
		}else{
			headerHolder = (ViewHolder.BookingHeaderHolder) vi.getTag();
		}
		AQuery taq = this.aq.recycle(vi);
		
		List<JourneyModel> journeys;
		if (groupPosition == 0){
			journeys =item.getDepartJourney();
			aq.id(headerHolder.name).getTextView().setText(R.string.depart_word);
		}else{
			aq.id(headerHolder.name).getTextView().setText(R.string.return_word);
			journeys = item.getReturnJourney();
		}
		
		if (journeys.size()<3){
			taq.id(headerHolder.image2).gone();
			taq.id(headerHolder.textV3).gone();
		}else{
			taq.id(headerHolder.image2).visible();
			taq.id(headerHolder.textV3).visible();
			taq.id(headerHolder.textV3).text(journeys.get(2).getName());
		}

		taq.id(headerHolder.textV1).text(journeys.get(0).getName());
		taq.id(headerHolder.textV2).text(journeys.get(1).getName());
		
		return vi;
	}

	@Override
	public boolean hasStableIds() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {
		// TODO Auto-generated method stub
		return true;
	}
	
	public void updateInforComplete(int groupPos, int childPost){
		List<CheckInModel> model = this.item.getMemberList().get(childPost).getCheckinList().get(groupPos);
		boolean isCheckedIn = false;
		for (int i = 0; i < model.size(); i ++){
			if(model.get(i).getLiftStatus().equals("CheckedIn")){
				isCheckedIn = true;
			}
		}
		if (isCheckedIn == false){
			if(groupPos == 0){
				if(this.item.getMemberList().get(childPost).isDepart()){
					LogHelper.debug("Depart Deselect");
					this.item.getMemberList().get(childPost).setDepart(false);
				}else{
					LogHelper.debug("Depart Select");
					this.item.getMemberList().get(childPost).setDepart(true);
				}
			}else{
				LogHelper.debug("Return DeSelect");
				if(this.item.getMemberList().get(childPost).isReturn()){
					this.item.getMemberList().get(childPost).setReturn(false);
				}else{
					LogHelper.debug("return Select");
					this.item.getMemberList().get(childPost).setReturn(true);
				}
			}
			this.notifyDataSetChanged();
		}
	}
	
	public List<MemberInfoModel> getAdapterMembers(){
		return this.item.getMemberList();
	}

	@Override
	public int getGroupType(int groupPosition) {
		// TODO Auto-generated method stub
		if (groupPosition == 0 && item.isDepartCheckin() || (item.isReturnCheckin() && groupPosition==1)){
			return 1;
		}
		return 0;
	}
	

}
