package com.airasia.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.airasia.mobile.R;
import com.airasia.model.SeatRejectModel;

import java.util.ArrayList;

/**
 * Created by NUappz-5 on 7/27/2015.
 */
public class SeatRejectAdapter extends ArrayAdapter<SeatRejectModel> {
    Context context;
    TextView text;
    private ArrayList<SeatRejectModel> data;
    LayoutInflater lInflater;


    public SeatRejectAdapter(Context context, int resource,
                             ArrayList<SeatRejectModel> objects) {
        super(context, resource, objects);
        // TODO Auto-generated constructor stub
        this.context = context;
        this.data = objects;
        lInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return data.size();
    }

    @Override
    public SeatRejectModel getItem(int position) {
        // TODO Auto-generated method stub
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        View view = null;
        try {
            view = convertView;
            if (view == null) {
                view = lInflater.inflate(R.layout.seat_reject_content,
                        parent, false);
            }

            text = (TextView) view.findViewById(R.id.customerName);
            text.setText(data.get(position).getcName());

        } catch (Exception e) {
            e.printStackTrace();
        }

        return view;
    }


}
