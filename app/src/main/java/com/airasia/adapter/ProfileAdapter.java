package com.airasia.adapter;

import java.util.List;

import com.airasia.adapter.DrawerAdapter.ViewHolder;
import com.airasia.mobile.R;
import com.airasia.model.DrawerModel;
import com.androidquery.AQuery;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class ProfileAdapter extends BaseAdapter {

	List<DrawerModel> items;
	LayoutInflater inflater;
	ViewHolder holder;
	AQuery aq;
	
	public ProfileAdapter(List<DrawerModel> _items, Activity _act, View view){
		this.items = _items;
		inflater = (LayoutInflater) _act.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		aq = new AQuery(view);
	}
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return items.size();
	}

	@Override
	public Object getItem(int arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return 0;
	}
	
	
	public int getFragID(int pos){
		return this.items.get(pos).getPosition();
	}

	@Override
	public View getView(int pos, View view, ViewGroup arg2) {
		// TODO Auto-generated method stub
		View vi = view;
		if (vi == null){
			vi = inflater.inflate(R.layout.item_profile, null);
			holder = new ViewHolder();
			holder.title = (TextView) vi.findViewById(R.id.item_profile_text);
			vi.setTag(holder);
		}else{
			holder = (ViewHolder) vi.getTag();
		}
		AQuery taq = this.aq.recycle(vi);
		taq.id(holder.title).text(items.get(pos).getName());
		return vi;
	}
	
	class ViewHolder{
		TextView title;
	}
	

}
