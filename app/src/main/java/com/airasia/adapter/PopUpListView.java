package com.airasia.adapter;

import java.util.List;

import android.R;
import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.airasia.model.CountryModel;
import com.airasia.model.StationModel;
import com.androidquery.AQuery;

public class PopUpListView extends BaseAdapter{
	
	LayoutInflater inflator;
	List<CountryModel> countryItems;
	List<StationModel> stationItems;
	int type = 1;
	AQuery aq;
	ViewHolder holder;
	public static final int STATION = 1, COUNTRY = 2, FLIGHT_STATION = 3;
	
	public PopUpListView(View v, Activity _act, List<CountryModel> _items, int _type){
		this.countryItems = _items;
		inflator = (LayoutInflater) _act.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		this.type = _type;
		aq = new AQuery(v);
	}
	
	public PopUpListView(View v, List<StationModel> _items, Activity _act, int _type){
		this.stationItems = _items;
		inflator = (LayoutInflater) _act.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		this.type = _type;
		aq = new AQuery(v);
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		if (type == STATION || type == FLIGHT_STATION){
			return stationItems.size();
		}else{
			return countryItems.size();
		}
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		if (type == STATION || type == FLIGHT_STATION){
			return stationItems.get(position);
		}else{
			return countryItems.get(position);
		}
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		View vi = convertView;
		if (vi == null){
			holder = new ViewHolder();
			vi = inflator.inflate(android.R.layout.simple_list_item_1, null);
			holder.text1 = (TextView) vi.findViewById(R.id.text1);
			vi.setTag(holder);
		}else{
			holder = (ViewHolder) vi.getTag();
		}
		
		AQuery taq = this.aq.recycle(vi);
		
		if (type == 1){
			StationModel station = this.stationItems.get(position);
			taq.id(holder.text1).getTextView().setText(station.getStationName());
		}else if (type == FLIGHT_STATION){
			StationModel station_dis = this.stationItems.get(position);
			taq.id(holder.text1).getTextView().setText(station_dis.getStationName() + " (" + station_dis.getKey() + ")");
		}else{
			CountryModel country = this.countryItems.get(position);
			taq.id(holder.text1).getTextView().setText(country.getName());
		}
		
		
		return vi;
	}
	
	public class ViewHolder{
		public TextView text1;
	}
	
	public void updateCountry(List<CountryModel> items){
		this.countryItems = items;
		this.notifyDataSetChanged();
	}
	
	public void updateStation(List<StationModel> items){
//		this.stationItems.clear();
//		this.stationItems.addAll(items);
		this.stationItems = items;
		this.notifyDataSetChanged();
	}
	
	
	
}
