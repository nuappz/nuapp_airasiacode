package com.airasia.adapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.airasia.holder.SQLHolder;
import com.airasia.holder.ViewHolder;
import com.airasia.mobile.R;
import com.airasia.model.BookingInfoModel;
import com.airasia.model.FareTaxesModel;
import com.airasia.model.MemberInfoModel;
import com.airasia.model.SegmentModel;
import com.airasia.model.StationModel;
import com.airasia.util.LogHelper;
import com.airasia.util.SQLhelper;
import com.androidquery.AQuery;

public class PriceSummaryAdapter extends BaseAdapter {

	BookingInfoModel item;
	LayoutInflater inflater;
	AQuery aq;
	ViewHolder.PriceSummaryHolder holder;
	Activity act;
	
	public PriceSummaryAdapter(View _v, Activity _act, BookingInfoModel _item){
		this.item = _item;
		this.act = _act;
		inflater = (LayoutInflater) _act.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		aq = new AQuery(_v);
	}
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		if (item.getReturnJourney() != null && item.getReturnJourney().size()>0){
			return 2;
		}else{
			return 1;
		}
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		View v = convertView;
		if (v == null){
			holder = new ViewHolder.PriceSummaryHolder();
			v = inflater.inflate(R.layout.item_summary_body, null);
			
			holder.departN = (TextView) v.findViewById(R.id.item_summ_depart);
			holder.returnN = (TextView) v.findViewById(R.id.item_summ_return);
			holder.contN = (TextView) v.findViewById(R.id.item_summ_cont);
			holder.contFlight = (ImageView) v.findViewById(R.id.item_summ_cont_divider);
			
			holder.journeyView = (LinearLayout) v.findViewById(R.id.item_view_journey);
			holder.bigPView = (LinearLayout) v.findViewById(R.id.item_view_bigP);
			holder.addOnView = (LinearLayout) v.findViewById(R.id.item_view_addon);
			holder.passangerView = (LinearLayout) v.findViewById(R.id.item_view_passanger);
			holder.taxView = (LinearLayout) v.findViewById(R.id.item_view_tax);
			
			v.setTag(holder);
		}else{
			holder = (ViewHolder.PriceSummaryHolder) v.getTag();
		}
		
		AQuery taq = this.aq.recycle(v);
		List<SegmentModel> seg = new ArrayList<SegmentModel>();
		if (position == 0){
			seg = item.getDepartSegments();
		}else{
			seg = item.getReturnSegments();
		}
		taq.id(holder.departN).text(seg.get(0).getDepartStation());
		taq.id(holder.returnN).text(seg.get(seg.size()-1).getArriveStation());
		
		LinearLayout journeyLayout = (LinearLayout) taq.id(holder.journeyView).getView();
		
		SQLhelper db = new SQLhelper(act);
		for (int i = 0; i < seg.size() ; i ++){
			String station_key = seg.get(i).getDepartStation();
			View childView = inflater.inflate(R.layout.item_summary_journey, null);
			StationModel station = db.getStationBaseKey(station_key, 0);
			TextView departName = (TextView) childView.findViewById(R.id.item_summary_depart);
			TextView departStation = (TextView) childView.findViewById(R.id.item_summary_depart_station);
			departName.setText(station.getStationName());
			departStation.setText(station.getAirportName());
			
			station_key = seg.get(i).getArriveStation();
			station = db.getStationBaseKey(station_key, 0);
			TextView returnName = (TextView) childView.findViewById(R.id.item_summary_return);
			TextView returnStation = (TextView) childView.findViewById(R.id.item_summary_return_station);
			returnName.setText(station.getStationName());
			returnStation.setText(station.getAirportName());
			
			journeyLayout.addView(childView);
		}
		
		db.close();
		
		List<MemberInfoModel> passangers = item.getPassengerInfo();
		LogHelper.debug("Passanger Size " + passangers.size());
		LinearLayout passangerLayout = (LinearLayout) taq.id(holder.passangerView).getView();
		for (int i = 0; i < passangers.size(); i ++){
			View childView = inflater.inflate(R.layout.item_summary_price, null);
			TextView pName = (TextView) childView.findViewById(R.id.item_summary_description);
			TextView pPrice = (TextView) childView.findViewById(R.id.item_summary_price);
			MemberInfoModel mPsg = passangers.get(i);
			if (mPsg.getFullName() != null
					&& mPsg.getFullName().trim().length()>0){
				pName.setText(mPsg.getFullName());
			}else{
				pName.setText(act.getString(R.string.guest_header).replace("$NUM$", ""+mPsg.getPassangerNum()) 
						+ " (" + mPsg.getPsgTypeString() +") " );
			}
			if (position == 0){
				pPrice.setText(""+item.getDepartTaxes().get(mPsg.getPsgTypeCode()).get(0).getAmount());
			}else{
				pPrice.setText(""+item.getReturnTaxes().get(mPsg.getPsgTypeCode()).get(0).getAmount());
			}
			passangerLayout.addView(childView);
		}
		
		List<FareTaxesModel> totalPrice = null;
		
		if (position == 0){
			totalPrice = item.getDepartTotalTax();
		}else{
			totalPrice = item.getReturnTotalTax();
		}
		
		LinearLayout taxesLayout = (LinearLayout) taq.id(holder.taxView).getView();
		
		for(int i = 0 ; i < totalPrice.size(); i ++){
			LogHelper.debug("Key Price "   + totalPrice.get(i).getKey());
			if (!totalPrice.get(i).getKey().equalsIgnoreCase("FarePrice")){
				View taxViews = inflater.inflate(R.layout.item_summary_price, null);
				TextView taxName = (TextView) taxViews.findViewById(R.id.item_summary_description);
				TextView taxPrice = (TextView) taxViews.findViewById(R.id.item_summary_price);
				taxName.setText(totalPrice.get(i).getTitle());
				taxPrice.setText(""+totalPrice.get(i).getAmount());
				taxesLayout.addView(taxViews);
			}
		}
		
		LinearLayout addonLayout = (LinearLayout) taq.id(holder.addOnView).getView();
		
//		for(int i = 0 ; i < totalPrice.size(); i ++){
//			View taxViews = inflater.inflate(R.layout.item_summary_price, null);
//			TextView taxName = (TextView) taxViews.findViewById(R.id.item_summary_description);
//			TextView taxPrice = (TextView) taxViews.findViewById(R.id.item_summary_price);
//			taxName.setText(totalPrice.get(i).getTitle());
//			taxPrice.setText(""+totalPrice.get(i).getAmount());
//			addonLayout.addView(taxViews);
//		}
		
		return v;
	}
	


}
