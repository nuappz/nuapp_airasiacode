package com.airasia.adapter;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.airasia.holder.ConstantHolder;
import com.airasia.holder.ViewHolder;
import com.airasia.mobile.R;
import com.airasia.model.FareModel;
import com.airasia.model.FareTypeModel;
import com.airasia.model.SegmentModel;
import com.airasia.util.ConstantHelper;
import com.androidquery.AQuery;

public class FlightFareAdapter extends BaseAdapter{

	LayoutInflater inflater;
	Context c;
	AQuery aq;
	ViewHolder.FlightFareHolder holder;
	public boolean isPromo = true;
	List<FareTypeModel> item;
	String selectedId = "";
	
	public  FlightFareAdapter(View _v, Context _c, List<FareTypeModel> _item, String _selectedId){
		this.c = _c;
		inflater = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		aq = new AQuery(_v);
		this.item = _item;
		this.selectedId = _selectedId;
	}
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return item.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return item.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		View view = convertView;
		if (view == null){
			view = inflater.inflate(R.layout.item_flight_fare, null);
			holder = new ViewHolder.FlightFareHolder();
			holder.flightContainer = (LinearLayout) view.findViewById(R.id.item_flight_container);
			holder.flightIcon = (ImageView) view.findViewById(R.id.flight_icon_1);
			holder.flightIcon2 = (ImageView) view.findViewById(R.id.flight_icon_2);
			
			holder.dTime1 = (TextView) view.findViewById(R.id.item_fare_d_time);
			holder.dLocation1 = (TextView) view.findViewById(R.id.item_fare_d_name);
			holder.rTime1 = (TextView) view.findViewById(R.id.item_fare_r_time);
			holder.rLocation1 = (TextView) view.findViewById(R.id.item_fare_r_name);
			holder.estTime1 = (TextView) view.findViewById(R.id.item_estimate_time);
			holder.flightNum1 = (TextView) view.findViewById(R.id.item_flight_number);
			
			holder.promoView = (TextView) view.findViewById(R.id.item_fare_promo);
			holder.discountView = (TextView) view.findViewById(R.id.item_fare_discount);
			holder.limitSeat = (TextView) view.findViewById(R.id.item_fare_seatlimit);
			holder.bundleView = (TextView) view.findViewById(R.id.item_fare_bundle);
			
			holder.adultLayout = (RelativeLayout)  view.findViewById(R.id.item_adult_fare_layout);
			holder.kidLayout = (RelativeLayout) view.findViewById(R.id.item_kid_fare_layout);
			holder.infantLayout = (RelativeLayout) view.findViewById(R.id.item_inf_fare_layout);
			
			holder.adultDollar = (TextView) view.findViewById(R.id.item_adult_fare_dollar);
			holder.adultCurrency = (TextView) view.findViewById(R.id.item_adult_fare_currency);
			holder.adultSen = (TextView) view.findViewById(R.id.item_adult_fare_sen);
			
			holder.kidDollar = (TextView) view.findViewById(R.id.item_kid_fare_dollar);
			holder.kidCurrency = (TextView) view.findViewById(R.id.item_kid_fare_currency);
			holder.kidSen = (TextView) view.findViewById(R.id.item_kid_fare_sen);
			
			holder.infantDollar = (TextView) view.findViewById(R.id.item_inf_fare_dollar);
			holder.infantCurrency = (TextView) view.findViewById(R.id.item_inf_fare_currency);
			holder.infantSen = (TextView) view.findViewById(R.id.item_inf_fare_sen);
			
//			holder.viewDivider = (View) view.findViewById(R.id.item_continous_divider);
			holder.continousFlight = (LinearLayout) view.findViewById(R.id.item_continous_container);
 			
			view.setTag(holder);
		}else{
			holder = (ViewHolder.FlightFareHolder) view.getTag();
		}
		
		AQuery taq = this.aq.recycle(view);
		
		FareTypeModel model = item.get(position);

		List<SegmentModel> segList = model.getSegList();
		SegmentModel mSeg = segList.get(0);
		taq.id(holder.dTime1).text(ConstantHelper.gethhmmFromDateString(mSeg.getDataSTD()));
		taq.id(holder.rTime1).text(ConstantHelper.gethhmmFromDateString(mSeg.getDateSTA()));
		taq.id(holder.dLocation1).text(mSeg.getDepartStation());
		taq.id(holder.rLocation1).text(mSeg.getArriveStation());
		
		if (model.getId().equals(selectedId)){
			aq.id(holder.flightContainer).background(R.drawable.round_corner_redline);
			aq.id(holder.flightIcon).image(R.drawable.icon_planered_v2_red);
			aq.id(holder.flightIcon2).image(R.drawable.icon_planered_v2_red);
			
		}else{
			aq.id(holder.flightContainer).background(R.drawable.round_curve_white_box);
			aq.id(holder.flightIcon).image(R.drawable.icon_planeblack_v2);
			aq.id(holder.flightIcon2).image(R.drawable.icon_planeblack_v2);
		}
		
//		if(segList.size()>=2){
//			mSeg = model.getSegList().get(1);
//			taq.id(holder.viewDivider).visible();
//			taq.id(holder.continousFlight).visible();
//			taq.id(holder.dLocation2).text(mSeg.getDepartStation());
//			taq.id(holder.rLocation2).text(mSeg.getArriveStation());
//		}else{
//			taq.id(holder.continousFlight).gone();
//			taq.id(holder.viewDivider).gone();
//		}
		
		LinearLayout segView = holder.continousFlight;
		
		if (segList.size()>1){
			for (int si = 1; si < segList.size(); si++){
				SegmentModel childSeg = segList.get(si);
				View childView = inflater.inflate(R.layout.item_flight_fare_cont, null);
				TextView dName = (TextView) childView.findViewById(R.id.item_fare_depart_name);
				TextView dTime = (TextView) childView.findViewById(R.id.item_fare_depart_time);
				dName.setText(childSeg.getDepartStation());
				dTime.setText(ConstantHelper.gethhmmFromDateString(childSeg.getDataSTD()));
				TextView rName = (TextView) childView.findViewById(R.id.item_fare_arrival_name);
				TextView rTime = (TextView) childView.findViewById(R.id.item_fare_arrival_time);
				rName.setText(childSeg.getArriveStation());
				rTime.setText(ConstantHelper.gethhmmFromDateString(childSeg.getDateSTA()));
				
				TextView estTime = (TextView) childView.findViewById(R.id.item_estimate_time);
				TextView flightNum = (TextView) childView.findViewById(R.id.item_flight_number);
				estTime.setText(childSeg.getEstimateTime());
				flightNum.setText(childSeg.getFlgithNum());
				
				segView.addView(childView);
			}
		}
		String estimateTime = ConstantHelper.calculateEstimateTime(mSeg.getDataSTD()
													, mSeg.getDateSTA()
													, ConstantHolder.PATTERN_YMDTHMS);
		taq.id(holder.estTime1).text(estimateTime);
		taq.id(holder.flightNum1).text(mSeg.getFlgithNum());
		
		
		/**Adult**/
		FareModel mFare = model.getHashMap().get(ConstantHolder.FARE_ADT);
		if (mFare != null){
			String price = String.valueOf(mFare.getFinalFare());
			String[] adultPrice = price.trim().split("\\.");
			
			taq.id(holder.adultLayout).visible();
			taq.id(holder.adultDollar).text(adultPrice[0].trim());
			taq.id(holder.adultCurrency).text(mFare.getCurrencyCode());
			taq.id(holder.adultSen).text(adultPrice[1].trim());
		}else{
			taq.id(holder.adultLayout).gone();
		}
		
		int discount = mFare.getDiscountPercentage();
		if (discount!=0){
			taq.id(holder.discountView).visible().text("-"+discount+"%");
		}else{
			taq.id(holder.discountView).gone();
		}
		
		if (model.getMinAvailableSeats()<=5){
			String seatLeft = c.getResources().getString(R.string.fare_limit_seat).replace("$SEAT$", model.getMinAvailableSeats()+"");
			taq.id(holder.limitSeat).visible().text(seatLeft);
		}else{
			taq.id(holder.limitSeat).gone();
		}
		
		/**Child**/
		mFare = model.getHashMap().get(ConstantHolder.FARE_CHD);
		if (mFare !=null){

			String price = String.valueOf(mFare.getFinalFare());
			String[] childPrice = price.trim().split("\\.");
			
			taq.id(holder.kidLayout).visible();
			taq.id(holder.kidDollar).text(childPrice[0].trim());
			taq.id(holder.kidCurrency).text(mFare.getCurrencyCode());
			taq.id(holder.kidSen).text(childPrice[1].trim());
		}else{
			taq.id(holder.kidLayout).gone();
		}
		
		mFare = model.getHashMap().get(ConstantHolder.FARE_INF);
		if (mFare !=null){
			String price = String.valueOf(mFare.getFinalFare());
			String[] childPrice = price.trim().split("\\.");
			
			taq.id(holder.infantLayout).visible();
			taq.id(holder.infantDollar).text(childPrice[0].trim());
			taq.id(holder.infantCurrency).text(mFare.getCurrencyCode());
			taq.id(holder.infantSen).text(childPrice[1].trim());
		}else{
			taq.id(holder.infantLayout).gone();
		}
		
		if (isPromo){
			aq.id(holder.promoView).visible();
		}else{
			aq.id(holder.promoView).gone();
		}
		
//		gethhmmFromDateString
		
		return view;
	}

	public void changeList(List<FareTypeModel> _item){
		if (this.item == null){
			this.item = new ArrayList<FareTypeModel>();
		}
		
		if (_item!=null){
			this.item = _item;
			this.notifyDataSetChanged();
		}
	}
	
}
