package com.airasia.mobile;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.Patterns;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.airasia.fragment.FragmentBase;
import com.airasia.fragment.FragmentCase;
import com.airasia.util.ConstantHelper;
import com.androidquery.AQuery;

public class SignUpActivity extends ActionBarActivity{

	int currentPage = 0;
	AQuery aq;
	
	@Override
	protected void onCreate(Bundle arg0) {
		// TODO Auto-generated method stub
		super.onCreate(arg0);
		setContentView(R.layout.activity_signup);
		aq = new AQuery(this);
		
		switchFragment(FragmentCase.FRAG_SIGNUP_LOGIN);
		initActionBar();
		// Create new fragment and transaction
		
	}
	
	void initActionBar(){
		ActionBar actionBar = getSupportActionBar();
		actionBar.setHomeButtonEnabled(true);
		actionBar.setDisplayHomeAsUpEnabled(true);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		
		switch (item.getItemId()) {
        case android.R.id.home:
//        	if (secondContent != null){
//        		initFragment(secondContent);
//        		secondContent = null;
//        	}else{
//        		this.finish();
//    		}
        	if(getSupportFragmentManager().getBackStackEntryCount() == 1) {
        		this.finish();
            }
            else {
            	getSupportFragmentManager().popBackStack();
            }
        		
            return true;
        default:
            return super.onOptionsItemSelected(item);
        }
	}
	
	

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		if(getSupportFragmentManager().getBackStackEntryCount() == 1) {
    		this.finish();
        }
//        else {
//        	getSupportFragmentManager().popBackStack();
//        }
		super.onBackPressed();
	}

	public void submitForm(View v){
		switch (v.getId()){
//		case R.id.complete_login:
//			checkLogin();
//			break;
//		case R.id.complete_personal:
//			switchFragment(FragmentCase.FRAG_SIGNUP_CONTACT);
//			break;
		case R.id.complete_contact:
			switchFragment(FragmentCase.FRAG_SIGNUP_CONFIRMATION);
			break;
		default:
			break;
		}
	}
	
	void checkLogin(){
		
//		android.util.Patterns.EMAIL_ADDRESS.matcher(aq.id(R.id.signup_username).toString()).matches();
		String userName = aq.id(R.id.signup_username).getEditText().getText().toString();
		String password = aq.id(R.id.signup_password).getEditText().getText().toString();
		String re_password = aq.id(R.id.signup_reenterpassword).getEditText().getText().toString();
		
		if (!Patterns.EMAIL_ADDRESS.matcher(userName).matches()){
			Toast.makeText(getApplicationContext(), "Email invalid!",
					   Toast.LENGTH_LONG).show();
			return;
		}
		if(password.length()<8 || password.length()>15
				 || !ConstantHelper.isAlphaNumeric(password)){
			Toast.makeText(getApplicationContext(), "Password invalid!",
					   Toast.LENGTH_LONG).show();
			return;
		}
		if(!password.equals(re_password)){
			Toast.makeText(getApplicationContext(), "Password do not match!",
					   Toast.LENGTH_LONG).show();
			return;
		}
		
		switchFragment(FragmentCase.FRAG_SIGNUP_PERSONAL);
		
		userName = null;
		password = null;
		re_password = null;
	}
	
	void switchFragment(int _pos){
		Fragment newFragment = new FragmentBase();
		Bundle args = new Bundle();
		args.putInt("type", _pos);
		newFragment.setArguments(args);
		
		FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

		// Replace whatever is in the fragment_container view with this fragment,
		// and add the transaction to the back stack if needed
		transaction.replace(R.id.signup_container, newFragment);
		transaction.addToBackStack(newFragment.getTag());

		// Commit the transaction
		transaction.commit();
	}

}
