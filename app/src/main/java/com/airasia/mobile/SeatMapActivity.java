package com.airasia.mobile;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Toast;

import com.airasia.adapter.SeatMapTabViewAdapter;
import com.airasia.layout.SeatMapSlidingTabLayout;
import com.androidquery.AQuery;

public class SeatMapActivity extends ActionBarActivity {

    public void rightPressed(View v){
        Toast.makeText(SeatMapActivity.this,"Right Button Click",Toast.LENGTH_SHORT).show();
    }

    public void donePressed(View v){
        Toast.makeText(SeatMapActivity.this,"Right Button Click",Toast.LENGTH_SHORT).show();
    }
    ViewPager pager;
    SeatMapTabViewAdapter adapter;
    SeatMapSlidingTabLayout tabs;
    CharSequence Titles[]={"PEN/KUL","KUL/MEL","KUL/DEL","DEL/BAN"}; // Array for Flight list with Direction
    int Numboftabs =Titles.length;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_seat_map);
        setupActionBar();

        adapter =  new SeatMapTabViewAdapter(getSupportFragmentManager(),Titles,Numboftabs);

        // Assigning ViewPager View and setting the adapter
        pager = (ViewPager) findViewById(R.id.seatmap_pager);
        pager.setAdapter(adapter);

        // Assiging the Sliding Tab Layout View
        tabs = (SeatMapSlidingTabLayout) findViewById(R.id.seatmap_tabs);
        tabs.setDistributeEvenly(true); // To make the Tabs Fixed set this true, This makes the tabs Space Evenly in Available width
        //tabs.setCustomTabView();
        //seatmap_custom_tab_title



        // Setting Custom Color for the Scroll bar indicator of the Tab View
        tabs.setCustomTabColorizer(new SeatMapSlidingTabLayout.TabColorizer() {
            @Override
            public int getIndicatorColor(int position) {
                return getResources().getColor(R.color.light_yellow);
            }

        });

        // Setting the ViewPager For the SlidingTabsLayout
        tabs.setViewPager(pager);
    }

    private void setupActionBar() {

        ActionBar ab = getSupportActionBar();
        ab.setDisplayShowCustomEnabled(true);
        ab.setDisplayShowTitleEnabled(false);
        //ab.setIcon(R.drawable.logo_aa);
        LayoutInflater inflator = (LayoutInflater) this
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = inflator.inflate(R.layout.custom_action_bar, null);

        AQuery tempQ = new AQuery(v);

        tempQ.id(R.id.aa_logo).gone();
        tempQ.id(R.id.app_title).text("");
        tempQ.id(R.id.leftButton_view).gone();
        tempQ.id(R.id.rightDone).text(R.string.ok_text);
        tempQ.id(R.id.rightButton_done).visible();

        ab.setCustomView(v);
        ab.setDisplayUseLogoEnabled(false);
        ab.setDisplayShowHomeEnabled(false);
    }
    @Override
    public void onBackPressed() {
        Intent i = new Intent(SeatMapActivity.this,Add_OnsActivity.class);
        startActivity(i);
        SeatMapActivity.this.finish();
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
    }

}