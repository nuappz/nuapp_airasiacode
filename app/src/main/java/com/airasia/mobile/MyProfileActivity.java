package com.airasia.mobile;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.apache.http.Header;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.LinearLayout;
import android.widget.ExpandableListView.OnGroupClickListener;

import com.airasia.adapter.FrenFamilyAdapter;
import com.airasia.holder.PrefsHolder;
import com.airasia.model.MemberInfoModel;
import com.airasia.model.PassportModel;
import com.airasia.util.ConstantHelper;
import com.airasia.util.CustomHttpClient;
import com.airasia.util.JSonHelper;
import com.airasia.util.LogHelper;
import com.androidquery.AQuery;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

public class MyProfileActivity extends ActionBarActivity{

	
	AQuery aq;
	SharedPreferences prefs;
	int type =1;
	String title;
	public static final int MY_PROFILE = 0;
	public static final int MY_DOCUMENT = 1;
	public static final int MY_FREN = 3;
	public static final int MY_FREN_SELECT = 4;
	
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		 prefs = getSharedPreferences(PrefsHolder.PREFS, Context.MODE_PRIVATE);
		 
		 Bundle extras = getIntent().getExtras();
		 if (extras != null){
			 type = extras.getInt("type");
		 }
		 
		 if(type == MY_PROFILE){
		 	setContentView(R.layout.fragment_my_profile);
		 	title = getString(R.string.profile_my_profile);
		 }else if (type == MY_FREN || type == MY_FREN_SELECT){
			setContentView(R.layout.fragment_fren_family);
		 	title = getString(R.string.profile_my_frenfamily);
		 }else {
			setContentView(R.layout.fragment_my_document);
		 	title = getString(R.string.profile_my_travel);
		 }
		 
		 aq = new AQuery(this);
		 setupActionBar(title);
		 if (type == MY_PROFILE){
			 initMyProfile();
		 }else if (type == MY_FREN || type == MY_FREN_SELECT){
			 initFrenFamily();
		 }else{
			 initTravelDocument();
		 }
		 
	}
	
	void initFrenFamily(){
		connGetFrenFamily();
	}
	 
	private void initMyProfile(){
		
		String fname = prefs.getString(PrefsHolder.USER_FIRSTNAME, "");
		String lname = prefs.getString(PrefsHolder.USER_LASTNAME, "");
		String email = prefs.getString(PrefsHolder.USER_USERNAME, "");
		String national = prefs.getString(PrefsHolder.USER_NATIONALITY, "");
		String dob = prefs.getString(PrefsHolder.USER_DOB, "");
		String mobile = prefs.getString(PrefsHolder.USER_MOBILE, "");
		
		aq.id(R.id.profile_email).getTextView().setText(email);
		aq.id(R.id.profile_dob).getTextView().setText("" + ConstantHelper.getDateFromddMMyyyy(dob));
		aq.id(R.id.profile_firstName).getTextView().setText(fname);
		aq.id(R.id.profile_lastName).getTextView().setText(lname);
		aq.id(R.id.profile_nationality).getTextView().setText(national);
		aq.id(R.id.profile_mobile).getTextView().setText(""+mobile);
	}
	
	private void setupActionBar(String _title) {
		
        ActionBar ab = getSupportActionBar();
        ab.setDisplayShowCustomEnabled(true);
        ab.setDisplayShowTitleEnabled(false);
        //ab.setIcon(R.drawable.logo_aa);
        LayoutInflater inflator = (LayoutInflater) this
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = inflator.inflate(R.layout.custom_action_bar, null);

        AQuery tempQ = new AQuery(v);
        
    	tempQ.id(R.id.leftButton_view).gone();
    	tempQ.id(R.id.rightButton_view).gone();
    	tempQ.id(R.id.aa_logo).gone();
    	tempQ.id(R.id.app_title).visible().getTextView().setText(_title);
        
        

        ab.setCustomView(v);
        ab.setDisplayUseLogoEnabled(false);
        ab.setDisplayShowHomeEnabled(false);
        
    }
	
	private void initTravelDocument(){
		connGetTravelDocument();
	}
	
	void connGetTravelDocument(){
		RequestParams params = new RequestParams();
		params.put("customerId", ""+prefs.getInt(PrefsHolder.USER_CID, 0));
		params.put("username", prefs.getString(PrefsHolder.USER_USERNAME, ""));
		params.put("ssoTicket", prefs.getString(PrefsHolder.SSO_TICKET, ""));
		
		CustomHttpClient.postWithHashWithoutServer(CustomHttpClient.GET_TRAVEL_DOC , params, new AsyncHttpResponseHandler() {
			
			@Override
			public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
				// TODO Auto-generated method stub
				String response = new String(responseBody);
				LogHelper.debug("Travel Doc 1 " +  response );
				response = response.replaceAll("null", "\"\"");
				LogHelper.debug("Travel Doc 2 " +  response );
				JSONObject json;
				LogHelper.debug(response);
				try {
					int responseCode = JSonHelper.GetResponseStatus(response);
					String responseMsg = JSonHelper.GetResponseMessage(response);
					if (responseCode == JSonHelper.SUCCESS_REPONSE){
						json = new JSONObject(response);
						JSONObject data = json.getJSONObject("data");
						JSONObject childJson = data.getJSONObject("result");
						PassportModel model = JSonHelper.parsePassport(childJson);
						
						
						if (model!=null){
							aq.id(R.id.profile_passport).getTextView().setText(model.getNumber());
							aq.id(R.id.profile_issuing).getTextView().setText(model.getCountryCode());
							aq.id(R.id.profile_expiry_date).getTextView().setText(ConstantHelper.getDateFromddMMyyyy(model.getDate()));
						}
						
						
					}else{
						MainActivity.showErrorMessage(MyProfileActivity.this, responseMsg);
					}
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (Exception e){
					e.printStackTrace();
				}
				
			}
			
			@Override
			public void onFailure(int statusCode, Header[] headers,
					byte[] responseBody, Throwable error) {
				// TODO Auto-generated method stub
				
			}
		});
	}
	
	public void connGetFrenFamily(){
		RequestParams params = new RequestParams();
		params.put("username", prefs.getString(PrefsHolder.USER_USERNAME, ""));
		params.put("ssoTicket", prefs.getString(PrefsHolder.SSO_TICKET, ""));
		
		
		CustomHttpClient.postWithHashWithoutServer(CustomHttpClient.GET_FREN_FAMILY, params, new AsyncHttpResponseHandler() {
			
			@Override
			public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
				// TODO Auto-generated method stub
				String response = new String(responseBody);
				response = response.replaceAll("null", "\"\"");
				LogHelper.debug("Fren & family " +response);
				int responseCode = JSonHelper.GetResponseStatus(response);
				
				if(responseCode == JSonHelper.SUCCESS_REPONSE){
					List<MemberInfoModel> model = JSonHelper.parseFrenFamily(response);
					
					FrenFamilyAdapter adapter
						= new FrenFamilyAdapter(aq.id(R.id.expandable_list).getExpandableListView(),
								MyProfileActivity.this, model);
					aq.id(R.id.expandable_list).getExpandableListView().setAdapter(adapter);
					setupExpandableList();
				}else{
					String errorMsg = JSonHelper.GetResponseMessage(response);
					MainActivity.showErrorMessage(MyProfileActivity.this, errorMsg);
				}
				
			}
			
			@Override
			public void onFailure(int statusCode, Header[] headers,
					byte[] responseBody, Throwable error) {
				// TODO Auto-generated method stub
				
			}
		});
	}
	
	void setupExpandableList(){
		
		aq.id(R.id.expandable_list).getExpandableListView().setOnGroupClickListener(new OnGroupClickListener() {
			int positionSelect = -1;
			@Override
			public boolean onGroupClick(ExpandableListView parent, View v,
					int groupPosition, long id) {
				// TODO Auto-generated method stub
				FrenFamilyAdapter adapter = (FrenFamilyAdapter) aq.id(R.id.expandable_list).getExpandableListView().getExpandableListAdapter();
	            
				if(type == MY_FREN){
					if(groupPosition != positionSelect){
			            aq.id(R.id.expandable_list).getExpandableListView().collapseGroup(positionSelect);
			            adapter.positionExpand = groupPosition;
			            positionSelect = groupPosition;
					}else{
						adapter.positionExpand = -1;
						positionSelect = -1;
					}
					adapter.notifyDataSetChanged();

					return false;
				}else{
					LogHelper.debug("selected Member");
					Bundle bundle = new Bundle();
					MemberInfoModel member = adapter.getFren(groupPosition);
					bundle.putString("name", member.getfName());
					bundle.putString("expireDate", member.getExpiredDate());
					bundle.putString("issue", member.getIssuingCountry());
					bundle.putString("dob", member.getDob());
					bundle.putString("national", member.getNationality());
					bundle.putString("passport", member.getPassport());
					Intent intent = new Intent();
			        intent.putExtras(bundle);
			        setResult(RESULT_OK, intent);
			        finish();
			        return true;
				}
			}
		});
	}
	
}
