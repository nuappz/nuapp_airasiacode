package com.airasia.mobile;

import java.util.List;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.airasia.adapter.MealPagerAdapter;
import com.airasia.adapter.PopUpPassangerList;
import com.airasia.adapter.SSRBaggageAdapter;
import com.airasia.fragment.FragmentCase;
import com.airasia.model.BookingInfoModel;
import com.airasia.model.CountryModel;
import com.airasia.model.MemberInfoModel;
import com.airasia.model.SSRItemModel;
import com.airasia.model.SSRMainModel;
import com.airasia.util.LogHelper;
import com.androidquery.AQuery;

public class SSRListingActivity extends ActionBarActivity{

	AQuery aq;
	String title = "";
	int type;
	ViewPager pager;
	AlertDialog dialog;

	SSRMainModel items;
	MemberInfoModel selectedPerson;
	BookingInfoModel booking;
	
	Handler handler;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		Bundle extras = getIntent().getExtras();
		if (extras != null){
			type = extras.getInt("type", 0);
			title = extras.getString("title", "");
			booking = (BookingInfoModel) extras.getSerializable("flight");
			items = (SSRMainModel) extras.getSerializable("ssr");
		}
		
		if (type == 1){
			setContentView(R.layout.activity_insurace);
		}else if(type == FragmentCase.FRAG_SSR_MEAL){
			setContentView(R.layout.fragment_meal_pager);
		}else{
			setContentView(R.layout.activity_list_view);
		}
		aq = new AQuery(this);
		
		setupActionBar();
		
		if (handler == null){
			handler = new Handler();
		}
		
		handler.postDelayed(viewUpdate, 500);
	}
	
	private void setupActionBar() {
		
        ActionBar ab = getSupportActionBar();
        ab.setDisplayShowCustomEnabled(true);
        ab.setDisplayShowTitleEnabled(false);
        //ab.setIcon(R.drawable.logo_aa);
        LayoutInflater inflator = (LayoutInflater) this
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = inflator.inflate(R.layout.custom_action_bar, null);

        AQuery tempQ = new AQuery(v);
        
        tempQ.id(R.id.aa_logo).gone();
        tempQ.id(R.id.app_title).text(title);
        tempQ.id(R.id.leftButton_view).gone();
        tempQ.id(R.id.rightDone).text(R.string.ok_text);
        tempQ.id(R.id.rightButton_done).visible();

        ab.setCustomView(v);
        ab.setDisplayUseLogoEnabled(false);
        ab.setDisplayShowHomeEnabled(false);
    }
	
	Runnable viewUpdate = new Runnable() {
		
		@Override
		public void run() {
			// TODO Auto-generated method stub
			setupView();
		}
	};
	
	private void setupView(){
		if (type == 1){
			setupInsurace();
		}else if (type == FragmentCase.FRAG_SSR_MEAL){
			setupMealPager();
		}else{
			SSRBaggageAdapter mAdapter = new SSRBaggageAdapter(items,
							booking,
							SSRListingActivity.this,
							aq.id(R.id.list_view_container).getView());
			aq.id(R.id.list_view_container).getListView().setAdapter(mAdapter);
		}
	}
	
	private void setupInsurace(){
		
		LinearLayout passangerLayout = (LinearLayout) aq.id(R.id.insurance_passanger).getView();
		List<MemberInfoModel> pList = items.getPassangers();//booking.getPassengerInfo();
		LayoutInflater infalter = (LayoutInflater) SSRListingActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		for (int i = 0 ; i < pList.size(); i++){
			View childView = infalter.inflate(R.layout.item_insurance_psg, null);
			TextView psgName = (TextView) childView.findViewById(R.id.item_insurance_psg);
			TextView currency = (TextView) childView.findViewById(R.id.item_insurance_currency);
			TextView dollar = (TextView) childView.findViewById(R.id.item_insurance_dollar);
			TextView sens = (TextView) childView.findViewById(R.id.item_insurance_sen);
			currency.setText(booking.getCurrencyCode());
			String value = String.valueOf(pList.get(i).getPremiumAmount());
			String[] splitTotal = value.split("\\.");
			dollar.setText(splitTotal[0]);
			sens.setTag(splitTotal[1]);
			psgName.setText(pList.get(i).getFullName());
			passangerLayout.addView(childView);
		}
		
		aq.id(R.id.insurance_travel).getView().setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(v.isActivated()){
					v.setActivated(false);
				}else{
					v.setActivated(true);
				}
			}
		});
		aq.id(R.id.insurance_tnc).getView().setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(v.isActivated()){
					v.setActivated(false);
				}else{
					v.setActivated(true);
				}
			}
		});
	}
	
	public void donePressed(View v){
		Bundle extras = new Bundle();
		switch (type){
		case 1:
			extras.putInt("type", type);
			extras.putSerializable("resultBooking", booking);
			extras.putSerializable("ssrmain", items);
			if(!aq.id(R.id.insurance_tnc).getView().isActivated()){
				LogHelper.debug("TNC Eror");
				showErrorMessage(getString(R.string.error_insurance_tnc));
				return;
			}
			break;
		default:
			extras.putInt("type", type);
			extras.putSerializable("resultBooking", booking);
			extras.putSerializable("ssrmain", items);
			break;
		}
		
		Intent intent = new Intent();
        intent.putExtras(extras);
		setResult(RESULT_OK, intent);
		finish();
	}
	
	public void setupMealPager(){
		selectedPerson = items.getPassangers().get(0);
		aq.id(R.id.meal_psg_name).text(selectedPerson.getFullName());
		pager = (ViewPager) aq.id(R.id.meal_view_pager).getView();
		MealPagerAdapter mPagerAdapter = new MealPagerAdapter(getSupportFragmentManager(),
					items , booking, selectedPerson);
		pager.setOffscreenPageLimit(1);
		pager.setAdapter(mPagerAdapter);
		
	}
	
	public void updateMealPerson(){
		aq.id(R.id.meal_psg_name).text(selectedPerson.getFullName());
		pager = (ViewPager) aq.id(R.id.meal_view_pager).getView();
		MealPagerAdapter mPagerAdapter = (MealPagerAdapter) pager.getAdapter();
		mPagerAdapter.updatePerson(selectedPerson);

	}
	
	public void SelectPassanger(View v){
		showArrayAlert(items.getPassangers(), v);
	}

	 public void showErrorMessage(String errorMsg){
		AlertDialog.Builder builder = new AlertDialog.Builder(SSRListingActivity.this);
		builder.setMessage(errorMsg);
		builder.setCancelable(true);
		builder.setPositiveButton(getResources().getString(R.string.ok_text),
                new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
	}
	
	 public void showArrayAlert(List<MemberInfoModel> _list, final View v){
	 	LogHelper.debug("Show Alert List");
	 	AlertDialog.Builder builder=new AlertDialog.Builder(SSRListingActivity.this);
		builder.setTitle(R.string.signup_state);
		ListView listView=new ListView(SSRListingActivity.this);
		PopUpPassangerList adapter = new PopUpPassangerList(listView, this, _list); 
		listView.setAdapter(adapter);
		listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
	 
		    @Override
		     public void onItemClick(AdapterView<?> arg0, View view,
		        int position, long arg3) {
		         // TODO Auto-generated method stub
		    		PopUpPassangerList adp = (PopUpPassangerList) arg0.getAdapter();
		    		selectedPerson = (MemberInfoModel) adp.getItem(position);
		    		updateMealPerson();
		    		if (dialog.isShowing()){
		    			dialog.dismiss();
		    		}
		    	}
		});
		builder.setView(listView);
	    dialog=builder.create();
	    dialog.show();
	}
	 
}
