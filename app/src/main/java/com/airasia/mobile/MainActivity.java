package com.airasia.mobile;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.text.format.Formatter;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnChildClickListener;
import android.widget.ExpandableListView.OnGroupClickListener;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.airasia.adapter.BookingAdapter;
import com.airasia.adapter.DrawerAdapter;
import com.airasia.adapter.FlightAdapter;
import com.airasia.adapter.GuestListingAdapter;
import com.airasia.fragment.BoardingPassFragment;
import com.airasia.fragment.BookingFragment;
import com.airasia.fragment.CheckInFragment;
import com.airasia.fragment.FragmentCase;
import com.airasia.fragment.FrenFamilyFragment;
import com.airasia.fragment.GuestDetailsFragment;
import com.airasia.fragment.HomeFragment;
import com.airasia.fragment.MyProfileFragment;
import com.airasia.fragment.PaymentFragment;
import com.airasia.fragment.ProfileFragment;
import com.airasia.fragment.SSRFragment;
import com.airasia.holder.ConstantHolder;
import com.airasia.holder.PrefsHolder;
import com.airasia.model.BookingInfoModel;
import com.airasia.model.CheckInModel;
import com.airasia.model.DrawerModel;
import com.airasia.model.FareTypeModel;
import com.airasia.model.FlightSearchModel;
import com.airasia.model.FlightsModel;
import com.airasia.model.MemberInfoModel;
import com.airasia.model.SSRContainer;
import com.airasia.model.SSRItemModel;
import com.airasia.model.SSRMainModel;
import com.airasia.model.SSRModel;
import com.airasia.model.SearchInfoModel;
import com.airasia.model.SeatSummary;
import com.airasia.model.SegmentModel;
import com.airasia.model.SelectedFlightModel;
import com.airasia.model.StationModel;
import com.airasia.service.ConnectionService;
import com.airasia.service.SessionService;
import com.airasia.util.ConstantHelper;
import com.airasia.util.CustomHttpClient;
import com.airasia.util.JSonHelper;
import com.airasia.util.LogHelper;
import com.airasia.util.SQLhelper;
import com.androidquery.AQuery;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.nostra13.universalimageloader.cache.disc.naming.HashCodeFileNameGenerator;
import com.nostra13.universalimageloader.cache.memory.impl.LruMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.nostra13.universalimageloader.core.decode.BaseImageDecoder;
import com.nostra13.universalimageloader.core.download.BaseImageDownloader;

@TargetApi(Build.VERSION_CODES.HONEYCOMB_MR1) @SuppressLint({ "InlinedApi", "NewApi" }) 
public class MainActivity extends ActionBarActivity implements OnItemClickListener {

	public static final int PICK_DATE_REQUEST = 1,
			MEMBER_CHECKIN_REQUEST = 2,
			PICK_STATE_REQUEST = 3,
			GUEST_NATION = 4,
			SSR_LISTING = 5,
			LOGIN_REQUEST = 6;
	
	List<DrawerModel> drawerList;
	
	private int selectedFragment = FragmentCase.FRAG_HOME;
	
	private ListView mDrawerList;
	private DrawerLayout mDrawerLayout;
	//private ActionBarDrawerToggle mDrawerToggle;
	AQuery aq;
	public static MainActivity act;
	BookingInfoModel currentBooking;
	SearchInfoModel saerchItem;
	FlightSearchModel item;
	SSRMainModel ssrItem;
	SelectedFlightModel selectedFligh;
	
	
	ProgressDialog progress;
	public static ProgressDialog globalProgress;
	AlertDialog dialog;
	
	
	SharedPreferences prefs;
	Handler handler;
	Handler timerHandler;
	Handler viewHandler;
	
	int adaultC = 1, childC = 0, infantC = 0;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		act = this;
		aq = new AQuery(this);
		prefs = getSharedPreferences(PrefsHolder.PREFS, MODE_PRIVATE);
		
		moveDrawerToTop();
		setupActionBar();
		
		initDrawer();
	    //Quick cheat: Add Fragment 1 to default view
	    //onItemClick(null, null, 0, 0);
		switchFragment(FragmentCase.FRAG_HOME);
		
		initImageLoader();
	}
	
	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		//mDrawerToggle.syncState();
	}
	
	public void initImageLoader(){
		ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(MainActivity.this)
//        .denyCacheImageMultipleSizesInMemory()
        .memoryCache(new LruMemoryCache(2 * 1024 * 1024))
        .memoryCacheSize(2 * 1024 * 1024)
//        .diskCacheSize(50 * 1024 * 1024)
//        .diskCacheFileCount(100)
        .build();
		
		ImageLoader.getInstance().init(config);
	}

	private void moveDrawerToTop() {
		LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	    DrawerLayout drawer = (DrawerLayout) inflater.inflate(R.layout.right_drawer, null); // "null" is important.

	    // HACK: "steal" the first child of decor view
	    ViewGroup decor = (ViewGroup) getWindow().getDecorView();
	    View child = decor.getChildAt(0);
	    decor.removeView(child);
	    FrameLayout container = (FrameLayout) drawer.findViewById(R.id.drawer_content); // This is the container we defined just now.
	    container.addView(child, 0);
	    drawer.findViewById(R.id.google_drawer).setPadding(0, getStatusBarHeight(), 0, 0);

	    // Make the drawer replace the first child
	    decor.addView(drawer);
	}
	
	public int getStatusBarHeight() {
	       int result = 0;
	       int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
	       if (resourceId > 0) {
	           result = getResources().getDimensionPixelSize(resourceId);
	       }
	       return result;
	 }

	
	
	private void setupActionBar() {
		
        ActionBar ab = getSupportActionBar();
        ab.setDisplayShowCustomEnabled(true);
        ab.setDisplayShowTitleEnabled(false);
        //ab.setIcon(R.drawable.logo_aa);
        LayoutInflater inflator = (LayoutInflater) this
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = inflator.inflate(R.layout.custom_action_bar, null);

        AQuery tempQ = new AQuery(v);
        
        if(prefs.getBoolean(PrefsHolder.IS_LOGIN, false)){
        	tempQ.id(R.id.leftButton_text).gone();
        }else{
        	tempQ.id(R.id.leftButton_text).visible();
        }

        ab.setCustomView(v);
        ab.setDisplayUseLogoEnabled(false);
        ab.setDisplayShowHomeEnabled(false);
    }
	
	private void hideButton(int button, boolean hide) {

        ActionBar ab = getSupportActionBar();
        AQuery tempQ = new AQuery(ab.getCustomView());
        if(hide == true){
        	tempQ.id(button).gone();
        }else{
        	tempQ.id(button).visible();
        }
    }
	
	private void updateTitle(String text, boolean hideTitle, boolean hideBanner) {

        ActionBar ab = getSupportActionBar();
        AQuery tempQ = new AQuery(ab.getCustomView());
        
        if (text !=null && text.length()>0 ){
        	tempQ.id(R.id.app_title).getTextView().setText(text);
        }
        

        if(hideTitle == true){
        	tempQ.id(R.id.app_title).gone();
        }else{
        	tempQ.id(R.id.app_title).visible();
        }
        
        if(hideBanner == true){
        	tempQ.id(R.id.aa_logo).gone();
        }else{
        	tempQ.id(R.id.aa_logo).visible();
        }
    }
	
	void startService(){
		Intent service = new Intent(this, ConnectionService.class);
		startService(service);
	}
	
	/** View Action **/
	public void BookFlight(View v){

		ConstantHolder.LOGIN_STATUS = 1;
		ConstantHolder.SESSION_TYPE = 0;
		switchFragment(FragmentCase.FRAG_BOOK_FLIGHTS);
	}
	
	public void PickDate(View v){
		Intent i = new Intent(this, CustomCalendarActivity.class);
		Bundle extras = new Bundle();
		extras.putInt("departId", R.id.booking_d_time);
		extras.putInt("returnId", R.id.booking_r_time);
		i.putExtras(extras);
		startActivityForResult(i, PICK_DATE_REQUEST);
	}
	
	public void AddPax(View v){
		
		switch (v.getId()){
		case R.id.booking_add_adult:
			if(adaultC <9){
				adaultC++;
				aq.id(R.id.booking_adult_count).text(""+adaultC);
			}
			break;
		case R.id.booking_add_kid:
			childC++;
			aq.id(R.id.booking_kid_count).text(""+childC);
			break;
		case R.id.booking_add_infant:
			infantC++;
			aq.id(R.id.booking_infant_count).text(""+infantC);
			break;
		}
		
		
		CheckButton();
		
	}
	
	public void MinusPax(View v){
		switch (v.getId()){
		case R.id.booking_minus_adult:
			if(adaultC >1){
				adaultC--;
				aq.id(R.id.booking_adult_count).text(""+adaultC);
			}
			break;
		case R.id.booking_minus_kid:
			if(childC >0){
				childC--;
				aq.id(R.id.booking_kid_count).text(""+childC);
			}
			break;
		case R.id.booking_minus_infant:
			if(infantC >0){
				infantC--;
				aq.id(R.id.booking_infant_count).text(""+infantC);
			}
			break;
		}
		
		CheckButton();
		
	}
	
	public void SwapCountry(View v){
		String departure = aq.id(R.id.booking_departure).getButton().getText().toString();
		String destination = aq.id(R.id.booking_destination).getButton().getText().toString();
		

		String departTag = (String) aq.id(R.id.booking_departure).getButton().getTag();
		String destiTag = (String) aq.id(R.id.booking_destination).getButton().getTag();
		
		aq.id(R.id.booking_departure).getButton().setText(destination);
		aq.id(R.id.booking_destination).getButton().setText(departure);
		if (destiTag !=null && destiTag.length()>0){
			aq.id(R.id.booking_departure).getButton().setTag(destiTag);
		}
		if (departTag !=null && departTag.length()>0){
			aq.id(R.id.booking_destination).getButton().setTag(departTag);
		} 
		
		
	}
	
	public void SearchFlights(View v){
		
		ConstantHolder.SESSION_TYPE = 0;
		ConstantHolder.LOGIN_STATUS = 1;
		connGetSession(0);
//		switchFragment(FragmentCase.FRAG_FARE_DISPLAY);

	}
	
	public void searchFlight(){
		String depart = (String) aq.id(R.id.booking_departure).getButton().getTag();
		String arrival= (String)aq.id(R.id.booking_destination).getButton().getTag();
		
		if (arrival == null){
			showErrorMessage(MainActivity.this, getString(R.string.error_return_earlier));
			return;
		}
		

		SimpleDateFormat format = new SimpleDateFormat("EEE',' dd MMM yyyy");
		SimpleDateFormat outFormat = new SimpleDateFormat("yyyy-MM-dd");
		
		String deDate = aq.id(R.id.booking_d_time).getText().toString();
		String reDate = aq.id(R.id.booking_r_time).getText().toString();
		
		deDate = ConstantHelper.changeDateFormat(deDate, format, outFormat);
		if (reDate != null && reDate.trim().length()>0){
			reDate = ConstantHelper.changeDateFormat(reDate, format, outFormat);
		}
		connSearchFlight(depart, arrival, deDate, reDate,
				adaultC, childC, infantC, 
				"", prefs.getString(PrefsHolder.USER_CURRENCY, PrefsHolder.DEFAULT_CURRENCY));
		
	}
	
	public void SetDestination(View v){
		LogHelper.debug("Conn get Destinasi"); 
		
		String code = (String) aq.id(R.id.booking_departure).getButton().getTag();
		if (code == null || code.length()<=0){
			showErrorMessage(MainActivity.this, getString(R.string.error_departure));
			return;
		}
		
		Intent i = new Intent(MainActivity.this, SearchStateActivity.class);
		Bundle extras = new Bundle();
		extras.putInt("viewId", v.getId());
		extras.putString("departCode", code);
		i.putExtras(extras);
		startActivityForResult(i, PICK_STATE_REQUEST);
	 }
		 
	public void SearchBooking(View v){
		String name = aq.id(R.id.checkin_familyName).getText().toString();
		String bookId = aq.id(R.id.checkin_bookingNumber).getText().toString();
		String stationCode = (String) aq.id(R.id.checkin_departure).getTag();
		
		if(name.length()<=0 ||
				bookId.length()<=0 || 
				stationCode.length()<=0){
			showErrorMessage(MainActivity.this, getString(R.string.field_mandatory));
			return;
		}
		

		connGetBooking(0 , name, bookId, stationCode, false);
		
	}
		
	public void AgreeCheckin(View v){
		if(v.getId() == R.id.check_in_agree){
			LogHelper.debug("Check in click");
//				showErrorMessage(this, "Module Ongoing");
			progress = ProgressDialog.show(MainActivity.this, getString(R.string.check_in_loading), getString(R.string.loading));
			
			boolean departCheckIn = true;
			boolean returnCheckIn = true;
			
			for (int i = 0; i < currentBooking.getMemberList().size(); i ++){
				MemberInfoModel mMember = currentBooking.getMemberList().get(i);
				if (mMember.isDepart()){
					if (!currentBooking.isDepartCheckin()){
						departCheckIn = false;
						 break;
					}
				}
				
				if (mMember.isReturn()){
					if (!currentBooking.isReturnCheckin()){
						returnCheckIn = false;
						break;
					}
				}
			}
			
			if (departCheckIn && returnCheckIn){
				if(currentBooking.isInternational()){
					connVerifyDoc();
				}else{
					CheckSeat();
				}
			}else{
				showErrorMessage(MainActivity.this, getString(R.string.check_in_invalid_na));
			}
		}else{
			List<MemberInfoModel> members = currentBooking.getMemberList();
			for (int i = 0 ; i < members.size() ; i++){
				MemberInfoModel model = members.get(i);
				if (model.getBringInfant()==1 || !model.getWheelChair().equals("0")){
					showErrorMessage(MainActivity.this, getString(R.string.check_in_invalid));
					return;
				}
			}
			
			switchFragment(FragmentCase.FRAG_CHECKIN_TNC);
		}
	}
	
	public void resetPassangerCount(){
		infantC = 0;
		childC = 0;
		adaultC = 1;
	}
	
	public void updatePassangerCount(){
		aq.id(R.id.booking_adult_count).text(""+adaultC);
		aq.id(R.id.booking_kid_count).text(""+childC);
		aq.id(R.id.booking_infant_count).text(""+infantC);
	}
	
	public void CheckButton(){
		int total = 0;
		total = childC + adaultC;
		if (total >=9){
			aq.id(R.id.booking_add_adult).clickable(false);
			aq.id(R.id.booking_add_kid).clickable(false);
			aq.id(R.id.booking_minus_adult).clickable(true);
			
			aq.id(R.id.add_adult_indi).image(R.drawable.up_1);
			aq.id(R.id.minus_adult_indi).image(R.drawable.down_0);
			aq.id(R.id.add_kid_indi).image(R.drawable.up_1);
			
		}else {
			aq.id(R.id.booking_add_adult).clickable(true);
			aq.id(R.id.booking_add_kid).clickable(true);
			aq.id(R.id.add_adult_indi).image(R.drawable.up_0);
			aq.id(R.id.add_kid_indi).image(R.drawable.up_0);
			if(adaultC<=1){
				aq.id(R.id.booking_minus_adult).clickable(false);
				aq.id(R.id.minus_adult_indi).image(R.drawable.down_1);
			}else{
				aq.id(R.id.booking_minus_adult).clickable(true);
				aq.id(R.id.minus_adult_indi).image(R.drawable.down_0);
			}
		}
		
		if(childC<=0){
			aq.id(R.id.booking_minus_kid).clickable(false);

			aq.id(R.id.minus_kid_indi).image(R.drawable.down_1);
		}else{
			aq.id(R.id.booking_minus_kid).clickable(true);
			aq.id(R.id.minus_kid_indi).image(R.drawable.down_0);
		}
		
		if (infantC>=adaultC || infantC>=4){
			if(infantC>adaultC){
				infantC--;
				aq.id(R.id.booking_infant_count).text(""+infantC);
			}
			aq.id(R.id.booking_add_infant).clickable(false);
			aq.id(R.id.booking_minus_infant).clickable(true);

			aq.id(R.id.add_infant_indi).image(R.drawable.up_1);
			aq.id(R.id.minus_infant_indi).image(R.drawable.down_0);
		}else{
			aq.id(R.id.booking_add_infant).clickable(true);
			aq.id(R.id.add_infant_indi).image(R.drawable.up_0);
			if (infantC<=0){
				aq.id(R.id.booking_minus_infant).clickable(false);
				aq.id(R.id.minus_infant_indi).image(R.drawable.down_1);
			}else{
				aq.id(R.id.booking_minus_infant).clickable(true);
				aq.id(R.id.minus_infant_indi).image(R.drawable.down_0);
			}
		}
		
	}
	
	
	
	public void switchFragment(int fragID){
		
		selectedFragment = fragID;
		
		//getSupportActionBar().setLogo(R.drawable.actionbar_back_button_selector);

		Bundle extras = new Bundle();
		switch(fragID)
		{
			case FragmentCase.FRAG_USER_PROFILE:
				
				ProfileFragment profileF = new ProfileFragment();
				getSupportActionBar().hide();
				swapFragment(profileF);
			break;
			
			case FragmentCase.FRAG_INFO_PAGE:
				MyProfileFragment myProfileF = new MyProfileFragment();
				extras.putInt("type", MyProfileFragment.MY_PROFILE);
				myProfileF.setArguments(extras);
				getSupportActionBar().show();
				swapFragment(myProfileF);
				break;
			case FragmentCase.FRAG_MY_DOCUMENT:
				MyProfileFragment myDocument = new MyProfileFragment();
				extras.putInt("type", MyProfileFragment.MY_DOCUMENT);
				myDocument.setArguments(extras);
				getSupportActionBar().show();
				swapFragment(myDocument);
				break;
			case FragmentCase.FRAG_MY_FREN:
				FrenFamilyFragment frenFamily = new FrenFamilyFragment();
				getSupportActionBar().show();
				swapFragment(frenFamily);
				break;
			case FragmentCase.FRAG_CHECKIN_BOOKING:
				CheckInFragment checkInBooking = CheckInFragment.newInstance(fragID);
				getSupportActionBar().show();
				hideButton(R.id.rightButton_view, false);
				hideButton(R.id.leftButton_view, true);
				updateTitle(getString(R.string.check_in_title), false, true);
				swapFragment(checkInBooking);
				break;
			case FragmentCase.FRAG_CHECKIN_TNC:
			case FragmentCase.FRAG_CHECK_IN:
			case FragmentCase.FRAG_LOGIN_CHECKIN:
				CheckInFragment checkIn = CheckInFragment.newInstance(fragID);
				getSupportActionBar().show();
				hideButton(R.id.rightButton_view, false);
				hideButton(R.id.leftButton_view, true);
				updateTitle(getString(R.string.check_in_title), false, true);
				
				swapFragment(checkIn);
				break;
			case FragmentCase.FRAG_BOARDING_PASS:
				BoardingPassFragment boardingpass = BoardingPassFragment.newInstance(fragID);
				getSupportActionBar().show();
				hideButton(R.id.rightButton_view, false);
				hideButton(R.id.leftButton_view, true);
				updateTitle(getString(R.string.boardingpass), false, true);				
				swapFragment(boardingpass);
				break;
//			case FragmentCase.FRAG_FARE_DISPLAY:
			case FragmentCase.FRAG_BOOK_FLIGHTS:
				BookingFragment bFrag = BookingFragment.newInstance(fragID);
				hideButton(R.id.rightButton_view, false);
				hideButton(R.id.leftButton_view, true);
				updateTitle(getString(R.string.search_flights), false, true);
				swapFragment(bFrag);
				saerchItem = null;
				selectedFligh = null;
				aq.id(R.id.summary_bar).gone();
				resetPassangerCount();
				break;
			case FragmentCase.FRAG_FARE_SUMMARY:
				BookingFragment sFrag = BookingFragment.newInstance(fragID, selectedFligh);
				hideButton(R.id.rightButton_view, false);
				hideButton(R.id.leftButton_view, true);
				updateTitle(getString(R.string.search_flights), false, true);
				swapFragment(sFrag);
				break;
			case FragmentCase.FRAG_SSR_LISTING:
				SSRFragment ssrFrag = SSRFragment.newInstance(fragID, ssrItem);
				hideButton(R.id.rightButton_view, false);
				hideButton(R.id.leftButton_view, true);
				updateTitle(getString(R.string.add_ons), false, true);
				swapFragment(ssrFrag);
				break;
			case FragmentCase.FRAG_PAYMENT_SUMMARY:
				PaymentFragment payFrag = PaymentFragment.newInstance(ssrItem, currentBooking);
				hideButton(R.id.rightButton_view, false);
				hideButton(R.id.leftButton_view, true);
				updateTitle(getString(R.string.summary), false, true);
				aq.id(R.id.summary_bar).gone();
				swapFragment(payFrag);
				break;
			default://home
//				aq.id(R.id.leftButton).text("Login now!");
				//getSupportActionBar().setLogo(R.drawable.btn_login);
				HomeFragment homeFrag = new HomeFragment();
				getSupportActionBar().show();
				LogoutSessionIfValid();
				hideButton(R.id.rightButton_view, false);
				hideButton(R.id.leftButton_view, false);
				updateTitle("", true, false);
				swapFragment(homeFrag);
				aq.id(R.id.summary_bar).gone();
			break;
				
		}
		

		viewHandler = new Handler();
		viewHandler.postDelayed(updateViewRun, 400);
		
	}
	
	void LogoutSessionIfValid(){
		String session = prefs.getString(PrefsHolder.SESSION_TICKET, "");
		if (session != null || session.length()>0){
			ConstantHolder.LOGIN_STATUS = 3;
			Intent i = new Intent(MainActivity.this, SessionService.class);
			startService(i);
			
		}
		if (timerHandler!=null){
			timerHandler.removeCallbacks(sessionRunnable);
		}
	}
	
	void swapFragment(Fragment frag){
		FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

		transaction.disallowAddToBackStack()
		// .setCustomAnimations(R.anim.slide_in_left,
		// R.anim.slide_out_right)
				.replace(R.id.main_content, frag).commitAllowingStateLoss();
		
		
	}
	
	private void initDrawer() {
		mDrawerLayout = (DrawerLayout)findViewById(R.id.activity_tabs_drawer_layout);
		mDrawerList = (ListView)findViewById(R.id.google_drawer);
		//mDrawerLayout.setDrawerListener(createDrawerToggle());
		
		drawerList = DrawerModel.generateDrawerLsit(this);
		
		DrawerAdapter mAdapter = new DrawerAdapter(drawerList, this,  aq.id(R.id.google_drawer).getView());
		
		mDrawerList.setAdapter(mAdapter);
		mDrawerList.setOnItemClickListener(this);
	}
	
//	@SuppressWarnings("deprecation")
//	private DrawerListener createDrawerToggle() {
//		mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.drawable.ic_launcher, R.string.drawer_open, R.string.drawer_close) {
//			
//			@Override
//			public void onDrawerClosed(View view) {
//				super.onDrawerClosed(view);
//			}
//			
//			@Override
//			public void onDrawerOpened(View drawerView) {
//				super.onDrawerOpened(drawerView);
//			}
//			
//			@Override
//			public void onDrawerStateChanged(int state) {
//			}
//		};
//		return mDrawerToggle;
//	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
		mDrawerLayout.closeDrawer(aq.id(R.id.google_drawer_layout).getView());
		
		if(drawerList.get(position).getPosition()==selectedFragment)
		{
			LogHelper.debug("is selected fragment");
			return;
		}
		
		switchFragment(drawerList.get(position).getPosition());

	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    switch (item.getItemId()) {
	      case android.R.id.home:
	    	  
	    	  leftPressed(null);
	    	  
	    		  
	    }
	    return true;
	}
	
	 @Override
     public boolean onKeyDown(int keyCode, KeyEvent event)  {
         if (keyCode == KeyEvent.KEYCODE_BACK) {
        	 if(mDrawerLayout.isDrawerOpen(aq.id(R.id.google_drawer_layout).getView())){
        	 	mDrawerLayout.closeDrawer(aq.id(R.id.google_drawer_layout).getView());
        	 	return true;
        	 }
        	 switch(selectedFragment)
        	 {
        	 	case FragmentCase.FRAG_USER_PROFILE:
        	 		switchFragment(FragmentCase.FRAG_HOME);
        	 		return true;
        	 	case FragmentCase.FRAG_CHECKIN_TNC:
        	 		switchFragment(FragmentCase.FRAG_CHECK_IN);
        	 		aq.id(R.id.check_in_submit).visible();
					handler = new Handler();
					handler.postDelayed(bookingRun, 100);
        	 		return true;
        	 	default:
        	 		switchFragment(FragmentCase.FRAG_HOME);
        	 		return true;
        	 	case FragmentCase.FRAG_FARE_DISPLAY:
        	 	case FragmentCase.FRAG_FARE_LIST:
        	 	case FragmentCase.FRAG_FARE_SUMMARY:
        	 	case FragmentCase.FRAG_GUEST_DETAILS:
        	 	case FragmentCase.FRAG_SSR_LISTING:
        			aq.id(R.id.summary_bar).gone();
        	 		switchFragment(FragmentCase.FRAG_BOOK_FLIGHTS);
        	 		LogoutSessionIfValid();
        	 		return true;
        	 	case FragmentCase.FRAG_HOME:
        	 		return super.onKeyDown(keyCode, event);
        	 }
        	 
        	 
        	 
         }

         return super.onKeyDown(keyCode, event);
     }
	 
	 
	 public void rightPressed(View view)
	 {
		 mDrawerLayout.openDrawer(aq.id(R.id.google_drawer_layout).getView());
	 }
	 
	 public void leftPressed(View view)
	 {
		 switch(selectedFragment)
    	 {
    	 	case FragmentCase.FRAG_HOME:
    	 		if (prefs.getBoolean(PrefsHolder.IS_LOGIN, false)){
    	 			switchFragment(FragmentCase.FRAG_USER_PROFILE);
    	 		}else{
    	 			Intent loginActivity = new Intent(MainActivity.this, LoginActivity.class);
    	 			Bundle extras = new Bundle();
    	 			extras.putInt("frag_type", 1);
    	 			loginActivity.putExtras(extras);
    	 			startActivity(loginActivity);
    	 		}
    	 		break;
    	 	case FragmentCase.FRAG_USER_PROFILE:
    	 		switchFragment(FragmentCase.FRAG_HOME);
    	 		break;
    	 	case FragmentCase.FRAG_MY_DOCUMENT:
    	 	case FragmentCase.FRAG_MY_BOOKING:
    	 	case FragmentCase.FRAG_MY_FREN:
    	 	case FragmentCase.FRAG_INFO_PAGE:
	 			switchFragment(FragmentCase.FRAG_USER_PROFILE);
    	 		break;
    	 	default:
    	 		{
    	 			
    	 		}
    	 		break;
    	 }
	 }
	 
	 public static void showErrorMessage(Context context, String errorMsg){
			AlertDialog.Builder builder = new AlertDialog.Builder(context);
			builder.setMessage(errorMsg);
			builder.setCancelable(true);
			builder.setPositiveButton(context.getResources().getString(R.string.ok_text),
	                new DialogInterface.OnClickListener() {
	            public void onClick(DialogInterface dialog, int id) {
	                dialog.cancel();
	            }
	        });
	        AlertDialog alert = builder.create();
	        alert.show();
		}
	 
	 public void showSuccessMessage(Context context, String errorMsg){
			AlertDialog.Builder builder = new AlertDialog.Builder(context);
			builder.setMessage(errorMsg);
			builder.setCancelable(true);
			builder.setPositiveButton(context.getResources().getString(R.string.ok_text),
	                new DialogInterface.OnClickListener() {
	            public void onClick(DialogInterface dialog, int id) {
	                dialog.cancel();
	                switchFragment(FragmentCase.FRAG_HOME);
	            }
	        });
	        AlertDialog alert = builder.create();
	        alert.show();
	}
	 
	 public void CheckIn(View v){
		 ConstantHolder.LOGIN_STATUS = 1;
		 ConstantHolder.SESSION_TYPE = 1;
		 if (!prefs.getBoolean(PrefsHolder.IS_LOGIN, false)){
			 ConstantHolder.SESSION_TYPE = 1;
			 switchFragment(FragmentCase.FRAG_CHECKIN_BOOKING);
			 Intent sessionService = new Intent(MainActivity.this, SessionService.class);
			 startService(sessionService);
		 }else{
			 switchFragment(FragmentCase.FRAG_LOGIN_CHECKIN);
			 connGetSession(1);
		 }
	 }
	 public void BoardingPass(View v){
		/* Intent i = new Intent(MainActivity.this,SeatMapActivity.class);
		 startActivity(i);
		 MainActivity.this.finish();
		 overridePendingTransition(R.anim.fade_in, R.anim.fade_out);*/
		 ConstantHolder.LOGIN_STATUS = 1;
		 ConstantHolder.SESSION_TYPE = 1;
		 if (!prefs.getBoolean(PrefsHolder.IS_LOGIN, false)){
			 ConstantHolder.SESSION_TYPE = 1;
			 switchFragment(FragmentCase.FRAG_BOARDING_PASS);
			 Intent sessionService = new Intent(MainActivity.this, SessionService.class);
			 startService(sessionService);
		 }else{
			 switchFragment(FragmentCase.FRAG_LOGIN_CHECKIN);
			 connGetSession(1);
		 }
		
	 }
	public void ManageBooking(View v){
		Intent i = new Intent(MainActivity.this,Add_OnsActivity.class);
		startActivity(i);
		MainActivity.this.finish();
		overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
	}
	 
	 public void SetDeparture(final View v){
		
		Intent i = new Intent(MainActivity.this, SearchStateActivity.class);
		Bundle extras = new Bundle();
		extras.putInt("viewId", v.getId());
		i.putExtras(extras);
		startActivityForResult(i, PICK_STATE_REQUEST);
			
	}
	
	public void connGetBooking(final int type, final String name, String id, final String stationCode, final boolean isLogin){
		SharedPreferences prefs = getSharedPreferences(PrefsHolder.PREFS, Context.MODE_PRIVATE);
		
		RequestParams params = new RequestParams();
		params.put("request_type", ""+type);
		if (id!=null && id.length()>0){
			params.put("record_locator", id);//QFLHUE
		}
		params.put("userSession", prefs.getString(PrefsHolder.SESSION_TICKET, ""));
		CustomHttpClient.postWithHash(CustomHttpClient.GET_BOOKING, params, new AsyncHttpResponseHandler() {
			
			@Override
			public void onStart() {
				// TODO Auto-generated method stub
				if (progress==null){
					progress = ProgressDialog.show(MainActivity.this, "Get Booking", "Loading");
				}
				super.onStart();
				
			}

			@Override
			public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
				// TODO Auto-generated method stub
				String response = new String(responseBody);
				response = response.replace("null", "\"\"");
				int responseCode = JSonHelper.GetResponseStatus(response);

				LogHelper.debug("Checkin booking = " + response);
				
				if(responseCode == JSonHelper.SUCCESS_REPONSE){
					String data = JSonHelper.JSonString(response, "data");
					
					String bookingData = JSonHelper.JSonString(data, "BookingData");

					List<MemberInfoModel> passengerList = JSonHelper.parsePassenger(bookingData);
					boolean customerExist = false;
					if (!isLogin){
						for (int c = 0; c < passengerList.size(); c++){
							LogHelper.debug(passengerList.get(c).getlName() + " = "+ name);
							if (passengerList.get(c).getlName().toLowerCase().equals(name.toLowerCase())){
								customerExist =true;
							}
						}
						if (!customerExist){
							showErrorMessage(MainActivity.this, getString(R.string.check_in_no_name));
							return;
						}

					}
					
					currentBooking = JSonHelper.parseBookingInfo(bookingData, MainActivity.this	);
					currentBooking.setMemberList(passengerList);
					if (!isLogin){
						if(!currentBooking.getDepartJourney().get(0).getName().equals(stationCode)){
							showErrorMessage(MainActivity.this, getString(R.string.check_in_no_name));
							return;
						}
					}
					
					LogHelper.debug("Infacnet count " + infantC);
					currentBooking.setInfantCount(infantC);
					currentBooking.setTaxesPrice(false);
					currentBooking.setTaxesPrice(true);
					currentBooking.generateEmptyMeal();
					
					
				}else{
					String error = JSonHelper.GetResponseMessage(response);
					showErrorMessage(MainActivity.this, error);
					if (progress!=null){
						progress.dismiss();
					}
				}
			}
			
			@Override
			public void onFailure(int statusCode, Header[] headers,
					byte[] responseBody, Throwable error) {
				// TODO Auto-generated method stub
				showErrorMessage(MainActivity.this, getString(R.string.connection_error));
				if (progress!=null){
					progress.dismiss();
				}
			}

			@Override
			public void onFinish() {
				// TODO Auto-generated method stub
				if (progress !=null){
					progress.dismiss();
				}
				if (type == 1){
					setupGuestPage();
					setupSummaryBar();
				}else{
					selectedFragment = FragmentCase.FRAG_CHECK_IN;
					CheckInFragment f = CheckInFragment.newInstance(selectedFragment);				
					swapFragment(f);
					
					hideButton(R.id.rightButton_view, false);
					hideButton(R.id.leftButton_view, true);

					handler = new Handler();
					handler.postDelayed(bookingRun, 100);
				}
				super.onFinish();
			}
			
			
		});
		
	}
	

	void initBookingList(){
		LogHelper.debug("Update booking List");
		BookingAdapter adapter = new BookingAdapter(currentBooking,
									MainActivity.this,
									aq.id(R.id.expandable_list).getView());
		aq.id(R.id.expandable_list).getExpandableListView().setAdapter(adapter);
		aq.id(R.id.expandable_list).getExpandableListView().expandGroup(0);
		aq.id(R.id.expandable_list).getExpandableListView().expandGroup(1);
		aq.id(R.id.expandable_list).getExpandableListView().setOnGroupClickListener(new OnGroupClickListener() {
			
			@Override
			public boolean onGroupClick(ExpandableListView parent, View v,
					int groupPosition, long id) {
				// TODO Auto-generated method stub
				return true;
			}
		});

		aq.id(R.id.expandable_list).getExpandableListView().setOnChildClickListener(new OnChildClickListener() {
			
			@Override
			public boolean onChildClick(ExpandableListView parent, View v,
					int groupPosition, int childPosition, long id) {
				// TODO Auto-generated method stub
				BookingAdapter adapter = (BookingAdapter) aq.id(R.id.expandable_list).getExpandableListView().getExpandableListAdapter();
				MemberInfoModel mMember = (MemberInfoModel) adapter.getChild(groupPosition, childPosition);
				List<List<CheckInModel>> checkList = mMember.getCheckinList();
				if (groupPosition<checkList.get(groupPosition).size()){
					for (int x = 0; x<checkList.get(groupPosition).size() ; x++){
						if (checkList.get(groupPosition).get(x).getLiftStatus().equalsIgnoreCase("CheckedIn")){
							return false;
						}
					}
				}
				
				if (currentBooking.isInternational()){
					Intent i = new Intent(MainActivity.this, CheckDetailsActivity.class);
					LogHelper.debug("Member Name = " + mMember.getFullName());
					Bundle extra = new Bundle();
					extra.putString("name", mMember.getfName());
					extra.putString("dob", mMember.getDob());
					extra.putString("national", mMember.getNationality());
					extra.putString("passport", mMember.getPassport());
					extra.putString("issue", mMember.getIssuingCountry());
					extra.putString("expDate", mMember.getExpiredDate());
					extra.putInt("parent", groupPosition);
					extra.putInt("child", childPosition);
					
					i.putExtras(extra);
					MainActivity.this.startActivityForResult(i, MEMBER_CHECKIN_REQUEST);
				}else{
					adapter.updateInforComplete(groupPosition, childPosition);
					List<MemberInfoModel> memberList = adapter.getAdapterMembers();
					boolean checkIn = false;
					for (int i = 0 ;i <memberList.size(); i++){
						MemberInfoModel m = memberList.get(i);
						if(m.isDepart() || m.isReturn()){
							checkIn = true;
							LogHelper.debug("Detect user got Checkin");
							break;
						}else{
							checkIn = false;
						}
					}
					if (checkIn==true){
						aq.id(R.id.check_in_submit).visible();
					}else{
						aq.id(R.id.check_in_submit).gone();
					}

				}

				return false;
			
			}
		});
	}
	
	
	//DorR -> Depart or return
	JSONArray converToJSonSeat(List<SegmentModel> segMent, List<MemberInfoModel> peopleCheck
					, boolean isDepart){
		JSONArray parentArr = null;
		JSONArray jsonSeg = null;
//		JSONArray jArr = new JSONArray();
		try{
			parentArr = new JSONArray();
				
			for (int i = 0;i < segMent.size(); i ++){
				SegmentModel seg = segMent.get(i);
				jsonSeg = new JSONArray();
				for (int p = 0 ; p < peopleCheck.size(); p ++){
					JSONObject json = new JSONObject();
					MemberInfoModel tempP = peopleCheck.get(p);
					if ((tempP.isDepart() && isDepart) || (tempP.isReturn() && !isDepart)){
						if (seg.getSeatList().get(p).getSeatNum()== null 
								|| seg.getSeatList().get(p).getSeatNum().length() < 0
								|| seg.getSeatList().get(p).getSeatNum().isEmpty()){
							json.put("CarrierCode", seg.getCarrierCode());
							json.put("FlightNo", seg.getFlgithNum());
							json.put("DepartureStation", seg.getDepartStation());
							json.put("ArrivalStation", seg.getArriveStation());
							json.put("STD", seg.getDataSTD());
							json.put("PassengerID", ""+tempP.getPid());
							json.put("FirstName", tempP.getfName());
							json.put("LastName", tempP.getlName());
							json.put("Nationality", tempP.getNationality());
							json.put("EquipmentType", seg.getSeatList().get(p).getEquipType());
							json.put("EquipmentTypeSuffix", seg.getSeatList().get(p).getEquipSuffix());
							json.put("PRBCCode", seg.getSeatList().get(p).getPRBCode());
							json.put("PassengerNumber", ""+tempP.getPassangerNum());
							json.put("DOB", tempP.getDob());
							json.put("PaxType", tempP.getPaxType());
							
							
							
							LogHelper.debug("Add Person in segment");
							jsonSeg.put(json);
						}
					}
				}
				if (jsonSeg.length()>0){
					parentArr.put(jsonSeg);
				}

//				jArr.put(jsonSeg);
			}

			
		}catch (Exception e){
			e.printStackTrace();
		}
		return parentArr;
	}
	
	JSONArray converToJSonCheckIn(List<SegmentModel> segMent, List<MemberInfoModel> peopleCheck
			, boolean isDepart){
		JSONArray parentArr = null;
		JSONArray jsonSeg = null;
		//JSONArray jArr = new JSONArray();
		try{
			parentArr = new JSONArray();
			jsonSeg = new JSONArray();
				
			for (int i = 0;i < segMent.size(); i ++){
				SegmentModel seg = segMent.get(i);
				jsonSeg = new JSONArray();
				for (int p = 0 ; p < peopleCheck.size(); p ++){
					JSONObject json = new JSONObject();
					MemberInfoModel tempP = peopleCheck.get(p);
					if ((tempP.isDepart() && isDepart) || (tempP.isReturn() && !isDepart)){
						json.put("CarrierCode", seg.getCarrierCode());
						json.put("FlightNo", seg.getFlgithNum());
						json.put("DepartureStation", seg.getDepartStation());
						json.put("ArrivalStation", seg.getArriveStation());
						json.put("STD", seg.getDataSTD());
						json.put("PassengerID", ""+tempP.getPid());
						json.put("FirstName", tempP.getfName());
						json.put("LastName", tempP.getlName());
						json.put("Nationality", tempP.getNationality());
						
						if(seg.isInternational()){
							String status = "";
							if (isDepart){
								status = tempP.getDepartDoc().get(i).getStatus();
							}else{
								status = tempP.getReturnDoc().get(i).getStatus();
							}

							LogHelper.debug("Add verify doc segment " + status);
							json.put("verifyTravelDocStatus",status );
						}
						if(json.length()>0){
							jsonSeg.put(json);
						}
					}
				}
				if (jsonSeg.length()>0){
					parentArr.put(jsonSeg);
				}
		//		jArr.put(jsonSeg);
			}
		
		}catch (Exception e){
			e.printStackTrace();
		}
		return parentArr;
	}
	
	JSONArray converToJSonVerify(List<SegmentModel> segMent, List<MemberInfoModel> peopleCheck
			, boolean isDepart){
		JSONArray segArr = null;
		JSONArray pplArr = null;
		//JSONArray jArr = new JSONArray();
		try{
			segArr = new JSONArray();
			LogHelper.debug("Segment Size " + segMent.size());
			for (int i = 0; i<segMent.size(); i ++){
				SegmentModel seg = segMent.get(i);
				pplArr = new JSONArray();
				for (int p = 0 ; p < peopleCheck.size(); p ++){
					JSONObject json = new JSONObject();
					MemberInfoModel tempP = peopleCheck.get(p);
					if ((tempP.isDepart() && isDepart) || (tempP.isReturn() && !isDepart)){
						json.put("Nationality", tempP.getNationality());
						json.put("DOB", tempP.getDob());
						json.put("DocumentNumber", tempP.getPassport());
						json.put("ExpiryDate", tempP.getExpiredDate());
						json.put("IssuingCountry", tempP.getIssuingCountry());
						json.put("ArrivalStation", seg.getArriveStation());
						json.put("ArrivalCountry", seg.getArrTimeZone());
						json.put("STA", seg.getDateSTA());
						json.put("CarrierCode", seg.getCarrierCode());
						json.put("DepartureStation", seg.getDepartStation());
						json.put("DepartureCountry", seg.getDepartTimeZone());
						json.put("STD", seg.getDataSTD());
						json.put("PassengerNumber", ""+tempP.getPassangerNum());
						json.put("FlightNo", seg.getFlgithNum());
						LogHelper.debug("Add Person in segment -> Verify");
						pplArr.put(json);
					}
				}
				if (pplArr.length()>0){
					segArr.put(pplArr);
				}
			}

			
			
		}catch (Exception e){
			e.printStackTrace();
		}
		return segArr;
	}
	
	JSONArray converToJSonUpdate(List<SegmentModel> segMent, List<MemberInfoModel> peopleCheck
			, boolean isDepart){
		JSONArray segArr = null;
		JSONArray pplArr = null;
		//JSONArray jArr = new JSONArray();
		try{
			segArr = new JSONArray();
			LogHelper.debug("Segment Size " + segMent.size());
			for (int i = 0; i<segMent.size(); i ++){
//				SegmentModel seg = segMent.get(i);
				pplArr = new JSONArray();
				for (int p = 0 ; p < peopleCheck.size(); p ++){
					JSONObject json = new JSONObject();
					MemberInfoModel tempP = peopleCheck.get(p);
					if ((tempP.isDepart() && isDepart) || (tempP.isReturn() && !isDepart)){
						

						json.put("DocumentNumber", tempP.getPassport());
						json.put("ExpiryDate", tempP.getExpiredDate());
						json.put("IssuingCountry", tempP.getIssuingCountry());
						json.put("DOB", tempP.getDob());
						json.put("Nationality", tempP.getNationality());
						json.put("Title", tempP.getTitle());
						json.put("FirstName", tempP.getfName());
						json.put("LastName", tempP.getlName());
						json.put("PassengerID", ""+tempP.getPid());
						json.put("PassengerNumber", ""+tempP.getPassangerNum());
						json.put("PaxType", tempP.getPaxType());
//						json.put("FlightNo", seg.getFlgithNum());
						
						pplArr.put(json);
					}
				}
				if (pplArr.length()>0){
					segArr.put(pplArr);
				}
			}

			
			
		}catch (Exception e){
			e.printStackTrace();
		}
		return segArr;
	}
	
	
//	void connCheckIn(){
//		RequestParams params = new RequestParams();
//		
//		CustomHttpClient.postWithHash(CustomHttpClient.CHECK_IN, params, new AsyncHttpResponseHandler() {
//			
//			@Override
//			public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
//				// TODO Auto-generated method stub
//				
//			}
//			
//			@Override
//			public void onFailure(int statusCode, Header[] headers,
//					byte[] responseBody, Throwable error) {
//				// TODO Auto-generated method stub
//				
//			}
//		});
//	}
	
	public void SignOut(View v){
		connSignOut();
	}
	
	void connSignOut(){
		if (prefs==null){
			prefs = getSharedPreferences(PrefsHolder.PREFS, MODE_PRIVATE);
		}
		RequestParams params = new RequestParams();
		params.put("customerId", ""+ prefs.getInt(PrefsHolder.USER_CID, 0));
		params.put("username", prefs.getString(PrefsHolder.USER_USERNAME, ""));
		params.put("ssoTicket", prefs.getString(PrefsHolder.SSO_TICKET, ""));
		
		CustomHttpClient.postWithHashWithoutServer(CustomHttpClient.MEMBER_LOGOUT, params, new AsyncHttpResponseHandler() {
			
			@Override
			public void onStart() {
				// TODO Auto-generated method stub
				progress = ProgressDialog.show(MainActivity.this, "Logout", "Loading");
				super.onStart();
			}

			@Override
			public void onFinish() {
				// TODO Auto-generated method stub
				
				switchFragment(FragmentCase.FRAG_HOME);
				
				SharedPreferences.Editor editor = prefs.edit();
				editor.putInt(PrefsHolder.USER_CID, 0);
				editor.putString(PrefsHolder.USER_USERNAME, "");
				editor.putString(PrefsHolder.USER_FIRSTNAME, "");
				editor.putString(PrefsHolder.USER_LASTNAME, "");
				editor.putString(PrefsHolder.USER_ADDRESS1, "");
				editor.putString(PrefsHolder.USER_ADDRESS2, "");
				editor.putString(PrefsHolder.USER_CITY, "");
				editor.putString(PrefsHolder.USER_COUNTRYCODE, "");
				editor.putString(PrefsHolder.USER_CULTURECODE, "");
				editor.putInt(PrefsHolder.USER_GENDER,0);
				editor.putString(PrefsHolder.USER_NATIONALITY, "");
				editor.putString(PrefsHolder.USER_PERSONALEMAIL, "");
				editor.putString(PrefsHolder.USER_POSTCODE, "");
				editor.putString(PrefsHolder.USER_STATE, "");
				editor.putString(PrefsHolder.USER_TITLE, "");
				editor.putString(PrefsHolder.USER_DOB, "");
				editor.putInt(PrefsHolder.USER_PID, 0);
				editor.putString(PrefsHolder.USER_MOBILE, "");
				editor.putInt(PrefsHolder.USER_NOTIFPREF, 0);
				editor.putBoolean(PrefsHolder.USER_ISBIGC, false);
				editor.putInt(PrefsHolder.USER_BIG_ID, 0);
				editor.putString(PrefsHolder.SSO_TICKET, "");
				
//				editor.putString(PrefsHolder.USER_TICKET, memberInfo[3]);
				editor.putBoolean(PrefsHolder.IS_LOGIN, false);
				editor.commit();
				hideButton(R.id.leftButton_text, false);
				
				if(progress !=null){
					progress.dismiss();
				}
				super.onFinish();
			}

			@Override
			public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
				// TODO Auto-generated method stub
				String response = new String(responseBody);
				int responseCode = JSonHelper.GetResponseStatus(response);
				if (responseCode == JSonHelper.SUCCESS_REPONSE){
					
					
				}
				
			}
			
			@Override
			public void onFailure(int statusCode, Header[] headers,
					byte[] responseBody, Throwable error) {
				// TODO Auto-generated method stub
				
			}
		});
	}
	
	public void switchQRImage(View v){
		if (v.getId() == R.id.right_profile_view){
			aq.id(R.id.right_profile_view).gone();
			aq.id(R.id.left_profile_view).visible();
			aq.id(R.id.profile_image).image(R.drawable.potrait_0);
		}else{
			aq.id(R.id.right_profile_view).visible();
			aq.id(R.id.left_profile_view).gone();
			aq.id(R.id.profile_image).image(R.drawable.vr_1);
		}
	}
	
	public void timerSession(){
		if (timerHandler == null){
			timerHandler = new Handler();
		}
		timerHandler.postDelayed(sessionRunnable, 480000);
	}
	
	public void removeTimerSession(){
		if (timerHandler != null){
			timerHandler.removeCallbacks(sessionRunnable);
		}
	}
	
	Runnable sessionRunnable = new Runnable() {
		
		@Override
		public void run() {
			// TODO Auto-generated method stub
			AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
			builder.setMessage(MainActivity.this.getResources().getString(R.string.continue_session));
			builder.setCancelable(false);
			builder.setPositiveButton(MainActivity.this.getResources().getString(R.string.ok_text),
	                new DialogInterface.OnClickListener() {
	            public void onClick(DialogInterface dialog, int id) {
	                dialog.cancel();
	                Intent i = new Intent(MainActivity.this, SessionService.class);
	    			startService(i);
	            }
	        });
			builder.setNegativeButton(getApplicationContext().getResources().getString(R.string.cancel_text),
	                new DialogInterface.OnClickListener() {
	            public void onClick(DialogInterface dialog, int id) {
	                dialog.cancel();
//	                switchFragment(FragmentCase.FRAG_HOME);
	                switchFragment(FragmentCase.FRAG_BOOK_FLIGHTS);
	            }
	        });
	        AlertDialog alert = builder.create();
	        alert.show();
			
		}
	};
	
	Runnable bookingRun = new Runnable() {
		
		@Override
		public void run() {
			// TODO Auto-generated method stub
			initBookingList();
		}
	};
	
	Runnable updateViewRun = new Runnable() {
		
		@Override
		public void run() {
			// TODO Auto-generated method stub
			switch (selectedFragment){
			case FragmentCase.FRAG_BOOK_FLIGHTS:
				String departName = prefs.getString(PrefsHolder.GEO_STATION, "");
				SQLhelper db = new SQLhelper(MainActivity.this);
				StationModel stationModel = db.getStationBaseKey(departName, 1);
				LogHelper.debug("Default Location " + departName);
				if (departName != null && departName.length()>0 && !departName.equals("null")){
					aq.id(R.id.booking_departure).text(stationModel.getNameWithKey());
					aq.id(R.id.booking_departure).getButton().setTag(stationModel.getKey());
				}
				Date date = new Date();
				SimpleDateFormat format = new SimpleDateFormat("EEE',' dd MMM yyyy");
				aq.id(R.id.booking_d_time).text(format.format(date));
				updatePassangerCount();
				break;
			default:
				connGetGeoIP();
				break;
			}
		}
	};
	
	void connAutoSeat(JSONArray jArr){
		RequestParams params = new RequestParams();
		params.put("record_locator", currentBooking.getRecordLocator());
		params.put("booking_id", currentBooking.getId());
		params.put("passenger_data", jArr.toString());
		params.put("userSession", prefs.getString(PrefsHolder.SESSION_TICKET, ""));
		
		CustomHttpClient.postWithHash(CustomHttpClient.AUTO_ASSIGN_SEAT , params, new AsyncHttpResponseHandler() {
			
			boolean isSuccess = false;
			
			@Override
			public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
				// TODO Auto-generated method stub
				String response = new String(responseBody);
				LogHelper.debug("Response Seat " + response);
				int responseCode = JSonHelper.GetResponseStatus(response);
				if (responseCode == JSonHelper.SUCCESS_REPONSE){
					isSuccess = true;
				}else{
					isSuccess = false;
					String responseMsg = JSonHelper.GetResponseMessage(response);
					showErrorMessage(MainActivity.this, responseMsg);
				}
			}
			
			@Override
			public void onFailure(int statusCode, Header[] headers,
					byte[] responseBody, Throwable error) {
				// TODO Auto-generated method stub
				if (progress!=null){
					progress.dismiss();
				}
				showErrorMessage(MainActivity.this, getString(R.string.connection_error));
			}

			@Override
			public void onFinish() {
				// TODO Auto-generated method stub
				if (isSuccess){
					connCheckInFlight();
				}else{
					if (progress!=null){
						progress.dismiss();
					}
				}
				super.onFinish();
			}
		});

	}
	
	void connCheckInFlight(){
		
		JSONArray journeyArr = new JSONArray();
		JSONArray deCheckArr = converToJSonCheckIn(currentBooking.getDepartSegments(),
				currentBooking.getMemberList(), true);
		JSONArray returnArr = converToJSonCheckIn(currentBooking.getReturnSegments(),
				currentBooking.getMemberList(), false);
		try{
			if (deCheckArr.length()>0){
				journeyArr.put(deCheckArr);
			}
			if (returnArr.length()>0){
				journeyArr.put(returnArr);
			}
			LogHelper.debug("Journey Arr " + journeyArr);
		}catch(Exception e){
			
		}
		
		RequestParams params = new RequestParams();
		params.put("record_locator", currentBooking.getRecordLocator());
		params.put("International", ""+currentBooking.isInternational());
		params.put("passenger_data", journeyArr.toString());
		params.put("userSession", prefs.getString(PrefsHolder.SESSION_TICKET, ""));
		
		CustomHttpClient.postWithHash(CustomHttpClient.CHECK_IN, params, new AsyncHttpResponseHandler() {
			
			@Override
			public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
				// TODO Auto-generated method stub
				String response= JSonHelper.removeNull(responseBody);
				
				int responseCode = JSonHelper.GetResponseStatus(response);
				LogHelper.debug(responseCode +" Response Check in  " + response);
				
				if (responseCode == JSonHelper.SUCCESS_REPONSE){
//					String barcode = JSonHelper.parseBarCode(response);
//					String responseMsg = JSonHelper.GetResponseMessage(response);
//					showErrorMessage(MainActivity.this, responseMsg);

					//Testing without barcode
					showSuccessMessage(MainActivity.this, "Success Check in");
				}else{
					
				}
			}
			
			@Override
			public void onFailure(int statusCode, Header[] headers,
					byte[] responseBody, Throwable error) {
				// TODO Auto-generated method stub
				if (progress!=null){
					progress.dismiss();
				}
				showErrorMessage(MainActivity.this, getString(R.string.connection_error));
			}

			@Override
			public void onFinish() {
				// TODO Auto-generated method stub
				if (progress!=null){
					progress.dismiss();
				}
				super.onFinish();
			}
			
			
		});
	}
	
	void connVerifyDoc(){
		
		JSONArray journeyArr = new JSONArray();
		JSONArray deCheckArr = converToJSonVerify(currentBooking.getDepartSegments(),
				currentBooking.getMemberList(), true);
		JSONArray returnArr = converToJSonVerify(currentBooking.getReturnSegments(),
				currentBooking.getMemberList(), false);
		try{
			if (deCheckArr.length()>0){
				journeyArr.put(deCheckArr);
			}
			if (returnArr.length()>0){
				journeyArr.put(returnArr);
			}
			LogHelper.debug("Verify Arr " + journeyArr);
		}catch(Exception e){
			
		}
		
		RequestParams params = new RequestParams();
		params.put("record_locator", currentBooking.getRecordLocator());
		params.put("passenger_data", journeyArr.toString());
		params.put("userSession", prefs.getString(PrefsHolder.SESSION_TICKET, ""));
		
		CustomHttpClient.postWithHash(CustomHttpClient.VERIFY_DOC, params, new AsyncHttpResponseHandler() {
			boolean success = false; 
			
			@Override
			public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
				// TODO Auto-generated method stub
				String response= JSonHelper.removeNull(responseBody);
				
				int responseCode = JSonHelper.GetResponseStatus(response);
				LogHelper.debug("Response Verify in  " + response);
				
				if (responseCode == JSonHelper.SUCCESS_REPONSE){
					success = true;
					JSonHelper.parseTravelDocStatus(response, currentBooking);
				}else{
					
				}
			}
			
			@Override
			public void onFailure(int statusCode, Header[] headers,
					byte[] responseBody, Throwable error) {
				// TODO Auto-generated method stub
				if (progress!=null){
					progress.dismiss();
				}
				showErrorMessage(MainActivity.this, getString(R.string.connection_error));
			}

			@Override
			public void onFinish() {
				// TODO Auto-generated method stub
				if (success){
					connUpdateUser();
				}else{
					if (progress!=null){
						progress.dismiss();
					}
				}
				super.onFinish();
			}
			
			
		});
	}
	
	void connUpdateUser(){
		
		JSONArray journeyArr = new JSONArray();
		JSONArray deCheckArr = converToJSonUpdate(currentBooking.getDepartSegments(),
				currentBooking.getMemberList(), true);
		JSONArray returnArr = converToJSonUpdate(currentBooking.getReturnSegments(),
				currentBooking.getMemberList(), false);
		try{
			if (deCheckArr.length()>0){
				journeyArr.put(deCheckArr);
			}
			if (returnArr.length()>0){
				journeyArr.put(returnArr);
			}
			LogHelper.debug("Update Arr " + journeyArr);
		}catch(Exception e){
			
		}
		
		RequestParams params = new RequestParams();
		params.put("record_locator", currentBooking.getRecordLocator());
		params.put("passenger_data", journeyArr.toString());
		params.put("userSession", prefs.getString(PrefsHolder.SESSION_TICKET, ""));
		
		CustomHttpClient.postWithHash(CustomHttpClient.UPDATE_DOC, params, new AsyncHttpResponseHandler() {
			boolean success = false; 
			
			@Override
			public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
				// TODO Auto-generated method stub
				String response= JSonHelper.removeNull(responseBody);
				
				int responseCode = JSonHelper.GetResponseStatus(response);
				LogHelper.debug("Response Update in  " + response);
				
				if (responseCode == JSonHelper.SUCCESS_REPONSE){
					success = true;
				}else{
				}
			}
			
			@Override
			public void onFailure(int statusCode, Header[] headers,
					byte[] responseBody, Throwable error) {
				// TODO Auto-generated method stub
//				LogHelper.debug("Fail " + new String(responseBody));
				if (progress!=null){
					progress.dismiss();
				}
				showErrorMessage(MainActivity.this, getString(R.string.connection_error));
			}

			@Override
			public void onFinish() {
				// TODO Auto-generated method stub
				if (success){
					CheckSeat();
				}else{
					if (progress!=null){
						progress.dismiss();
					}
				}
				super.onFinish();
			}
			
			
		});
	}
	
	void CheckSeat(){

		if (!isSeatDesignate()){
			JSONArray parentArr= new JSONArray();
			JSONArray departArr = converToJSonSeat(currentBooking.getDepartSegments(),
					currentBooking.getMemberList(), true);
			JSONArray returnArr = converToJSonSeat(currentBooking.getReturnSegments(),
					currentBooking.getMemberList(), false);
			try {
				if (departArr.length()>0){
					parentArr.put(departArr);
				}
				if (returnArr.length()>0){
					parentArr.put(returnArr);
				}
				
				LogHelper.debug("Encode " + parentArr);
				connAutoSeat(parentArr);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}else{
//			currentBooking.g
			connCheckInFlight();
		}
	}
	
	boolean isSeatDesignate(){

		for (int i = 0; i <currentBooking.getDepartSegments().size(); i ++){
			SegmentModel seg = currentBooking.getDepartSegments().get(i);
			for (int childId = 0 ; childId < seg.getSeatList().size(); childId++){
				SeatSummary seat = seg.getSeatList().get(childId);
				if (seat.getSeatNum() != null && !seat.getSeatNum().isEmpty()){
					return true;
				}
			}
		}
		
		for (int x = 0; x <currentBooking.getDepartSegments().size(); x ++){
			SegmentModel seg = currentBooking.getReturnSegments().get(x);
			for (int childId = 0 ; childId < seg.getSeatList().size(); childId++){
				SeatSummary seat = seg.getSeatList().get(childId);
				if (seat.getSeatNum() != null && !seat.getSeatNum().isEmpty()){
					return true;
				}
			}
		}
		
		return false;
	}
	
	public void connGetUserBooking(int type){
		RequestParams params = new RequestParams();
		params.put("userSession", prefs.getString(PrefsHolder.SESSION_TICKET, ""));
		params.put("email", prefs.getString(PrefsHolder.USER_USERNAME, ""));
		params.put("type", ""+type);
		
		CustomHttpClient.postWithHash(CustomHttpClient.GET_USER_BOOKING, params, new AsyncHttpResponseHandler() {
			
			@Override
			public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
				// TODO Auto-generated method stub
				String response = JSonHelper.removeNull(responseBody);
				int responseCode = JSonHelper.GetResponseStatus(response);
				LogHelper.debug("get user booking " + response);
				if (responseCode == JSonHelper.SUCCESS_REPONSE){
					try{
						JSONObject jResponse = new JSONObject(response);
						JSONObject dataObj = jResponse.getJSONObject("data");
						List<FlightsModel> items = JSonHelper.parseFlightInfo(dataObj);
						setupFlightList(items);
					}catch (Exception e){
						e.printStackTrace();
					}
				}else{
					String errorMsg = JSonHelper.GetResponseMessage(response);
					showErrorMessage(MainActivity.this, errorMsg);
				}
				
			}
			
			@Override
			public void onFailure(int statusCode, Header[] headers,
					byte[] responseBody, Throwable error) {
				// TODO Auto-generated method stub

				showErrorMessage(MainActivity.this, getString(R.string.connection_error));
			}

			@Override
			public void onStart() {
				// TODO Auto-generated method stub
				super.onStart();
			}

			@Override
			public void onFinish() {
				// TODO Auto-generated method stub
				if (progress !=null){
					progress.dismiss();
				}
				super.onFinish();
			}
		});
	}
	

	public void setupFlightList(List<FlightsModel> items){
		FlightAdapter adapter = new FlightAdapter(items
					, MainActivity.this
					, aq.id(R.id.checkin_flights_list).getView());
		aq.id(R.id.checkin_flights_list).getExpandableListView().setAdapter(adapter);
		aq.id(R.id.checkin_flights_list).getExpandableListView().expandGroup(0);
		aq.id(R.id.checkin_flights_list).getExpandableListView().setOnGroupClickListener(new OnGroupClickListener() {
			
			@Override
			public boolean onGroupClick(ExpandableListView parent, View v,
					int groupPosition, long id) {
				// TODO Auto-generated method stub
				return true;
			}
		});
		aq.id(R.id.checkin_flights_list).getExpandableListView().setOnChildClickListener(new OnChildClickListener() {
			
			@Override
			public boolean onChildClick(ExpandableListView parent, View v,
					int groupPosition, int childPosition, long id) {
				// TODO Auto-generated method stub
				FlightAdapter mAdapter = (FlightAdapter) aq.id(R.id.checkin_flights_list)
											.getExpandableListView().getExpandableListAdapter();
				FlightsModel model = (FlightsModel) mAdapter.getChild(groupPosition, childPosition);
				connGetBooking(0, "", model.getRecordLocator(), "", true);
				return false;
			}
		});
		
	}
	
	private void connGetSession(final int type){
		
		LogHelper.debug("Sesse - > Type " + ConstantHolder.LOGIN_STATUS +"\nSesse - > from " + ConstantHolder.SESSION_TYPE);
		RequestParams params = new RequestParams();
		params.put("request_from", ""+ConstantHolder.SESSION_TYPE);
		params.put("request_type", ""+ConstantHolder.LOGIN_STATUS);
		
		CustomHttpClient.postWithHash(CustomHttpClient.GET_SESSION, params, new AsyncHttpResponseHandler() {
			
			boolean success = false; 
			@Override
			public void onFinish() {
				// TODO Auto-generated method stub
				if (success){
					if (type==1){
						if (progress!=null){
							progress.dismiss();
						}
						connGetUserBooking(1);
					}else{
						LogHelper.debug("start Get Flight");
						searchFlight();
					}
				}else{
					if (progress!=null){
						progress.dismiss();
					}
				}
				super.onFinish();
			}

			@Override
			public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
				// TODO Auto-generated method stub
				String response = new String(responseBody);
				LogHelper.debug("Session Respose = " + response);
				int responseCode =  JSonHelper.GetResponseStatus(response);
				try {
					if(responseCode == JSonHelper.SUCCESS_REPONSE){
						success = true;
						JSONObject jsonResponse = new JSONObject(response);
						JSONObject data = jsonResponse.getJSONObject("data");
						String userSession = "";
						userSession= data.getString("userSession");
						LogHelper.debug("Session = " + userSession);
						timerSession();
						prefs.edit().putString(PrefsHolder.SESSION_TICKET, userSession).commit();
						ConstantHolder.LOGIN_STATUS = 2;
					}else{
						showErrorMessage(MainActivity.this, getString(R.string.connection_sess_expired));
						switchFragment(FragmentCase.FRAG_HOME);
					}
				}catch (Exception e){
					
				}
			}
			
			@Override
			public void onFailure(int statusCode, Header[] headers,
					byte[] responseBody, Throwable error) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onStart() {
				// TODO Auto-generated method stub
				progress = ProgressDialog.show(MainActivity.this, "" , getString(R.string.loading));
				
				super.onStart();
			}
		});
	}
	
	public void connGetGeoIP(){
		WifiManager wm = (WifiManager) getSystemService(WIFI_SERVICE);
		String ip = Formatter.formatIpAddress(wm.getConnectionInfo().getIpAddress());
		RequestParams params = new RequestParams();
		params.put("ip", ip);
		CustomHttpClient.postWithHash(CustomHttpClient.GEO_IP,params , new AsyncHttpResponseHandler() {
				
				@Override
				public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
					// TODO Auto-generated method stub
					String response = JSonHelper.removeNull(responseBody);
					LogHelper.debug("Response GEO = " + response);
					int responseCode = JSonHelper.GetResponseStatus(response);
					if (responseCode == JSonHelper.SUCCESS_REPONSE){
						try{
							String dataRes = JSonHelper.JSonString(response, "data");
							JSONObject data = new JSONObject(dataRes);
							String geoStation = data.getString("geoipStation");
							String geoCountry = data.getString("geoipCountryCode");
							String geoCurrency = data.getString("userCurrencyCode");
							prefs.edit().putString(PrefsHolder.GEO_STATION, geoStation)
										.putString(PrefsHolder.GEO_COUNTRY, geoCountry)
										.putString(PrefsHolder.GEO_CURRENCY, geoCurrency)
										.commit();
						}catch(Exception e){
							e.printStackTrace();
						}
					}else{
						
					}
				}
				
				@Override
				public void onFailure(int statusCode, Header[] headers,
						byte[] responseBody, Throwable error) {
					// TODO Auto-generated method stub
					
				}	
			});
		}
	 
	@TargetApi(Build.VERSION_CODES.HONEYCOMB_MR1) @Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
	    // Check which request we're responding to
	    if (requestCode == PICK_DATE_REQUEST) {
	        // Make sure the request was successful
	        if (resultCode == RESULT_OK) {
	        	Bundle bundle = data.getExtras();
	            Date departureDate = (Date) bundle.getSerializable(CustomCalendarActivity.DEPARTURE_DATE);
	            Date returnDate = (Date) bundle.getSerializable(CustomCalendarActivity.RETURN_DATE);
	            LogHelper.debug("departure date selected = "+departureDate);
	            LogHelper.debug("return date selected = "+returnDate);
	            
	            int did = bundle.getInt("departId", 0);
	            int rid = bundle.getInt("returnId", 0);
	            SimpleDateFormat outF = new SimpleDateFormat("EEE',' dd MMM yyyy");
	            
	            String dateString = outF.format(departureDate);
	            aq.id(did).text(dateString);
	            
	            if (returnDate != null){
	            	dateString = outF.format(returnDate);
	            	aq.id(rid).text(dateString);
	            }
	        }
	    } else if (requestCode == MEMBER_CHECKIN_REQUEST){
	    	if (resultCode == RESULT_OK){
	    		LogHelper.debug("Update Data Here");
	    		Bundle extras = data.getExtras();
	    		if (extras != null){
					String expDate = extras.getString("expireDate", "");
		        	String issue = extras.getString("issue", "");
		        	String dob = extras.getString("dob", "");
		        	String national = extras.getString("national", "");
		        	String passport = extras.getString("passport", "");
		        	int pos = extras.getInt("child", 0);
		        	int parentPos = extras.getInt("parent", 0);
		        	if (currentBooking != null){
		        		currentBooking.updateMemberAtPos(pos, expDate, issue, dob, national, passport);
		        		if (parentPos == 0){
		        			currentBooking.getMemberList().get(pos).setDepart(true);
		        		}else{
		        			currentBooking.getMemberList().get(pos).setReturn(true);
		        		}
		        		BookingAdapter adapter = (BookingAdapter) aq.id(R.id.expandable_list).getExpandableListView().getExpandableListAdapter();
						adapter.notifyDataSetChanged();
						
						aq.id(R.id.check_in_submit).visible();
						
		        	}
	    		}
	    	}
	    }else if (requestCode == PICK_STATE_REQUEST){
	    	if (resultCode == RESULT_OK){
		    	Bundle extras = data.getExtras();
	    		if (extras != null){
					String stationName = extras.getString("name", "");
		        	String stationKey = extras.getString("key", "");
		        	int vid = extras.getInt("viewId", 0);
			    	aq.id(vid).getButton().setText(stationName + " (" + stationKey + ")");
			    	aq.id(vid).getButton().setTag(stationKey);
			    	if(vid == R.id.booking_departure){
			    		aq.id(R.id.booking_destination).text("");
			    		aq.id(R.id.booking_destination).getButton().setTag("");
			    	}
	    		}
	    	}
	    }else if ( requestCode == GUEST_NATION){
	    	if (resultCode == RESULT_OK){
	    		Bundle extras = data.getExtras();
	    		if (extras!=null){
					String countryName = extras.getString("name", "");
		        	String countryKey = extras.getString("key", "");
		        	int position = extras.getInt("viewId", 0);
		        	updateGuestList(countryKey, countryName, position);
	    		}
	    	}
	    }else if (requestCode == SSR_LISTING){
	    	if (resultCode == RESULT_OK){
	    		Bundle extras = data.getExtras();
	    		int type = -1;
	    		if (extras!=null){
	    			type = extras.getInt("type", -1);
	    		}
	    		if (type != -1){
	    			currentBooking = (BookingInfoModel) extras.getSerializable("resultBooking");
	    			ssrItem = (SSRMainModel) extras.getSerializable("ssrmain");
	    			switch(type){
	    			case 1:
	    				updateInsurance();
	    				break;
    				default:
    					upgradeSSRBaggagePrice();
    					break;
	    			}
	    		}
	    	}
	    }else if(requestCode == LOGIN_REQUEST){
	    	if (resultCode == RESULT_OK){
	    		refreshGuestListing();
	    	}
	    }
	}
	
	public void closeProgressDialog(){
		if (progress!=null){
			progress.dismiss();
		}
	}
	
	private void connSearchFlight(String depart, String arrival,final String deDate,final String reDate,
						int adultCount, int childCount, int infantCount, String promoCode, String userCurreny){
		
		saerchItem = new SearchInfoModel(depart, arrival, promoCode, 
				userCurreny, adultCount, childCount, infantCount, deDate, reDate);
		
		RequestParams params = new RequestParams();
		params.put("departureStation", depart);
		params.put("arrivalStation", arrival);
		params.put("departureDate", deDate);
		if(reDate != null && reDate.trim().length()>0){
			params.put("returnDate", reDate);
			saerchItem.setReturn(true);
		}else{
			saerchItem.setReturn(false);
		}
		params.put("adultPax", ""+adultCount);
		params.put("childPax", ""+childCount);
		params.put("infantPax", ""+infantCount);
		params.put("promoCode", promoCode);
		params.put("userCurrencyCode", userCurreny);
		params.put("userSession", prefs.getString(PrefsHolder.SESSION_TICKET, ""));
		
		
		
		CustomHttpClient.postWithHash(CustomHttpClient.GET_AVAILABILITY, params, new AsyncHttpResponseHandler() {
			
			@Override
			public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
				// TODO Auto-generated method stub
				String response = new String(responseBody);
				int responseCode = JSonHelper.GetResponseStatus(response);
				LogHelper.debug("Response Flight  " + response);
				if (responseCode == JSonHelper.SUCCESS_REPONSE){
					saerchItem = JSonHelper.parseFlight(response, deDate, reDate, saerchItem);
					int position = 1;
					for (int i =0; i < saerchItem.getFlightList().size(); i++){
						if(saerchItem.getFlightList().get(i).getDateyyyyMMdd().equals(deDate)){
							position = i;
							break;
						}
					}
					selectedFragment = FragmentCase.FRAG_FARE_DISPLAY;
					BookingFragment bFrag = BookingFragment.newInstance(FragmentCase.FRAG_FARE_DISPLAY, saerchItem, 0, position, "", "");
					swapFragment(bFrag);
				}else{
					String errorMsg = JSonHelper.GetResponseMessage(response);
					showErrorMessage(MainActivity.this, errorMsg);
				}

				closeProgressDialog();
			}
			
			@Override
			public void onFailure(int statusCode, Header[] headers,
					byte[] responseBody, Throwable error) {
				// TODO Auto-generated method stub
				showErrorMessage(MainActivity.this, getString(R.string.connection_error));
				closeProgressDialog();
			}

			@Override
			public void onFinish() {
				// TODO Auto-generated method stub
				if (progress !=null){
					progress.dismiss();
				}
				super.onFinish();
			}

			@Override
			public void onStart() {
				// TODO Auto-generated method stub
				super.onStart();
			}
		});
		
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		if (viewHandler !=null){
			viewHandler.removeCallbacks(updateViewRun);
		}
		
		if (handler !=null){
			handler.removeCallbacks(bookingRun);
		}
		
		if (timerHandler !=null){
			timerHandler.removeCallbacks(sessionRunnable);
		}
		
		super.onDestroy();
	}
	
	public void setupReturnFlight(){
		int pos = 1;
		for (int i = 0; i < saerchItem.getFlightList().size(); i++){
			FlightSearchModel item = saerchItem.getFlightList().get(i);
			if(item.getDateyyyyMMdd().equalsIgnoreCase(saerchItem.getSelectReturnDate())){
				pos = i;
				break;
			}
		}
		LogHelper.debug("Setup Return");
		BookingFragment bFrag = BookingFragment.newInstance(FragmentCase.FRAG_FARE_DISPLAY, saerchItem, 1, pos, "", "", false);
		swapFragment(bFrag);
	}

	public void setupSelectedFlight(String _id, SearchInfoModel _seacrhItem, int pos,FareTypeModel _fm,
										FareTypeModel _upgradeFare, boolean isDepart){
		if (selectedFligh == null){
			selectedFligh = new SelectedFlightModel();
		}
		
		selectedFligh.currency = saerchItem.getUserCurrency();

		selectedFligh.setPaxCount(_seacrhItem.getAdultCount(), 
						_seacrhItem.getChildCount(),
						_seacrhItem.getInfantCount());
		
		if (isDepart){
			selectedFligh.upgradeDepart = _upgradeFare;
			selectedFligh.departModel = _fm;
			selectedFligh.dDate = _seacrhItem.getFlightList().get(pos).getDateyyyyMMdd();
			selectedFligh.departId = _id;
			selectedFligh.departName = saerchItem.getDepartList();
		}else{
			selectedFligh.upgradeReturn = _upgradeFare;
			selectedFligh.returnModel = _fm;
			selectedFligh.rDate = _seacrhItem.getFlightList(	).get(pos).getDateyyyyMMdd();
			selectedFligh.returnId = _id;
			selectedFligh.returnName = saerchItem.getReturnList();
		}
		
	}
	
	public boolean checkFligthDate(FareTypeModel _item, int frag, int pos){
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		Date d;
		try {
			d = format.parse(selectedFligh.dDate);
		
			List<SegmentModel> segList = _item.getSegList();
			List<SegmentModel> departSeg = selectedFligh.departModel.getSegList();
			SimpleDateFormat checkFormat = new SimpleDateFormat(ConstantHolder.PATTERN_YMDTHMS);
			for (int i = 0; i < segList.size(); i ++){
				Date fareDate = checkFormat.parse(segList.get(i).getDataSTD());
				for (int x = 0 ; x < departSeg.size(); x++){
					Date departDate = checkFormat.parse(departSeg.get(x).getDataSTD());
					if (fareDate.before(departDate)){
						MainActivity.showErrorMessage(MainActivity.this, getString(R.string.error_return_earlier));
						return false;
					}
				}
			}
			
			String returnStd = segList.get(0).getDataSTD();
			String departSTA = departSeg.get(departSeg.size()-1).getDateSTA();
			int diffDate = ConstantHelper.calculateTimeRangeHour(returnStd,
								departSTA,
								ConstantHolder.PATTERN_YMDTHMS);
			
			if (diffDate<3){
				showErrorMessage(MainActivity.this, getString(R.string.error_time_range));
				return false;
			}
			
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			d = new Date();
		}
		
		
		return true;
	}

	public void connSellJourney(){
		RequestParams params = new RequestParams();

		LogHelper.debug(selectedFligh.dDate + " Sell Depart Journey " + selectedFligh.departId);
		String departId = "";
		if(selectedFligh.isUpgradeDepart()){
			departId  = selectedFligh.upgradeDepart.getId();
		}else{
			departId = selectedFligh.departId;
		}
		
		params.put("depart_journey_id", departId);
		params.put("departureDate", selectedFligh.dDate);
		

		if (selectedFligh.returnId != null && selectedFligh.returnId.trim().length()>0){
			String returnId = "";
			if (selectedFligh.isUpgradeReturn()){
				returnId = selectedFligh.upgradeReturn.getId();
			}else{
				returnId = selectedFligh.returnId;
			}
			
			params.put("return_journey_id", returnId);
			LogHelper.debug(" Sell Return ID Journey " + selectedFligh.returnId);
		}
		if (selectedFligh.rDate != null && selectedFligh.rDate.trim().length()>0){
			params.put("returnDate", selectedFligh.rDate);
			LogHelper.debug(" Sell Return Date Journey " + selectedFligh.rDate);
		}
		LogHelper.debug(selectedFligh.rDate + " Sell Return Journey " + selectedFligh.returnId);
		
		params.put("userSession", prefs.getString(PrefsHolder.SESSION_TICKET, ""));
		CustomHttpClient.postWithHash(CustomHttpClient.SELL_JOURNEY, params, new AsyncHttpResponseHandler() {
			
			
			
			@Override
			public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
				// TODO Auto-generated method stub
				String resposne = new String(responseBody);
				int responseCode = JSonHelper.GetResponseStatus(resposne);
				LogHelper.debug("Sell Journey " + resposne);
				if (responseCode == JSonHelper.SUCCESS_REPONSE){
					connGetBooking(1, "", "", "", true);
				}else if (responseCode == 1010){
					String errorMsg = JSonHelper.GetResponseMessage(resposne);
					showErrorMessage(MainActivity.this, errorMsg);
					switchFragment(FragmentCase.FRAG_HOME);
					if (progress!=null){
						progress.dismiss();
					}
				}
			}
			
			@Override
			public void onFailure(int statusCode, Header[] headers,
					byte[] responseBody, Throwable error) {
				// TODO Auto-generated method stub
				if (progress!=null){
					progress.dismiss();
				}
			}

			@Override
			public void onFinish() {
				// TODO Auto-generated method stub

				
				super.onFinish();
			}
		});
	}
	
	public void setupSummaryBar(){
		aq.id(R.id.summary_bar).visible();
		aq.id(R.id.summary_bar_currency).text(currentBooking.getCurrencyCode());
		String cost = String.valueOf(currentBooking.getTotalCost());
		String[] seperateCode = cost.split("\\.");
		aq.id(R.id.summary_bar_dollar).text(seperateCode[0]);
		if (seperateCode[1].trim().length()>1){
			aq.id(R.id.summary_bar_sen).text(seperateCode[1]);
		}else{
			aq.id(R.id.summary_bar_sen).text(seperateCode[1]+"0");
		}
	}
	
	public void adultNational(){
		
	}
	
	public void GuestDetails(View v){
		if (progress==null || !progress.isShowing()){
			progress = ProgressDialog.show(MainActivity.this, "Get Booking", "Loading");
		}
		connSellJourney();
		
	}
	
	public void setupGuestPage(){
		selectedFragment = FragmentCase.FRAG_GUEST_DETAILS;
		List<MemberInfoModel> mList = new ArrayList<MemberInfoModel>();
		boolean isLogin = prefs.getBoolean(PrefsHolder.IS_LOGIN, false);
		if (!isLogin){
			MemberInfoModel header = new MemberInfoModel();
			header.setfName(getString(R.string.guest_login_header));
			header.setPsgType(MemberInfoModel.HEADER);
			mList.add(header);
		}
		
		List<MemberInfoModel> adultList = currentBooking.getPassengerInfo();
		
		for (int ai = 0 ; ai < adultList.size(); ai++){
			mList.add(adultList.get(ai));
		}

		int count = adultList.get(adultList.size()-1).getPassangerNum();
		count++;
		
//		for (int ai = 0 ; ai < currentBooking.getChildCount(); ai++){
//			MemberInfoModel mModel = new MemberInfoModel();
//			mModel.setPsgType(MemberInfoModel.CHILD);
//			mModel.setPsgNumber(count);
//			count++;
//			mList.add(mModel);
//		}
		
		for (int ai = 0 ; ai < selectedFligh.infantPax; ai++){
			MemberInfoModel mModel = new MemberInfoModel();
			mModel.setPsgType(MemberInfoModel.INFANT);
			mModel.setPassangerNum(count);
			count++;
			mList.add(mModel);
		}
		
		MemberInfoModel contactPerson = new MemberInfoModel();
		contactPerson.setPsgType(MemberInfoModel.CONTACT);
		mList.add(contactPerson);
		
		MemberInfoModel emergencyPerson = new MemberInfoModel();
		emergencyPerson.setPsgType(MemberInfoModel.EMERGENCY);
		mList.add(emergencyPerson);
		
		LogHelper.debug("BEfore Init List Size " + mList.size());
		

		setupSummaryBar();
		GuestDetailsFragment gFrag = GuestDetailsFragment.newInstance(selectedFligh, mList, isLogin);
		hideButton(R.id.rightButton_view, false);
		hideButton(R.id.leftButton_view, true);
		updateTitle(getString(R.string.search_flights), false, true);
		swapFragment(gFrag);
	}
	
	public void EditDepartSelection(View v){
		int position = 1;
		for (int i =0; i < saerchItem.getFlightList().size(); i++){
			LogHelper.debug("Date Selected " + selectedFligh.dDate);
			LogHelper.debug("Date On Flight " + saerchItem.getFlightList().get(i).getDateyyyyMMdd());
			if(saerchItem.getFlightList().get(i).getDateyyyyMMdd().equals(selectedFligh.dDate)){
				position = i;
				break;
			}
		}
		BookingFragment bFrag = BookingFragment.newInstance(FragmentCase.FRAG_FARE_DISPLAY,
					saerchItem,
					0,
					position,
					selectedFligh.departId,
					selectedFligh.dDate,
					true);
		swapFragment(bFrag);
	}
	
	public void EditReturnSelection(View v){
		int position = 1;
		for (int i =0; i < saerchItem.getFlightList().size(); i++){
			if(saerchItem.getFlightList().get(i).getDateyyyyMMdd().equals(selectedFligh.rDate)){
				position = i;
				break;
			}
		}
		BookingFragment bFrag = BookingFragment.newInstance(FragmentCase.FRAG_FARE_DISPLAY,
					saerchItem,
					1,
					position,
					selectedFligh.returnId,
					selectedFligh.rDate,
					true);
		swapFragment(bFrag);
	}
	
	public void PriceSummary(View v){
		Intent i = new Intent(MainActivity.this, PriceSummaryActivity.class);
		if (currentBooking == null){
			currentBooking = new BookingInfoModel();
		}
		
		Bundle extra = new Bundle();
		extra.putSerializable("info", currentBooking);
		i.putExtras(extra);
		
		startActivity(i);
	}

	public void UpdateSSR(View v){
		Intent i = new Intent(MainActivity.this, SSRListingActivity.class);
		if (ssrItem == null){
			ssrItem = new SSRMainModel();
		}
		
		int type = 0;
		String title = "";
		
		Bundle extra = new Bundle();
		if (v.getId() == R.id.ssr_baggage_view){
			type = 0;
			title = getString(R.string.add_ons);
		}
		if (v.getId() == R.id.ssr_insurance_view){
			type = 1;
			title = getString(R.string.insurance_view_title);
		}
		
		if (v.getId() == R.id.ssr_meal_view){
			type = FragmentCase.FRAG_SSR_MEAL;
			title = getString(R.string.meal_title);
		}
		extra.putInt("type", type);
		extra.putString("title", title);
		extra.putSerializable("ssr", ssrItem);
		extra.putSerializable("flight", currentBooking);
		i.putExtras(extra);
		
		startActivityForResult(i, SSR_LISTING);
	}
	
	private void updateGuestList(String _key, String _name, int position){
		GuestListingAdapter mAdapter = 
						(GuestListingAdapter) aq.id(R.id.list_view_container).getListView().getAdapter();
		if (mAdapter != null){
			mAdapter.updateGuestNation(_key, _name, position);
			LogHelper.debug("upate User Data");
		}
	}
	
	public void AddOnClick(View v){
		getGuestToJSon();
	}
	
	private void getGuestToJSon(){

		if (progress == null || !progress.isShowing()){
			progress = ProgressDialog.show(MainActivity.this, "", "Loading");
		}
		
		JSONArray jGuest = new JSONArray();
		JSONObject jContact = new JSONObject();
		JSONObject jEmergency = new JSONObject();
		
		GuestListingAdapter mAdapter = (GuestListingAdapter) aq.id(R.id.list_view_container).getListView().getAdapter();
		
		List<MemberInfoModel> pList =  mAdapter.getMemberInfos();
		
//		if (!checkPassangerDate(pList)){
//			showErrorMessage(MainActivity.this, getString(R.string.error_dob_year));
//			progress.dismiss();
//			return;
//		}
		boolean isContactSelected = false;
		try{
			for (int i = 0; i <pList.size(); i++){
				MemberInfoModel mModel = pList.get(i);
				if(mModel.isHuman()){
					JSONObject jPerson = new JSONObject();
					if (mModel.getPsgType() == MemberInfoModel.CHILD
							||mModel.getPsgType() == MemberInfoModel.INFANT
							||mModel.getPsgType() == MemberInfoModel.ADULT) {
						jPerson.put("PaxType", mModel.getPaxType());
						jPerson.put("FirstName", mModel.getfName());
						jPerson.put("LastName", mModel.getlName());
						jPerson.put("DOB", mModel.getDob());
						jPerson.put("Gender", mModel.getGenderStr());
						jPerson.put("Nationality", mModel.getCountryCode());
						jPerson.put("PassengerNumber", ""+mModel.getPassangerNum());
						if (mModel.isContactPerson()){
							isContactSelected = true;
							String title = mModel.getGenderStr();
							if (title.equalsIgnoreCase("Male")){
								jContact.put("Title", "Mr");
							}else{
								jContact.put("Title", "Ms");
							}
							jContact.put("FirstName", mModel.getfName());
							jContact.put("LastName", mModel.getlName());
							jContact.put("MobileNo", mModel.getMobileNo());
							jContact.put("Email", mModel.getUserName());
						}
						jGuest.put(jPerson);
					}else if (mModel.getPsgType() ==  MemberInfoModel.CONTACT && !isContactSelected){
						jContact.put("Title", mModel.getGenderStr());
						jContact.put("FirstName", mModel.getfName());
						jContact.put("LastName", mModel.getlName());
						jContact.put("MobileNo", mModel.getMobileNo());
						jContact.put("Email", mModel.getUserName());
					}else if (mModel.getPsgType() ==  MemberInfoModel.EMERGENCY){
						int check = mModel.checkEmergencyContactValid();
						if (check == 1){
							jEmergency.put("FirstName", mModel.getfName());
							jEmergency.put("LastName", mModel.getlName());
							jEmergency.put("MobileNo", mModel.getMobileNo());
							jEmergency.put("Relationship", mModel.getUserName());
						}else if (check == 2){
							showErrorMessage(MainActivity.this, getString(R.string.error_emergency_contact));
							if (progress!=null){
								progress.dismiss();
							}
							return;
						}
					}
				}
			}
		}catch (Exception e){
			e.printStackTrace();
		}
		
		LogHelper.debug("Guest Json " + jGuest.toString());
		LogHelper.debug("Jcontact " + jContact.toString());
		
		connGetSSRAvailable(0, 0, jGuest, jContact, null);
	}
	
	private void connGetSSRAvailable(int requestType, int requestFrom,
				JSONArray passanger, JSONObject contact, JSONObject emergency){
		RequestParams params = new RequestParams();
		params.put("request_type", String.valueOf(requestType));
		params.put("request_from", String.valueOf(requestFrom));
		
		if (passanger !=null){
			params.put("passenger_data", passanger.toString());
		}
		
		if (contact != null){
			params.put("contact_data", contact.toString());
		}
		
		if (emergency != null){
			params.put("emergency_data", emergency);
		}

		params.put("userSession", prefs.getString(PrefsHolder.SESSION_TICKET, ""));
		CustomHttpClient.postWithHash(CustomHttpClient.GET_SSR, params, new AsyncHttpResponseHandler() {
			
		
			@Override
			public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
				// TODO Auto-generated method stub
				String response = new String(responseBody);
				LogHelper.debug("SSR Response " + response);
				try{
					int responseCode = JSonHelper.GetResponseStatus(response);
					if (responseCode == JSonHelper.SUCCESS_REPONSE){
						JSONObject json = new JSONObject(response);
						JSONObject data = json.getJSONObject("data");
						ssrItem = JSonHelper.parseSSRMainModel(data);
						updateItemName();
						ssrItem.generateEmptyMeal();
						switchFragment(FragmentCase.FRAG_SSR_LISTING);
					}else{
						
					}
				}catch (Exception e){
					
				}
			}
			
			@Override
			public void onFailure(int statusCode, Header[] headers,
					byte[] responseBody, Throwable error) {
				// TODO Auto-generated method stub	
			}

			@Override
			public void onFinish() {
				// TODO Auto-generated method stub
				if (progress != null && progress.isShowing()){
					progress.dismiss();
				}
				super.onFinish();
			}
			
			
			
		});
	}
	
	private boolean checkPassangerDate(List<MemberInfoModel> _pList){
		Date d1 = new Date();
		Date d2 = new Date();
		SimpleDateFormat format = new SimpleDateFormat(ConstantHolder.PATTERN_YMD);
		
		
		for (int i = 0 ; i < _pList.size(); i ++){
			if (_pList.get(i).getPsgType() == MemberInfoModel.CHILD
					||_pList.get(i).getPsgType() == MemberInfoModel.INFANT
					||_pList.get(i).getPsgType() == MemberInfoModel.ADULT) {
				try {
					d2 = format.parse(_pList.get(i).getDob());
					int yearCount = ConstantHelper.getYearDiff(d1, d2);
					if (yearCount <0){
						return false;
					}else if (yearCount>0 && yearCount<2){
						if (_pList.get(i).getPsgType()!=MemberInfoModel.INFANT){
							return false;
						}
					}else if (yearCount>2 && yearCount<18){
						if (_pList.get(i).getPsgType()!=MemberInfoModel.CHILD){
							return false;
						}
					}else{
						if (_pList.get(i).getPsgType() == MemberInfoModel.INFANT ||
								_pList.get(i).getPsgType() == MemberInfoModel.CHILD){
							return false;
						}
					}
					
				}catch (Exception e){
					e.printStackTrace();
				}
			}
		}
		
		
		return true;
	}

	public void updateItemName(){
		if (ssrItem != null){
			List<SSRContainer> container = ssrItem.getDepartContainer();
			if (container!=null && container.size()>0){
				for (int i = 0; i < container.size();i++){
					List<SSRItemModel> list = container.get(i).getWheelList();
					generateName(list, false);
					
					list = container.get(i).getBaggageList();
					generateName(list, true);
					list = sortBaggage(list);
					
					list = container.get(i).getMealList();
					generateName(list, false);
				
					list = container.get(i).getMealBundle();
					generateName(list, false);
				}
			}
			
			if (ssrItem.getReturnContainer()!=null){
				container = ssrItem.getReturnContainer();
				if (container!=null && container.size()>0){
					for (int i = 0; i < container.size();i++){
						List<SSRItemModel> list = container.get(i).getWheelList();
						generateName(list, false);
						
						list = container.get(i).getBaggageList();
						generateName(list, true);
						list = sortBaggage(list);
						
						list = container.get(i).getMealList();
						generateName(list, false);
						
						list = container.get(i).getMealBundle();
						generateName(list, false);
					}
				}
			}
		}
	}
	
	public void generateName(List<SSRItemModel> list, boolean isBaggage){
		SQLhelper db = new SQLhelper(MainActivity.this);
		
		if (list == null){
			db.close();
			return;
		}
		
		for (int i = 0; i < list.size(); i ++){
			if (i == 0 && isBaggage){
				
			}else{
				String key = list.get(i).getSsrCode();
				if (key!=null && key.length()>0){
					SSRModel mSSR = db.getSSRBaseKey(key);
					LogHelper.debug( key +" SSR Desctiprion " + mSSR.getDescriptio());
					list.get(i).setDescripion(mSSR.getDescriptio());
				}
			}
		}
		db.close();
	}

	public void upgradeSSRBaggagePrice(){
		aq.id(R.id.ssr_baggage_currency).text(currentBooking.getCurrencyCode());
		String totalBaggagePrice = currentBooking.getTotalBaggagePrice();
		String[] splitTotal = totalBaggagePrice.split("\\.");
		LogHelper.debug("Price Baggage Total  " + totalBaggagePrice);
		aq.id(R.id.ssr_baggage_dollar).text(splitTotal[0]);
		aq.id(R.id.ssr_baggage_sen).text(splitTotal[1]);
	}

	public void refreshGuestListing(){
		try{
			boolean _isLogin = prefs.getBoolean(PrefsHolder.IS_LOGIN, false);
			GuestListingAdapter mAdapter = (GuestListingAdapter) aq.id(R.id.list_view_container)
												.getListView().getAdapter();
			mAdapter.updateLoginStatus(_isLogin);
			LogHelper.debug("Refresh guest list success");
		
		}catch (Exception e){
			LogHelper.debug("Refresh guest list error");
		}
		
	}
	public List<SSRItemModel> sortBaggage(List<SSRItemModel> baggage) {
	    List<SSRItemModel> sortedBaggage = baggage;
	    for(int out = baggage.size()-1; out>1; out--) {
	        for(int i = 0; i < out;i ++){
	            int n = i+1;
	            if(sortedBaggage.get(i).getBaggageOrder() > sortedBaggage.get(n).getBaggageOrder()) {
	            	SSRItemModel temp = sortedBaggage.get(i);
	                sortedBaggage.set(i, sortedBaggage.get(n));
	                sortedBaggage.set(n, temp);
	            }
	        }
	    }

	    return sortedBaggage;
	}
	
	public void updateInsurance(){
		aq.id(R.id.ssr_insurance_currency).text(currentBooking.getCurrencyCode());
		String totalInsurancePrice = ssrItem.getTotalInsurance();
		String[] splitTotal = totalInsurancePrice.split("\\.");
		LogHelper.debug("Price Baggage Total  " + totalInsurancePrice);
		aq.id(R.id.ssr_insurance_dollar).text(splitTotal[0]);
		aq.id(R.id.ssr_insurance_sen).text(splitTotal[1]);
	}
	
	public void BookingSummary(View v){
		switchFragment(FragmentCase.FRAG_PAYMENT_SUMMARY);
	}

	
}
