package com.airasia.holder;

import java.util.List;

import com.airasia.model.DrawerModel;

public class ConstantHolder {

	public final static String nameSpace = "http://AA_SSO/Admin/AdminService";
	public final static String soapUrl = "https://stgsso.airasia.com/AdminService.svc";//?wsdl";
	
	//Action
	public final static String actionGetCountry = "GetCountry";
	public final static String actionAuthentication = "AuthenticateUser";

	public final static String sa_Authentication = "http://AA_SSO/Admin/AdminService/AdminService/AuthenticateUser";
	public final static String sa_GetCountry = "http://AA_SSO/Admin/AdminService/AdminService/GetCountry";

	public final static String PATTERN_YMD = "yyyy-MM-dd";
	public final static String PATTERN_YMDTHMS = "yyyy-MM-dd'T'hh:mm:ss";
	
	public static List<DrawerModel> drawerList;
	public static List<String> bannerList;
	// HTTP timeout
	public static final int DEFAULT_HTTP_TIMEOUT = 300000;
	public static final int DEFAULT_HTTP_RETRY = 10;
	
	public static String lang = "en-GB";
	
//	public static final String BASE_URL = "https://54.169.17.14/";
	public static final String BASE_URL = "https://54.169.34.56/";

	public static final String URL = BASE_URL + "api/apps.php";
	
	public static final String testKey = "cSjFy2-7WRdU3j?Ex$GLy1E8cFU,`z";

	public static final String FARE_ADT = "ADT";
	public static final String FARE_CHD = "CHD";
	public static final String FARE_INF = "INF";
	
	public static int LOGIN_STATUS = 3;
	public static int SESSION_TYPE = 0;
	
	public static String session_ticket;
	
}
