package com.airasia.service;

import java.util.List;

import org.apache.http.Header;
import org.json.JSONObject;

import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;

import com.airasia.model.CountryModel;
import com.airasia.model.CurrencyModel;
import com.airasia.model.FareClassUpgrade;
import com.airasia.model.FareTaxesModel;
import com.airasia.model.SSRModel;
import com.airasia.model.StationModel;
import com.airasia.util.CustomHttpClient;
import com.airasia.util.JSonHelper;
import com.airasia.util.LogHelper;
import com.airasia.util.SQLhelper;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

public class ConnectionService extends Service {

	Handler handler = new Handler();
	
	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		// TODO Auto-generated method stub
		LogHelper.debug("Connection Service Start");
//		handler.postDelayed(runabble, 500);re
		connGetCountry();
		return Service.START_NOT_STICKY;
//		return super.onStartCommand(intent, flags, startId);
	}
	
	

	@Override
	public void onCreate() {
		// TODO Auto-generated method stub
		super.onCreate();
	}


	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}



	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		return null;
	}
	
	Runnable runabble = new Runnable() {
		
		@Override
		public void run() {
			// TODO Auto-generated method stub
//			connUpdateDevice("0", "0", "0");
			connGetCountry();
		}
	};
	
	private void connGetCountry(){
		RequestParams params = new RequestParams();
		
		CustomHttpClient.postWithHash(CustomHttpClient.GET_COUNTRY, params, new AsyncHttpResponseHandler() {
			
			@Override
			public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
				// TODO Auto-generated method stub
				String respone = new String(responseBody);
//				LogHelper.debug("Country = " + respone);
				int responseCode = JSonHelper.GetResponseStatus(respone);
				LogHelper.debug("Responsse Code Settings " + responseCode);
				
				try{
					if(responseCode != JSonHelper.SUCCESS_REPONSE){
						LogHelper.debug("Error " + JSonHelper.GetResponseMessage(respone));
					}else{
						List<CountryModel> countryList = JSonHelper.parseCountryList(respone);
						List<StationModel> stationList = JSonHelper.parseStationList(respone);
						List<SSRModel> ssrList = JSonHelper.praseSSRList(respone);
						
						JSONObject json = new JSONObject(respone);
						JSONObject data = json.getJSONObject("data");
						List<FareClassUpgrade> fareCList = JSonHelper.praseFareClassList(data);
						List<FareTaxesModel> fareTList = JSonHelper.parseTaxes(data);
						List<CountryModel> currencyList = JSonHelper.parseExchangeRare(data);
						
						LogHelper.debug("Update Country DB");
						LogHelper.debug("Country List" + countryList.size());
						SQLhelper db = new SQLhelper(getApplicationContext());
						if(countryList != null && countryList.size()!=0){
							for (int i=0; i <countryList.size();i++){
								db.insertCountry(countryList.get(i));
							}
	
						}
	 
						LogHelper.debug("Station List" + stationList.size());
						if(stationList != null && stationList.size()!=0){
							for (int x=0; x<stationList.size(); x++){
									db.insertStation(stationList.get(x));
	//							LogHelper.debug("Insert Station Database Succesfull");
								}
								LogHelper.debug("Insert Country Database Succesfull");
						}
						
						LogHelper.debug("SSR List Siz " + ssrList.size());
						if (ssrList!=null && ssrList.size()!=0){
							db.insertSSRList(ssrList);
							LogHelper.debug("Insert SSR Database Succesfull");
						}
						
						if (fareCList!=null && fareCList.size()>0){
							db.insertFareClassList(fareCList);
							LogHelper.debug("Insert Fare Class Upgrade Text");
						}
						
						if (fareTList != null && fareTList.size()>0){
							db.insertFareTaxesList(fareTList);
							LogHelper.debug("Insert Fare Tax Text");	
						}
						
						if (currencyList != null && currencyList.size()>0){
							db.insertCurrencyList(currencyList);
							LogHelper.debug("Insert exchange rate");
						}
						db.close();
					}
				}catch (Exception e){
					e.printStackTrace();
				}
				
				
			}
			
			@Override
			public void onFailure(int statusCode, Header[] headers,
					byte[] responseBody, Throwable error) {
				// TODO Auto-generated method stub
				
			}
		});
	}


//	private void connUpdateDevice(String deviceID, String OS, String deviceToken){
//		RequestParams params = new RequestParams();
//		String android_id = Secure.getString(getApplicationContext().getContentResolver(),
//                Secure.ANDROID_ID); 
//		params.add("deviceID", android_id);
//		params.add("operatingSystem", "android");
//		params.add("deviceToken", deviceToken);
//		LogHelper.debug("Service Params = " + params.toString());
//		
//		CustomHttpClient.post(CustomHttpClient.UPDATE_DEVICE, params, new AsyncHttpResponseHandler() {
//			
//			@Override
//			public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
//				// TODO Auto-generated method stub
//				String response = new String(responseBody);
//				
//			}
//			
//			@Override
//			public void onFailure(int statusCode, Header[] headers,
//					byte[] responseBody, Throwable error) {
//				// TODO Auto-generated method stub
//				
//			}
//		});
//	}

}
